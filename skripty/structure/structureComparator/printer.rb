class Printer
  class << self

    def c(str)
      puts "compare_#{str}"
    end

    def d(str)
      puts str
    end

    # ap
    def a(str)
      ap str
    end

    # ap
    def error(str)
      puts "ERROR: #{str}"
      exit
    end

    def undefined
      raise NotImplementedError
    end
  end
end