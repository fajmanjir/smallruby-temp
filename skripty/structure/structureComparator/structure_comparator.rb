require_relative 'node_type'
require_relative 'printer'


class AssertException < Exception

end

class StructureComparator

  def initialize
    @comparations = 0
  end

  def compare_with_mri(ast, mri)
    ap ast
    p '------------------------------------------------------------'
    # p mri

    mri = mri[1] if mri[0] == :program
    ap mri

    # p NodeType.convert(ast[0])

    result = compare_node(ast, mri)
    if result
      puts 'Validation SUCCESS'
      return true
    else
      puts 'Validation FAILED'
      return false
    end
  end

  private

  def compare_node(node, mri)
    Printer.d "node: #{node[0]}"
    @comparations += 1

    return true if mri[0].is_a? Fixnum
    assert_not_equals(mri.length, 0)

    if mri.is_a?(Array) && mri.length < 2 && node.is_a?(Array) && node[0] != NodeType.get_sym(:block)
      Printer.d 'MRI Trimmed'
      return compare_node(node, mri[0])
    end

    return compare_ident(node, mri) if node.is_a?(String)


    node_type = node[0]

    Printer.d '----------------------------'
    Printer.d "- compare_node: #{node_type}"

    case node_type
      when NodeType.get_sym(:lasgn);  compare_lasgn(node, mri)
      when NodeType.get_sym(:number);  compare_number(node, mri)
      when NodeType.get_sym(:class);  compare_class(node, mri)
      when NodeType.get_sym(:colon2);  compare_colon2(node, mri)
      when NodeType.get_sym(:scope);  compare_scope(node, mri)
      when NodeType.get_sym(:defn);  compare_defn(node, mri)
      when NodeType.get_sym(:fcall);  compare_fcall(node, mri)
      else
        compare_default(node, mri)
    end
  end

  def compare_class(node, mri)

    Printer.c 'class'

    return false if mri[0] != :class

    compare_node(node[1], mri[1]) # class name
    compare_node(node[2], mri[3]) # body
  end

  def compare_scope(node, mri)
    Printer.c 'scope'
    assert_equals(mri[0], :bodystmt)
    Printer.d 'scope 2'
    compare_node(node[2], mri[1])
    true
  end

  def compare_defn(node, mri)
    Printer.c 'defn'
    assert_equals(mri[0], :def)
    compare_node(node[2], mri[1]) # method name
    compare_node(node[3], mri[3]) # method body
    true
  end

  def compare_fcall(node, mri)
    Printer.c 'fcall'
    assert_equals(mri[0], :method_add_arg)
    assert_equals(mri[1][0], :fcall)
    compare_node(node[2], mri[1][1]) # method name
    # compare_node(node[3], mri[3]) # method body
    true
  end

  def compare_lasgn(node, mri)

    Printer.c 'lasgn'

    assert_equals(mri[0], :assign)

    compare_node(node[1], mri[1][1]) # left side
    compare_node(node[2], mri[2]) # right side
  end

  def compare_ident(node, mri)
    # "abc"
    Printer.c 'ident'

    assert_equals(mri[0], :@ident)
    assert_equals(mri[1], node) # compare identifiers
  end

  def compare_number(node, mri)
    Printer.c 'number'
    assert_equals(mri[0], :@int)
    assert_equals(node[1].to_s, mri[1])
    true
  end

  def compare_colon2(node, mri)
    Printer.c 'colon2'
    assert_equals(mri[0], :const_ref)
    assert_equals(mri[1][0], :@const)
    assert_equals(node[2], mri[1][1])
    true
  end

  def compare_default(node, mri)
    Printer.c 'default'
    false
  end

  def assert(bool)
    unless bool
      Printer.error "#{__LINE__}: assertion failed"
      raise AssertException.new("assertion failed")
    end
  end

  def assert_equals(a, b)
    unless a == b
      Printer.a a
      Printer.a b
      Printer.error "#{__LINE__}: assertion failed"
      raise AssertException.new("assertion failed")
    end
  end

  def assert_not_equals(a, b)
    unless a != b
      Printer.a a
      Printer.a b
      Printer.error "#{__LINE__}: not_equals assertion failed"
      raise AssertException.new(" not_equals assertion failed")
    end
  end
end