class NodeType
  class << self
    def convert(type)
      type = type.to_s[5..-1].downcase
      # case type.to_s
      #   when ''
      #   else
      #
      # end

      type.to_sym
    end

    def get(node_type_sym)
      case node_type_sym
        when :lasgn;  return 'NODE_LASGN'
        when :block;  return 'NODE_BLOCK'
        when :fcall;  return 'NODE_FCALL'
        when :number;  return 'NODE_NUMBER'
        when :class;  return 'NODE_CLASS'
        when :colon2;  return 'NODE_COLON2'
        when :scope;  return 'NODE_SCOPE'
        when :defn;  return 'NODE_DEFN'
        else
          Printer.error "unknown NodeType #{node_type_sym}"
          node_type_sym
      end
    end

    def get_sym(node_type_sym)
      get(node_type_sym).to_sym
    end
  end
end