require 'ripper'
require 'benchmark'
require 'awesome_print'

require_relative 'structureComparator/structure_comparator'

structure_test = StructureComparator.new

tokens = 1

tokens_in_snippet = 1
cycles = tokens / tokens_in_snippet
p cycles

# pp Ripper.sexp('def hello(world) "Hello, #{world}!"; end')
if ARGV.length == 1

  # time = Benchmark.measure {
    ap Ripper.sexp(ARGV[0] * cycles)
  # }
  # puts "\nTokens count: #{tokens}\n#{time.real}"
  # ap Ripper.sexp_raw(ARGV[0])
elsif ARGV.length == 2
  if ARGV[0] == '--lex'
    ap Ripper.lex(ARGV[1])
  else
    comparator = StructureComparator.new
    ast = instance_eval ARGV[1]
    mri_ast = Ripper.sexp(ARGV[0])
    comparator.compare_with_mri(ast, mri_ast)
  end

end
