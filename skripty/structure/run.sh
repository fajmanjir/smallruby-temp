#!/usr/bin/env bash

if [ "$#" -eq 1 ]; then
    ruby starter.rb "$1"
elif [ "$#" -eq 2 ]; then
    ruby starter.rb "$1" "$2"
else
    echo 'usage:    run.sh [--lex] "<string_to_parse>"';
    echo '   or:    run.sh "<string_to_parse>" "<custom_ast_string>"';
fi
