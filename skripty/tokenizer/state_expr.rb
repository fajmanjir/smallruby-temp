
# nodes.rb - translate c++ code macro function definitions into smalltalk methods 

className = 'InputReader'
category = 'shortcuts'


class NodeTranslator
  def initialize(className, category)
    @writer = SmallTalkWriter.new(className, category)
  end

  def translate(file, start_line = 0, end_line = -1)
    line_number = 0
    # lines with inner function calls
    unsuccessful_lines = []

    printHeader

    file.each_line do |line|
      if(start_line <= line_number)
        break if end_line != -1 && end_line < line_number
        unsuccessful_lines << line unless translate_line(line)
      end
      line_number += 1
    end

    # unsuccessful_lines.each do |line|
    #   puts line
    # end
  end

private

  def printHeader
    puts "\"{ NameSpace: Smalltalk }\"

Object subclass:#StateBit
        instanceVariableNames:''
        classVariableNames:''
        poolDictionaries:''
        category:'Ruby Parser-AST'
!"
  end

  def translate_line(line)
#    line = line[8, line.length] # remove '#define ' part

    return false if((res = /^(\S+)$/.match(line)) == nil)

    captures = res.captures
    # 0) attr name
  
    receiver = 'parserState'
    attribute = captures[0]
    message = captures[0]

    @writer.begin_method(captures[0], [])
    @writer.method_body(captures[0])
    @writer.end_method

    puts @writer.build_string

    return true
  end
end

class SmallTalkWriter
  def initialize(class_name, category, indentation = 4)
    @class_name = class_name
    @category = category
    @result = ""
    @indentation = indentation
    @author = ' ' * indentation
    @author <<= "\"Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>\"\n"
    #@author <<= ' ' * indentation
    #@author <<= "\"Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>\"\n"
  end

  def begin_method(name, params)
    str_params = ""
    if(params.count > 0)
      first = true
      params.each do |param|
        if(first)
          first = false
          str_params <<= ": #{param}"
        else
          str_params <<= " #{param}: #{param}"
        end
      end
    end

    @result <<= <<-END

!#{@class_name} methodsFor: '#{@category}'!

EXPR_#{name}#{str_params}
END
  end

  def method_body(flag_name)
    @result <<= ' ' * @indentation
    @result <<= "^ 1 bitShift: (StateBit EXPR_#{flag_name}_BIT)"
    @result <<= "\n\n#{@author}"
  end

  def end_method
    @result <<= <<-END
! !
    END
  end

  def build_string
    result = @result
    @result = ""
    result
  end
end


file = File.open(ARGV[0])
nt = NodeTranslator.new(className, category)
nt.translate(file)