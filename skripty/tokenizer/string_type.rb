
# nodes.rb - translate c++ code macro function definitions into smalltalk methods 

className = 'StringState'
category = 'enum'


class NodeTranslator
  def initialize(className, category)
    @writer = SmallTalkWriter.new(className, category)
  end

  def translate(file, start_line = 0, end_line = -1)
    line_number = 0
    # lines with inner function calls
    unsuccessful_lines = []

    file.each_line do |line|
      if(start_line <= line_number)
        break if end_line != -1 && end_line < line_number
        unsuccessful_lines << line unless translate_line(line)
      end
      line_number += 1
    end

    # unsuccessful_lines.each do |line|
    #   puts line
    # end
  end

private

  def translate_line(line)
#    line = line[8, line.length] # remove '#define ' part

    return false if((res = /^(\S+)/.match(line)) == nil)

    captures = res.captures
    # 0) flag name
    # 1) comment

    @values ||= ['16r01', '16r02', '16r04', '16r08', '16r10', '16r20']
    @value_id ||= 0

    @writer.begin_method(captures[0], [])
    @writer.method_body(@values[@value_id])
    @writer.end_method

    puts @writer.build_string

    @value_id += 1
    return true
  end
end

class SmallTalkWriter
  def initialize(class_name, category, indentation = 4)
    @class_name = class_name
    @category = category
    @result = ""
    @indentation = indentation
    @author = ' ' * indentation
    @author <<= "\"Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>\"\n"
    #@author <<= ' ' * indentation
    #@author <<= "\"Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>\"\n"
  end

  def begin_method(name, params)
    str_params = ""
    if(params.count > 0)
      first = true
      params.each do |param|
        if(first)
          first = false
          str_params <<= ": #{param}"
        else
          str_params <<= " #{param}: #{param}"
        end
      end
    end

    @result <<= <<-END

!#{@class_name} class methodsFor: '#{@category}'!

#{name}#{str_params}
END
  end

  def method_body(return_val)
    @result <<= ' ' * @indentation
    @result <<= "^ #{return_val}"
    @result <<= "\n\n#{@author}"
  end

  def end_method
    @result <<= <<-END
! !
    END
  end

  def build_string
    result = @result
    @result = ""
    result
  end
end


file = File.open(ARGV[0])
nt = NodeTranslator.new(className, category)
nt.translate(file)