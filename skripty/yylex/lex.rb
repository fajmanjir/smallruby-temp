
require_relative 'parser'

# nodes.rb - translate c++ code macro function definitions into smalltalk methods 

className = 'StateBit'
category = 'enum'


if(ARGV[0] == nil)
  #puts 'usage: ruby lex.rb <file>'
  #exit -1
  ARGV[0] = 'data/case.cpp' # default file
end

class TranslateException < Exception
end

class NodeTranslator
  def initialize(className, category)
    @writer = SmallTalkWriter.new(className, category)
    @parser = Parser.new
  end

  def translate(file, start_line = 0, end_line = -1)
    line_number = 0
    @unsuccessful_lines = []
    @lines_processed = 0

    file.each_line do |line|
      if(start_line <= line_number)
        break if end_line != -1 && end_line < line_number
        # unsuccessful_lines << line unless translate_line(line)
        unsuccessful(line) unless translate_line(line)
        @lines_processed += 1
      end
      line_number += 1
    end

    if @unsuccessful_lines.length == 0
      STDERR.puts "\033[00;32m-------------------------------------------------------\033[00m"
      STDERR.puts "\033[00;32m#{@lines_processed} lines, #{@unsuccessful_lines.length} failures\033[00m"
    else
      STDERR.puts "\033[00;31m-------------------------------------------------------\033[00m"
      @unsuccessful_lines.each do |line|
        STDERR.puts("\033[00;31m" + line.strip + "\033[00m")
      end
      STDERR.puts ''
      STDERR.puts "\033[00;31m#{@lines_processed} lines, #{@unsuccessful_lines.length} failures\033[00m"

      STDERR.puts "stack depth: #{@parser.stack.length}"
    end
  end

private

  # unsuccessful line handler
  def unsuccessful(line)
    res = /^(\s*)(.*)/.match(line)
    puts "#{res.captures[0] * 2}#{res.captures[1]}"
    @unsuccessful_lines << line
  end

  def translate_line(line)
    return @parser.translate(line)
  end
end

class SmallTalkWriter
  def initialize(class_name, category, indentation = 4)
    @class_name = class_name
    @category = category
    @result = ""
    @indentation = indentation
    @author = ' ' * indentation
    @author <<= "\"Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>\"\n"
    #@author <<= ' ' * indentation
    #@author <<= "\"Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>\"\n"
  end

  def begin_method(name, params)
    str_params = ""
    if(params.count > 0)
      first = true
      params.each do |param|
        if(first)
          first = false
          str_params <<= ": #{param}"
        else
          str_params <<= " #{param}: #{param}"
        end
      end
    end

    @result <<= <<-END

!#{@class_name} class methodsFor: '#{@category}'!

#{name}#{str_params}
END
  end

  def method_body(return_val, comment)
    @result <<= ' ' * @indentation
    @result <<= "\"#{comment}\"\n"
    @result <<= ' ' * @indentation
    @result <<= "^ #{return_val}"
    @result <<= "\n\n#{@author}"
  end

  def end_method
    @result <<= <<-END
! !
    END
  end

  def build_string
    result = @result
    @result = ""
    result
  end
end


file = File.open(ARGV[0])
nt = NodeTranslator.new(className, category)
nt.translate(file)