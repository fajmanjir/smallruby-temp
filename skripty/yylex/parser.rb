class Parser
	def initialize
		# initialize_variables
		@stack = [] # closing brackets counter at each level
		@rules = []

		# empty line
		addRule(/^\s*$/, proc { |res|  puts '';	true })

		# if assign c
		addRule(/^(\s*)if\(\(c\s*=\s*nextc\(\)\)\s*==\s*'(.{1,2})'\) {$/, proc { |res|
			puts "#{res[0]*2}c := self nextc.";
			puts "#{res[0]*2}(c = #{convert_char(res[1])}) ifTrue: [";
			@stack << 1
			true 
		})

		# if && or ||  with chars
		addRule(/^(\s*)if\s*\(c\s*([=!])=\s*'(.{1,2})'\s*(\|\||&&)\s*c\s*([=!])=\s*'(.{1,2})'\)\s*(\{|break\s*;|continue\s*;)$/, proc { |res|
			if(res[3] == '&&');	op = 'and';			else;				op = 'or';			end
			# condition2 = lookup_bool(res[1], false)
			puts "#{res[0]*2}((c #{'~' if res[1]=='!'}= #{convert_char(res[2])}) #{op}: [c #{'~' if res[4]=='!'}= #{convert_char(res[5])}]) ifTrue: ["
			if(res[6] == 'break;')
				puts "#{res[0]*2}    Break break."
				puts "#{res[0]*2}]."
			elsif(res[6] == 'continue;')
				puts "#{res[0]*2}    Continue continue."
				puts "#{res[0]*2}]."
			else
				@stack << 1
			end
			true 
		})

		# if &&
		addRule(/^(\s*)if\s*\(c\s*!=\s*-1 && (.+)\)\s*\{$/, proc { |res|
			condition2 = lookup_bool(res[1], false)
			puts "#{res[0]*2}((c ~= -1) and: [#{condition2}]) ifTrue: ["
			@stack << 1
			true 
		})

		# if compare c
		addRule(/^(\s*)if\s*\(c\s*([=!])=\s*(.*)\)\s*\{$/, proc { |res|
			operand2 = lookup_part(res[2], false)
			puts "#{res[0]*2}(c #{'~' if res[1] == '!'}= #{operand2}) ifTrue: ["
			@stack << 1
			true 
		})

		# if nonzero c
		addRule(/^(\s*)if\s*\(c\)\s*\{$/, proc { |res|
			puts "#{res[0]*2}(c ~= 0) ifTrue: ["
			@stack << 1
			true 
		})

		# if lex_state_p
		addRule(/^(\s*)if\s*\(\s*lex_state_p\((EXPR_.+)\)\s*\) \{$/, proc { |res|
			puts "#{res[0]*2}(self stateP: #{convert_expr(res[1])}) ifTrue: [";
			@stack << 1; true
		})

		# if cmdState / kw
		addRule(/^(\s*)if\s*\(\s*(cmd_state|kw)\s*\)\s*\{$/, proc { |res|
			puts "#{res[0]*2}(#{lookup_part(res[1])}) ifTrue: [";
			@stack << 1; true
		})

		# if bool method
		addRule(/^(\s*)if\s*\(\s*(.*)\s*\)\s*\{$/, proc { |res|
			cond = lookup_bool(res[1], false)
			@stack << 1;
			puts "#{res[0]*2}(#{cond}) ifTrue: ["; true
		})

		# provizorni if
		addRule(/^(\s*)if\s*\(.*$/, proc { |res|	@stack << 1;   false })

		# else if
		addRule(/^(\s*)\}\s*else\s*if\s*\(c\s*==\s*'(.{1,2})'\)\s*\{$/, proc { |res|
		  puts "#{res[0]*2}] ifFalse: [ (c = #{convert_char(res[1])}) ifTrue: ["
			@stack.push(@stack.pop + 1);
			true
		})

		addRule(/^(\s*)\}\s*else if\s*\((.*)\) \{$/, proc { |res|
			if(condition = lookup_bool(res[1], false))
				puts "#{res[0]*2}] ifFalse: [ (#{condition}) ifTrue: ["
				@stack.push(@stack.pop + 1);   true
			else
				false
			end
		})

		addRule(/^(\s*)\}\s*else if\s*\((.*)\s*==\s*(.*)\) \{$/, proc { |res|
			if((condition1 = lookup_part(res[1], true)) && (condition2 = lookup_part(res[2], true)))
				puts "#{res[0]*2}] ifFalse: [ (#{condition1} = #{condition2}) ifTrue: ["
				@stack.push(@stack.pop + 1);   true
			else
				false
			end
		})

		# else if
		addRule(/^(\s*)\}\s*else if\s*\(.*$/, proc { |res|	@stack.push(@stack.pop + 1);   false })

		# else
		addRule(/^(\s*)\}\s*else\s*\{$/, proc { |res|	puts "#{res[0]*2}] ifFalse: [";   true })

		# begin block
		addRule(/^(\s*)\{\s*$/, proc { |res|	 puts "#{res[0]*2}[";  @stack.push(0);   true })

		# end if or end block
		addRule(/^(\s*)\}$/, proc { |res|
			if(@stack.size == 0)
				STDERR.puts 'Error: Too many closing parenthesis!'
				raise TranslateException
			end
			if @stack.last == 0
				puts "#{res[0]*2}] value."
				@stack.pop
			elsif @stack.last == -1 # provizorni switch
				@stack.pop
				raise TranslateException
			else
				puts "#{res[0]*2}#{'].'*@stack.pop}"
			end
			true
		})

		# provizorni do while
		# addRule(/^(\s*)\}\s*while\s*\(.*\)\s*\{$/, proc { |res|	@stack << -1;   false })		

		# provizorni switch
		addRule(/^(\s*)switch\s*\(.*\)\s*\{$/, proc { |res|	@stack << -1;   false })

		# assign expr
		addRule(/^(\s*)lex_state = (EXPR_\S+);$/, proc { |res|	puts "#{res[0]*2}state := #{lookup_part(res[1], false)}.";   true })

		# assign token to c
		addRule(/^(\s*)c = (modifier_|keyword_|t[^;]+);$/, proc { |res|	puts "#{res[0]*2}c := TokenType #{res[1]}.";   true })		

		# assignment
		addRule(/^(\s*)(c|mb|nondigit|result) = (.*);\s*(\/\*(.*)\*\/)?$/, proc { |res|	puts "#{res[0]*2}#{res[1]} := #{lookup_part(res[2], false)}.";   true })		
		addRule(/^(\s*)last_state = lex_state;$/, proc { |res|	puts "#{res[0]*2}lastState := state.";   true })		
		addRule(/^(\s*)lpar_beg = (.*);\s*$/, proc { |res|	puts "#{res[0]*2}lparBeg := #{resolve_number(res[1])}.";   true })

		# bool assignment
		addRule(/^(\s*)(\S+) = (0|1|FALSE|TRUE|false|true);\s*$/, proc { |res|
			puts "#{res[0]*2}#{lookup_boolean_var(res[1])} := #{(res[2]=='1' || res[2].downcase == 'true' ? 'true' : 'false' )}.";   true })

		# return
		addRule(/^(\s*)return c;$/, proc { |res|	puts "#{res[0]*2}^ c";   true })
		addRule(/^(\s*)return (modifier_|keyword_|t[^;]+);$/, proc { |res|
			puts "#{res[0]*2}^ TokenType #{res[1]}"
			true
		})
		addRule(/^(\s*)return '(.{1,2})';$/, proc { |res|
			puts "#{res[0]*2}^ #{convert_char(res[1])}"
			true
		})
		addRule(/^(\s*)return (-?\d+);$/, proc { |res|
			puts "#{res[0]*2}^ #{res[1]}"
			true
		})
		# universal return
		addRule(/^(\s*)return (.*);$/, proc { |res|
			puts "#{res[0]*2}^ #{lookup_part(res[1])}"
			true
		})

		# ostatni jednoducha pravidla

		# pushback
		addRule(/^(\s*)pushback\(c\);$/, proc { |res|	puts "#{res[0]*2}self pushback.";   true })
		addRule(/^(\s*)pushback\((.*)\);$/, proc { |res|	puts "#{res[0]*2}self pushback: #{lookup_part(res[1], false)}.";   true })

		addRule(/^(\s*)tokadd\('(.{1,2})'\);$/, proc { |res|	puts "#{res[0]*2}tokenBuilder add: #{convert_char(res[1])}."; true })
		
		# lval
		addRule(/^(\s*)set_yylval_id\('(.{1,2})'\);$/, proc { |res|	puts "#{res[0]*2}lval id: #{convert_char(res[1])}.";   true })
		addRule(/^(\s*)set_yylval_id\((modifier_|keyword_|t[^;]+)\);$/, proc { |res|	puts "#{res[0]*2}lval id: (TokenType #{res[1]}).";   true })
		addRule(/^(\s*)set_yylval_name\((.*)\);$/, proc { |res|	puts "#{res[0]*2}lval name: #{lookup_part(res[1])}.";   true })

		addRule(/^(\s*)(--)?paren_nest(--)?;\s*$/, proc { |res|
			puts "#{res[0]*2}parenNest := parenNest - 1.";   true })

		# NEW_STRTERM
		addRule(/^(\s*)lex_strterm = NEW_STRTERM\(str_([^,]+),\s*'(.{1,2})', 0\);$/, proc { |res|
			puts "#{res[0]*2}strTerm := builder NEW_STRTERM: (StringType #{res[1]}) term: (#{convert_char(res[2])}) paren: nil positions: nil.";		true		})
		addRule(/(\s*)lex_strterm\s*=\s*NEW_STRTERM\(str_([^,]+),\s*term,\s*paren\);$/, proc { |res|
			puts "#{res[0]*2}strTerm := builder NEW_STRTERM: (StringType #{res[1]}) term: term paren: paren positions: nil.";		true		})
		

		# goto
		addRule(/^(\s*)goto (\S+);$/, proc { |res|	puts "#{res[0]*2}Goto block: #{snake_to_camel(res[1]) << 'Block'}.";   true })

		# lex_state = IS_AFTER_OPERATOR() ? EXPR_ARG : EXPR_BEG;
		addRule(/^(\s*)lex_state = IS_AFTER_OPERATOR\(\) \? (EXPR_\S+) : (EXPR_\S+);$/, proc { |res|
			puts "#{res[0]*2}(self isAfterOperator) ifTrue: ["
			puts "#{res[0]*2}    state := StateExpr #{res[1]}."
			puts "#{res[0]*2}] ifFalse: ["
			puts "#{res[0]*2}    state := StateExpr #{res[2]}."
			puts "#{res[0]*2}]."
			true
		})

		# goto_eol
		addRule(/^(\s*)lex_goto_eol\(parser_state\);$/, proc { |res|
			puts "#{res[0]*2}self lex_goto_eol.";   true })

		# warnings
		addRule(/^(\s*)warn_balanced\("(.*[^\\])", "(.*[^\\])"\);$/, proc { |res|
			puts "#{res[0]*2}self warnBalanced: '#{escape_string(res[1])}' syn: '#{escape_string(res[2])}'.";   true })
		addRule(/^(\s*)rb_warning\("(.*[^\\])"\);$/, proc { |res|
			puts "#{res[0]*2}self warning: '#{escape_string(res[1])}'.";   true })
		addRule(/^(\s*)rb_warning0\("(.*[^\\])"\);$/, proc { |res|
			puts "#{res[0]*2}self warning0: '#{escape_string(res[1])}'.";   true })
		addRule(/^(\s*)yy_error\s*\("(.*[^\\])"\);$/, proc { |res|
			puts "#{res[0]*2}self yyError: '#{escape_string(res[1])}'.";   true })
		addRule(/^(\s*)rb_compile_error\(parser_state, "(.*[^\\])"\);$/, proc { |res|
			puts "#{res[0]*2}self compileError: '#{escape_string(res[1])}'.";   true })
		addRule(/^(\s*)rb_warn\("(.*[^\\])", (\S+)\);$/, proc { |res|
			puts "#{res[0]*2}self warn: '#{escape_string(res[1])}' arg: #{lookup_part(res[2])}.";   true })

		# comment
		addRule(/^(\s*)\/\*(.*)\s*\*\/$/, proc { |res|
			puts "#{res[0]*2}\"#{escape_comment(res[1]).strip}\"";   true })

		# all other
		addRule(/^(\s*)(.*);$/, proc { |res|
			puts "#{res[0]*2}#{lookup_part(res[1], false)}.";   true })
	end

	def lookup_part(part, parenthesis = true)
		raise TranslateException if part == nil
		part.strip!
		if part[0] == '!'
			res = lookup_part(part[1..part.length])
			res = '(' + res.strip + ') not'
		elsif (res = resolve_number(part))
		elsif part.index('(') # is method
			res = lookup_method(part, parenthesis)
		else # is variable
			res = lookup_variable(part, parenthesis)
		end

		# surround with () when part has more than one word
		return "(#{res})" if parenthesis && (res.index(' '))
		return res
	end

	def escape_string(str)
		str.split('\'').join('\'\'')
	end

	def escape_comment(str)
		str.split('"').join('""')
	end

	def lookup_bool(name, parenthesis = true)
		raise TranslateException if name == nil
		name.strip!
		if name[0] == '!'
			res = lookup_part(name[1..name.length])
			res = '(' + res.strip + ') not'
		else
			begin
				res = lookup_bool_method(name, parenthesis)
			rescue TranslateException
				res = lookup_boolean_var(name, parenthesis)
			end
		end

		# surround with () when part has more than one word
		return "(#{res})" if parenthesis && (res.index(' '))
		return res
	end

	def lookup_method(name, parenthesis = true)
		name.strip!
		case name
		when "nextc()";				res = "self nextc"
		when "newtok()";			res = "tokenBuilder newToken"
		when "tokfix()";			res = "tokenBuilder fix"
		when "parser_is_identchar()";	    res = "self isIdentChar"
		when "tokadd(c)";			res = "tokenBuilder add: c"
		when "no_digits()";			res = "^ self noDigits"
		else
			raise TranslateException
		end
		return res
	end

	def lookup_bool_method(name, parenthesis = true)
		case name
		when "IS_BEG()";			res = "self isBeg"
		when "IS_END()";			res = "self isEnd"
		when "IS_SPCARG(c)";	res = "self isSpcarg"
		when "IS_ARG()";			res = "self isArg"
		when "IS_LABEL_POSSIBLE()";			res = "self isLabelPossible"
		when "IS_LABEL_SUFFIX(0)"; 			res = "self isLabelSuffix: 0"
		when "IS_AFTER_OPERATOR()";			res = "self isAfterOperator"
		when "ISDIGIT(c)";			res = "enc isDigit: c"
		when "parser_is_identchar()";			res = "self isIdentChar"
		else
			raise TranslateException
		end
		# surround with () when part has more than one word
		# return "(#{res})" if parenthesis && (res.index(' '))
		# return res
	end

	def lookup_variable(name, parenthesis = true)
		name.strip!
		case name
		when /^'\\?.'$/;		res = convert_char(name[1...name.length-1])
		when "c";			res = "c"
		when "c0";		res = "c0"
		when "c2";		res = "c2"
		when "parser_state->enc";		res = "enc"
		when "result";		res = "result"
		when "lex_state";		res = "state"
		when "ident";		res = "ident"
		when "cmd_state";		res = "cmdState"
		when "kw";		res = "kw"
		when "EXPR_MAX_STATE";   res = "StateBit #{name}"
		when /^EXPR_\S+_BIT$/;   res = "StateBit #{name}"
		when /^EXPR_[^_]*[^_T]$/;   res = "StateExpr #{name}"
		when "EXPR_DOT";   res = "StateExpr #{name}"
		when /^ENC_\S+$/;   res = "Encoding #{name}"
		when "kw";   res = "kw"
		when /^(t|keyword_|modifier_)\S+$/;						res = "TokenType #{name}"
		when "continue";			res = "Continue continue"
		when "break";			res = "Break break"
		else
			raise TranslateException
		end
		return res
	end

	def lookup_boolean_var(name, parenthesis = true)
		case name
		#when 'space_seen'; return 'spaceSeen'
		#when 'command_start'; return 'commandStart'
		when 'eofp'; return 'eofp'
		when 'nondigit'; return 'nondigit ~= 0'
		when 'seen_e'; return 'seenE ~= 0'
		end
		raise TranslateException
	end

	def resolve_number(number)
		case number
		when /^0[xX][\dabcdefABCDEF]+$/
			return '16r' + number[2...number.length]
		when /^0\d+$/
			return '8r' + number[1...number.length]
		when /^-?\d+$/
			return number
		end
		false
	end

	# convert expression made using bitwise or |
	def convert_expr(exprs)
		raise TranslateException if(exprs.index('&'))

		arr = exprs.split('|')
		res = '(' * arr.size
		res <<= "StateExpr #{arr[0].strip})"
		i = 1

		while(i < arr.size)
			res <<= " bitOr: (StateExpr #{arr[i].strip}))"
			i += 1
		end
		res
	end

	# convert snake case string to camel case
	def snake_to_camel(str)
		parts = str.split('_')
		res = parts[0]
		first = true
		parts.each do |part|
			if(first)
				first = false
			else
				res <<= part.capitalize
			end
		end
		res
	end

	def convert_char(c)
		return "(TokenType fromChar: $#{c})" if c.length == 1
		if c.length == 2
			return '(TokenType fromChar: $\')' if(c[0] == '\\' && c[1] == "'")
			return "(SpecialChar #{c[1]} asciiValue)"
		end
		return c
	end

	def translate(line)
		@rules.each do |rule|
			begin
				unless((res = rule[:regexp].match(line)) == nil)
					return true if rule[:block].call(res.captures)
				end
			rescue TranslateException
			end

		end
		return false
	end

	attr_accessor :stack

private

	def addRule(regexp, block)
		@rules << { regexp: regexp, block: block }
	end
end