
# nodes.rb - translate c++ code macro function definitions into smalltalk methods 

className = 'ASTBuilder'
category = 'creation'


class NodeTranslator
  def initialize(className, category)
    @writer = SmallTalkWriter.new(className, category)
  end

  def translate(file, start_line = 0, end_line = -1)
    line_number = 0
    # lines with inner function calls
    unsuccessful_lines = []

    file.each_line do |line|
      if(start_line <= line_number)
        break if end_line != -1 && end_line < line_number
        unsuccessful_lines << line unless translate_line(line)
      end
      line_number += 1
    end

    unsuccessful_lines.each do |line|
      puts line
    end
  end

private

  def translate_line(line)
    line = line[8, line.length] # remove '#define ' part

    # Example lines:  NEW_IFUNC(f,c)          NEW_NODE(NODE_IFUNC,f,c,0)
    #                 NEW_DEFS(r,i,a,d)   NEW_NODE(NODE_DEFS,r,i,NEW_SCOPE(a,d))

    return false if((res = /(\w+)\(([^\)]*)\)\s+(\w*)\((.+)\)$/.match(line)) == nil)

    captures = res.captures

    input_params = captures[1].split(',')
    input_params = [] unless input_params.size > 0
    output_params = captures[3].split(',')

# for basic input lines should be if, for incomplete translation use unless
    return false unless(output_params.size != 4 || captures[2] != 'NEW_NODE')
    
    @writer.begin_method(captures[0], input_params)
    @writer.method_body(captures[2], ["t", "a0", "a1", "a2"], output_params)
    @writer.end_method

    puts @writer.build_string

    return true
  end
end

class SmallTalkWriter
  def initialize(class_name, category, indentation = 4)
    @class_name = class_name
    @category = category
    @result = ""
    @indentation = indentation
    @author = ' ' * indentation
    @author <<= "\"Created: / 15-03-2015 / 11:51:18 / hhyperion <fajmaji1@fit.cvut.cz>\"\n"
    @author <<= ' ' * indentation
    @author <<= "\"Modified: / 15-03-2015 / 15:48:39 / hhyperion <fajmaji1@fit.cvut.cz>\"\n"
  end

  def begin_method(name, params)
    str_params = ""
    if(params.count > 0)
      first = true
      params.each do |param|
        if(first)
          first = false
          str_params <<= ": #{param}"
        else
          str_params <<= " #{param}: #{param}"
        end
      end
    end

    @result <<= <<-END

!#{@class_name} methodsFor: '#{@category}'!

#{name}#{str_params}
END
  end

  def method_body(method_name, param_names, param_values)
    @result <<= ' ' * @indentation
    @result <<= "^ self #{method_name}: "
    i = 0
    if(param_names.size == param_values.size)
      param_names.each do |name|
        if(i > 0)
          @result <<= " #{name}: "
        else
          @result <<= "\#"
        end
        @result <<= (param_values[i] == '0' ? 'nil' : param_values[i] )
        i += 1
      end
    else
      @result <<= param_values.join(',')
    end
    @result <<= ".\n\n#{@author}"
  end

  def end_method
    @result <<= <<-END
! !
    END
  end

  def build_string
    result = @result
    @result = ""
    result
  end
end


file = File.open(ARGV[0])
nt = NodeTranslator.new(className, category)
nt.translate(file, 170, 277)

#nt.translate(file, 170, 175)

# nt.translate(file, 250, 258)
