
# nodes.rb - translate c++ code macro function definitions into smalltalk methods 

className = 'Ruby::LexemPrinter'
category = 'enum TokenType'
method_name_prefix = 'token_'


class NodeTranslator
  def initialize(className, category, method_name_prefix = '')
    @writer = SmallTalkWriter.new(className, category)
    @method_name_prefix = method_name_prefix
  end

  def translate(file, start_line = 0, end_line = -1)
    line_number = 0
    # lines with inner function calls
    unsuccessful_lines = []

    puts '"{ Encoding: utf8 }" ! '

    file.each_line do |line|
      if(start_line <= line_number)
        break if end_line != -1 && end_line < line_number
        unsuccessful_lines << line unless translate_line(line)
      end
      line_number += 1
    end

    # unsuccessful_lines.each do |line|
    #   puts line
    # end
  end

private

  def translate_line(line)
#    line = line[8, line.length] # remove '#define ' part

    return false if((res = /([^ ]+) = (\d+)/.match(line)) == nil)

    captures = res.captures

    input_params = captures[1].split(',')
    input_params = []

# for basic input lines should be if, for incomplete translation use unless
#   return false unless(output_params.size != 4 || captures[2] != 'NEW_NODE')
    
    @writer.begin_method(@method_name_prefix + captures[1], [])
    @writer.method_body(captures[2], "'" + captures[0] + "'")
    @writer.end_method

    puts @writer.build_string

    return true
  end
end

class SmallTalkWriter
  def initialize(class_name, category, indentation = 4)
    @class_name = class_name
    @category = category
    @result = ""
    @indentation = indentation
    @author = ' ' * indentation
    @author <<= "\"Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>\"\n"
  end

  def begin_method(name, params)
    str_params = ""
    if(params.count > 0)
      first = true
      params.each do |param|
        if(first)
          first = false
          str_params <<= ": #{param}"
        else
          str_params <<= " #{param}: #{param}"
        end
      end
    end

    @result <<= <<-END

!#{@class_name} class methodsFor: '#{@category}'!

#{name}#{str_params}
END
  end

  def method_body(method_name, return_val)
    @result <<= ' ' * @indentation
    @result <<= "^ #{return_val}"
    @result <<= "\n\n#{@author}"
  end

  def end_method
    @result <<= <<-END
! !
    END
  end

  def build_string
    result = @result
    @result = ""
    result
  end
end


file = File.open(ARGV[0])
nt = NodeTranslator.new(className, category, method_name_prefix)
nt.translate(file, 0)

#nt.translate(file, 170, 175)

# nt.translate(file, 250, 258)
