module STCC
  class Printer
    class << self
      @fatal_errors ||= []
      @verbose = false

      def fatal_error(msg)
        # raise FatalError.new(msg)
        STDERR.puts "\033[00;31mwarning: #{msg}\033[00m"
      end

      def error(msg)
        @errors ||= []
        @errors << msg
      end

      def warn(msg)
        warning msg
      end

      def warning(msg)
        @warnings ||= []
        @warnings << msg
        STDERR.puts "\033[02;33mwarning: #{msg}\033[00m"
      end

      def log(msg)
        @logs ||= []
        @logs << msg
        STDERR.puts "\033[02;33mlog: #{msg}\033[00m"
      end

      # debug shortcut
      def d(msg)
        puts "\033[00;31m#{msg}\033[00m\n" if @verbose
      end

      def debug(msg)
        puts "\033[00;31mdebug: #{msg}\033[00m" if @verbose
      end

      def empty?
        return (!@errors || @errors.length == 0) && (!@warnings || @warnings.length == 0) && (!@logs || @logs.length == 0)
      end

      def print_all
        unless @fatal_error_msg.nil?
          STDERR.puts "\033[01;31mERROR: #{@fatal_error_msg}\033[00m"
        end

        unless @errors.nil?
          @errors.each do |msg|
            STDERR.puts "\033[00;31mERROR: #{msg}\033[00m"
          end
          @errors = []
        end
        unless @warnings.nil?
          @warnings.each do |msg|
            STDERR.puts "\033[02;33mwarning: #{msg}\033[00m"
          end
          @warnings = []
        end
        unless @logs.nil?
          @logs.each do |msg|
            STDERR.puts "\033[00;34mlog: #{msg}\033[00m"
          end
          logs = []
        end
      end

      attr_accessor :fatal_error_msg, :verbose
      attr_reader :errors, :warnings, :logs
    end
  end
end