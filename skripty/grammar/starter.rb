
className = 'TestingParser'
category = 'grammar'

usage = 'usage: ./run.sh [--verbose] <project_name>'

if ARGV.length == 0
  puts usage
  exit -1
end

module STCC

  require_relative 'translator.rb'

  puts "args: #{ARGV.length}"

  if ARGV.length == 2
    if ARGV[0] == '--verbose'
      Printer.verbose = true
    else
      puts usage
      exit -1
    end
    project_name = ARGV[1]
  else
    project_name = ARGV[0]
  end

  input_folder = 'data'
  output_folder = 'out'
  export_ps_enabled = true # PostScript image
  clean = false

  unless File.exist?("#{input_folder}/#{project_name}.y")
    Printer.fatal_error "File not found: #{input_folder}/#{project_name}.y"
  end

  file = File.open("#{input_folder}/#{project_name}.y")

  # nt = STCC::NodeTranslator.new(className, category)
  # nt.notifications_enabled = true
  # # true if symbol with given name is a terminal
  # nt.is_terminal_function = proc { |name|
  #   (/^(t[A-Z]|(keyword_)|(modifier_)|').*$/.match(name) != nil) && name != 'keyword_variable'
  # }
  # nt.translate(file)

  puts "Building project \"#{project_name}\"\n\n"

  bison_parser = BisonParser.new
  bison_parser.change_state(bison_parser.states[:parserDeclarationsState])
  grammar = bison_parser.parse(file)

  # if grammar.nonterminals['keyword_variable']
  #   Printer.fatal_error "NASEL JSEM nonterminal keyword_variable "
  #   exit
  # end

  puts 'building parser from grammar...'
  parser_builder = BottomUpParserBuilder.new
  result_parser = parser_builder.build(grammar)
  states = parser_builder.states
  # Printer.log result_parser.grammar.nonterminals['primary'].follow.map{|s| s.name}.join ', '
  # result_parser.states[9].dump_action_table

  if clean
    puts 'cleaning project files...'
    system "rm #{output_folder}/#{project_name}.*"
  end

  puts 'exporting parser to SmallTalk...'
  parser_exporter = SmallTalkBUParserExporter.new(result_parser)
  parser_exporter.debug = true
  parser_exporter.generate_actions_template("#{output_folder}/#{project_name}.cfg.dist")

  if File.exist?("#{input_folder}/#{project_name}.cfg")
    parser_exporter.translate_actions_from_file("#{input_folder}/#{project_name}.cfg")
  else
    puts " - action translations file not found: #{input_folder}/#{project_name}.cfg"
  end

  puts('exporting state diagram to dot...')
  exporter = StatesExporterDot.new
  exporter.export(states, "#{output_folder}/#{project_name}.dot")

  if export_ps_enabled
    puts('exporting state diagram to PS...')
    max_states_for_ps = 100
    if states.length <= max_states_for_ps
      system 'bash', 'scripts/print_dot.sh', "#{output_folder}/#{project_name}.dot", "#{output_folder}/#{project_name}.ps"
    else
      puts " - skipped, cannot print automaton with more than #{max_states_for_ps} to ps"
    end
  end


  parser_exporter.export_parser("#{output_folder}/#{project_name}.st")


  puts("done\n\nOutput is in folder \"#{output_folder}\"")

end