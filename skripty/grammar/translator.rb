# nodes.rb - translate bison .y file grammar into smalltalk grammar

module STCC
  $max_unsuccessful_lines = 2


  require_relative 'printer.rb'
  require_relative 'bison_parser/bison_parser'
  require_relative 'parser_builder/bottom_up_parser_builder'
  require_relative 'parser_builder/exporter/states_dot_exporter'
  require_relative 'parser_exporter/smalltalk_bu_parser_exporter'

  class TranslateException < Exception
  end
  class EndOfInput < Exception
  end
  class FatalError < Exception
    def initialize(message)
      @message = message
    end

    attr_reader :message
  end

  class NodeTranslator
    def initialize(_class_name, _category, parser_builder = BottomUpParserBuilder.new)
      @grammar_parser = BisonParser.new
      @parser_builder = parser_builder
      @notifications_enabled = false
    end

    def translate(input, start_line = 0, end_line = -1)
      grammar = @grammar_parser.parse(input, start_line, end_line)
      if @grammar_parser.unsuccessful_lines.length > 0
        print_stats(@grammar_parser.unsuccessful_lines)
        exit
      end

      parser = @parser_builder.build(grammar)
      unless Printer.empty?
        Printer.print_all
        exit if (Printer.errors && Printer.errors.length > 0)
      end

p grammar.nonterminals['program'].rules[0].symbols.map { |s| s.name}.join ' '
      parser
    end

    def is_terminal_function=(func)
      @grammar_parser.is_terminal_function = func
    end

    attr_accessor :notifications_enabled
    attr_reader :parser

    private

    def print_stats(unsuccessful_lines)
      if unsuccessful_lines.length == 0
        if @notifications_enabled
          STDERR.puts "\033[00;32m-------------------------------------------------------\033[00m"
          STDERR.puts "\033[00;32m#{@lines_processed + 1} lines, #{unsuccessful_lines.length} failures\033[00m"
        end
      else
        STDERR.puts "\033[00;31m-------------------------------------------------------\033[00m"
        unsuccessful_lines.each do |line|
          STDERR.puts("\033[00;31m" + line.strip + "\033[00m")
        end
        STDERR.puts ''
        STDERR.puts "\033[00;31m#{@lines_processed + 1} lines, #{unsuccessful_lines.length} failures\033[00m"
      end

      Printer.print_all
    end
  end
end