primary   : k_for for_var keyword_in
            {COND_PUSH(1);}
            expr_value do
            {COND_POP();}
            compstmt
            k_end
            {
              /*
               *  for a, b, c in e
               *  #=>
               *  e.each{|*x| a, b, c = x
               *
               *  for a in e
               *  #=>
               *  e.each{|x| a, = x}
               */
              $$ = NEW_FOR($2, $5, $8);
              fixpos($$, $2);
            }
          | k_class cpath superclass
            {
              if(in_def || in_single)
                yy_error("class definition in method body");
              class_nest++;
              local_push(0);
              $<num>$ = sourceline;
            }
            bodystmt
          ;
%%


primary   : k_for for_var keyword_in
            {COND_PUSH(1);}
            expr_value do
            {COND_POP();}
            compstmt
            k_end
            {
              /*
               *  for a, b, c in e
               *  #=>
               *  e.each{|*x| a, b, c = x
               *
               *  for a in e
               *  #=>
               *  e.each{|x| a, = x}
               */
              $$ = NEW_FOR($2, $5, $8);
              fixpos($$, $2);
            }
          | k_class cpath superclass
            {
              if(in_def || in_single)
                yy_error("class definition in method body");
              class_nest++;
              local_push(0);
              $<num>$ = sourceline;
            }
            bodystmt