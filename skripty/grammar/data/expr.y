
%{
#define YYSTYPE double
#include <math.h>
#include <stdio.h>
%}

/* BISON Declarations */
%token tINTEGER
%left '+'

/* Grammar follows */
%%
program         : arg
                | /* empty */
                ;

arg             : arg '+' arg { @@ := @1 + @3. }
                | tINTEGER
                ;

%%

/* Lexical analyzer returns a double floating point
 *    number on the stack and the token NUM, or the ASCII
 *       character read if not a number.  Skips all blanks
 *          and tabs, returns 0 for EOF. */

#include <ctype.h>

yyerror (s)  /* Called by yyparse on error */
             char *s;
{
          printf ("%s\n", s);
}

yylex ()
{
	  int c;

	    /* skip white space  */
	    while ((c = getchar ()) == ' ' || c == '\t')  
		        ;
	      /* process numbers   */
	      if (c == '.' || isdigit (c))                
		          {
				        ungetc (c, stdin);
					      scanf ("%lf", &yylval);
					            return tINTEGER;
						        }
	        /* return end-of-file  */
	        if (c == EOF)                            
			    return 0;
		  /* return single chars */
		  return c;                                
}

main ()
{
	  yyparse ();
}

