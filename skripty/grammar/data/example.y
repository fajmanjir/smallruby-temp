
%token
  keyword_class
  keyword_module
  keyword_def
  keyword_undef
  keyword_begin
  keyword_rescue
  keyword_ensure
  keyword_end
  keyword_if
  keyword_unless
  keyword_then
  keyword_elsif
  keyword_else
  keyword_case
  keyword_when
  keyword_while
  keyword_until
  keyword_for
  keyword_break
  keyword_next
  keyword_redo
  keyword_retry
  keyword_in
  keyword_do
  keyword_do_cond
  keyword_do_block
  keyword_do_LAMBDA
  keyword_return
  keyword_yield
  keyword_super
  keyword_self
  keyword_nil
  keyword_true
  keyword_false
  keyword_and
  keyword_or
  keyword_not
  modifier_if
  modifier_unless
  modifier_while
  modifier_until
  modifier_rescue
  keyword_alias
  keyword_defined
  keyword_BEGIN
  keyword_END
  keyword__LINE__
  keyword__FILE__
  keyword__ENCODING__

%token <id>   tIDENTIFIER tFID tGVAR tIVAR tCONSTANT tCVAR tLABEL
%token <node> tINTEGER tFLOAT tRATIONAL tIMAGINARY tSTRING_CONTENT tCHAR
%token <node> tNTH_REF tBACK_REF
%token <num>  tREGEXP_END

%type <node> singleton strings string string1 xstring regexp
%type <node> string_contents xstring_contents regexp_contents string_content
%type <node> words symbols symbol_list qwords qsymbols word_list qword_list qsym_list word
%type <node> literal numeric simple_numeric dsym cpath
%type <node> top_compstmt top_stmts top_stmt
%type <node> bodystmt compstmt stmts stmt_or_begin stmt expr arg primary command command_call method_call
%type <node> expr_value arg_value primary_value fcall
%type <node> if_tail opt_else case_body cases opt_rescue exc_list exc_var opt_ensure
%type <node> args call_args opt_call_args
%type <node> paren_args opt_paren_args args_tail opt_args_tail block_args_tail opt_block_args_tail
%type <node> command_args aref_args opt_block_arg block_arg var_ref var_lhs
%type <node> command_asgn mrhs mrhs_arg superclass block_call block_command
%type <node> f_block_optarg f_block_opt
%type <node> f_arglist f_args f_arg f_arg_item f_optarg f_marg f_marg_list f_margs
%type <node> assoc_list assocs assoc undef_list backref string_dvar for_var
%type <node> block_param opt_block_param block_param_def f_opt
%type <node> f_kwarg f_kw f_block_kwarg f_block_kw
%type <node> bv_decls opt_bv_decl bvar
%type <node> lambda f_larglist lambda_body
%type <node> brace_block cmd_brace_block do_block lhs none fitem
%type <node> mlhs mlhs_head mlhs_basic mlhs_item mlhs_node mlhs_post mlhs_inner
%type <id>   fsym keyword_variable user_variable sym symbol operation operation2 operation3
%type <id>   cname fname op f_rest_arg f_block_arg opt_f_block_arg f_norm_arg f_bad_arg
%type <id>   f_kwrest f_label

%token tUPLUS           /* unary+ */
%token tUMINUS          /* unary- */
%token tPOW             /* ** */
%token tCMP             /* <=> */
%token tEQ              /* == */
%token tEQQ             /* === */
%token tNEQ             /* != */
%token tGEQ             /* >= */
%token tLEQ             /* <= */
%token tANDOP tOROP     /* && and || */
%token tMATCH tNMATCH   /* =~ and !~ */
%token tDOT2 tDOT3      /* .. and ... */
%token tAREF tASET      /* [] and []= */
%token tLSHFT tRSHFT    /* << and >> */
%token tCOLON2          /* :: */
%token tCOLON3          /* :: at EXPR_BEG */
%token <id> tOP_ASGN    /* +=, -=  etc. */
%token tASSOC           /* => */
%token tLPAREN          /* ( */
%token tLPAREN_ARG      /* ( */
%token tRPAREN          /* ) */
%token tLBRACK          /* [ */
%token tLBRACE          /* { */
%token tLBRACE_ARG      /* { */
%token tSTAR            /* * */
%token tDSTAR           /* ** */
%token tAMPER           /* & */
%token tLAMBDA          /* -> */
%token tSYMBEG tSTRING_BEG tXSTRING_BEG tREGEXP_BEG tWORDS_BEG tQWORDS_BEG tSYMBOLS_BEG tQSYMBOLS_BEG
%token tSTRING_DBEG tSTRING_DEND tSTRING_DVAR tSTRING_END tLAMBEG

/*
 *      precedence table
 */

%nonassoc tLOWEST
%nonassoc tLBRACE_ARG

%nonassoc  modifier_if modifier_unless modifier_while modifier_until
%left  keyword_or keyword_and
%right keyword_not
%nonassoc keyword_defined
%right '=' tOP_ASGN
%left modifier_rescue
%right '?' ':'
%nonassoc tDOT2 tDOT3
%left  tOROP
%left  tANDOP
%nonassoc  tCMP tEQ tEQQ tNEQ tMATCH tNMATCH
%left  '>' tGEQ '<' tLEQ
%left  '|' '^'
%left  '&'
%left  tLSHFT tRSHFT
%left  '+' '-'
%left  '*' '/' '%'
%right tUMINUS_NUM tUMINUS
%right tPOW
%right '!' '~' tUPLUS

%token tLAST_TOKEN
%%
program         : stmt
                ;

stmt            : keyword_alias tGVAR tBACK_REF
                  {
                    @@ := astBuilder NEW_VALIAS: @2 o: @3 positions: nil.
                  }
                | stmt modifier_if expr_value
                  {
                    @@ := astBuilder NEW_IF: @3 t: @1 e: nil positions: nil.
                  }
                | expr
                ;

expr_value      : expr
                  {
                    (@@ = nil) ifTrue: [
                      @@ := astBuilder NEW_NIL: nil.
                    ].
                  }
                ;

expr            : expr keyword_and expr
                  {
                    @@ := astBuilder NEW_NODE: (NodeType NODE_AND) a0: @1 a1: @2 a2: nil positions: nil.
                  }
                | expr keyword_or expr
                  {
                    @@ := astBuilder NEW_NODE: (NodeType NODE_OR) a0: @1 a1: @2 a2: nil positions: nil.
                  }
                | arg
                ;

command_call    : /* TODO: */
                ;

arg             : arg '+' arg { @@ := astBuilder NEW_CALL: @1 m: (TokenType fromChar: $+) a: @3 positions: nil. }
                | arg '-' arg { @@ := astBuilder NEW_CALL: @1 m: (TokenType fromChar: $-) a: @3 positions: nil. }
                | arg '*' arg { @@ := astBuilder NEW_CALL: @1 m: (TokenType fromChar: $*) a: @3 positions: nil. }
                | arg '/' arg { @@ := astBuilder NEW_CALL: @1 m: (TokenType fromChar: $/) a: @3 positions: nil. }
                | arg '>' arg { @@ := astBuilder NEW_CALL: @1 m: (TokenType fromChar: $>) a: @3 positions: nil. }
                | arg '<' arg { @@ := astBuilder NEW_CALL: @1 m: (TokenType fromChar: $<) a: @3 positions: nil. }
                | arg tCMP arg { @@ := astBuilder NEW_CALL: @1 m: (TokenType fromChar: (TokenType tCMP)) a: @3 positions: nil. }
                | tLPAREN arg rparen { @@ := @2 }
                | numeric
                | lhs '=' arg
                  {
                    @1 nd_value: @3.
                    @@ := @1.
                  }
                ;

lhs             : user_variable
                  {
                    @@ := astBuilder NEW_LASGN: @1 val: nil positions: nil.
                  }
                ;

user_variable   : tIDENTIFIER { @@ := astBuilder NEW_LIT: (inputReader tokenBuilder buf) positions: nil. }
                ;

numeric         : simple_numeric
                ;

simple_numeric  : tINTEGER
                ;

rparen          : ')'
                ;
