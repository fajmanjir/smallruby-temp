
#!/bin/ruby
# -*- encoding : utf-8 -*-
require_relative 'spec_helper'
require_relative '../translator'

module STCC
  describe BisonParser do
    subject(:parser) { BisonParser.new }

    describe 'Parser' do
      it "verify nonterminal rules" do
        file = File.open('data/short.y')
        # parser.change_state(parser.states[:grammarRules])
        parser.parse(file)
        nonterminals = parser.grammar.nonterminals
        expect(nonterminals['program'].rules.length).to be 1
        expect(nonterminals['top_compstmt'].rules.length).to be 1
        expect(nonterminals['top_stmts'].rules.length).to be 4
        expect(nonterminals['top_stmt'].rules.length).to be 2
        expect(nonterminals['bodystmt'].rules.length).to be 1
        expect(nonterminals['compstmt'].rules.length).to be 1
        expect(nonterminals['stmts'].rules.length).to be 4
        expect(nonterminals['stmt_or_begin'].rules.length).to be 2
        expect(nonterminals['stmt'].rules.length).to be 23
        expect(nonterminals['command_asgn'].rules.length).to be 2
        expect(nonterminals['expr'].rules.length).to be 6
        expect(nonterminals['expr_value'].rules.length).to be 1
        expect(nonterminals['command_call'].rules.length).to be 2
        expect(nonterminals['block_command'].rules.length).to be 2
        expect(nonterminals['block_command'].rules.length).to be 2
        expect(nonterminals['cmd_brace_block'].rules.length).to be 1
        expect(nonterminals['fcall'].rules.length).to be 1
        expect(nonterminals['command'].rules.length).to be 11
        expect(nonterminals['mlhs'].rules.length).to be 2
        expect(nonterminals['mlhs_inner'].rules.length).to be 2
        expect(nonterminals['mlhs_basic'].rules.length).to be 10
        expect(nonterminals['mlhs_item'].rules.length).to be 2
        expect(nonterminals['mlhs_head'].rules.length).to be 2
        expect(nonterminals['mlhs_post'].rules.length).to be 2
        expect(nonterminals['mlhs_node'].rules.length).to be 9
        expect(nonterminals['lhs'].rules.length).to be 9
        expect(nonterminals['cname'].rules.length).to be 2
        expect(nonterminals['cpath'].rules.length).to be 3
        expect(nonterminals['fname'].rules.length).to be 5
        expect(nonterminals['fsym'].rules.length).to be 2
        expect(nonterminals['fsym'].rules.length).to be 2
        expect(nonterminals['fitem'].rules.length).to be 2
        expect(nonterminals['undef_list'].rules.length).to be 2
        expect(nonterminals['op'].rules.length).to be 30
        expect(nonterminals['reswords'].rules.length).to be 41
        expect(nonterminals['arg'].rules.length).to be 44
        expect(nonterminals['arg_value'].rules.length).to be 1
        expect(nonterminals['aref_args'].rules.length).to be 4
        expect(nonterminals['paren_args'].rules.length).to be 1
        expect(nonterminals['opt_paren_args'].rules.length).to be 2
        expect(nonterminals['opt_call_args'].rules.length).to be 5
        expect(nonterminals['call_args'].rules.length).to be 5
        expect(nonterminals['call_args'].rules.length).to be 5
        expect(nonterminals['command_args'].rules.length).to be 1
        expect(nonterminals['block_arg'].rules.length).to be 1
        expect(nonterminals['opt_block_arg'].rules.length).to be 2
        expect(nonterminals['args'].rules.length).to be 4
        expect(nonterminals['args'].rules.length).to be 4
        expect(nonterminals['mrhs_arg'].rules.length).to be 2
        expect(nonterminals['mrhs'].rules.length).to be 3

        primary_rules = nonterminals['primary'].rules
        expect(primary_rules[0].symbols.length).to be 1
        expect(primary_rules[0].symbols[0].name).to eq 'literal'
        expect(primary_rules[1].symbols.length).to be 1
        expect(primary_rules[1].symbols[0].name).to eq 'strings'
        expect(primary_rules[2].symbols.length).to be 1
        expect(primary_rules[2].symbols[0].name).to eq 'xstring'
        expect(primary_rules[3].symbols.length).to be 1
        expect(primary_rules[3].symbols[0].name).to eq 'regexp'
        expect(primary_rules[4].symbols.length).to be 1
        expect(primary_rules[4].symbols[0].name).to eq 'words'
        expect(primary_rules[5].symbols.length).to be 1
        expect(primary_rules[5].symbols[0].name).to eq 'qwords'
        expect(primary_rules[6].symbols.length).to be 1
        expect(primary_rules[6].symbols[0].name).to eq 'symbols'
        expect(primary_rules[7].symbols.length).to be 1
        expect(primary_rules[7].symbols[0].name).to eq 'qsymbols'
        expect(primary_rules[8].symbols.length).to be 1
        expect(primary_rules[8].symbols[0].name).to eq 'var_ref'
        expect(primary_rules[9].symbols.length).to be 1
        expect(primary_rules[9].symbols[0].name).to eq 'backref'
        expect(primary_rules[10].symbols.length).to eq 2
        expect(primary_rules[10].symbols[0].name).to eq 'tFID'
        expect(primary_rules[11].symbols.length).to eq 5
        expect(primary_rules[12].symbols.length).to eq 4
        expect(primary_rules[13].symbols.length).to eq 5
        expect(primary_rules[14].symbols.length).to eq 4
        expect(primary_rules[15].symbols.length).to eq 4
        expect(primary_rules[16].symbols.length).to eq 3
        expect(primary_rules[17].symbols.length).to eq 4
        expect(primary_rules[18].symbols.length).to eq 4
        expect(primary_rules[19].symbols.length).to eq 2
        expect(primary_rules[20].symbols.length).to eq 5
        expect(primary_rules[21].symbols.length).to eq 4
        expect(primary_rules[22].symbols.length).to eq 2
        expect(primary_rules[23].symbols.length).to eq 7 # keyword_defined
        expect(primary_rules[24].symbols.length).to eq 5
        expect(primary_rules[25].symbols.length).to eq 4
        idx = 25
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 3 # fcall
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 1 # method_call
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 3 # method_call
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 3 # tLAMBDA
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 7 # k_if
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 7 # k_unless
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 8 # k_while
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 8 # k_until
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 6 # k_case
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 5 # k_case
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 10 # k_for
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 7 # k_class cpath
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 9 # k_class tLSHFT
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 6 # k_module
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 7 # k_def fname
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 10 # k_def singleton
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 2 # keyword_break
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 2 # keyword_next
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 2 # keyword_redo
        idx += 1; expect(primary_rules[idx].symbols.length).to eq 2 # keyword_retry

        expect(primary_rules.length).to be 46

        expect(nonterminals['opt_nl'].rules[0].symbols).to eq []
      end
    end
  end
end