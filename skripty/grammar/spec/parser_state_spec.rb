
#!/bin/ruby
# -*- encoding : utf-8 -*-
require_relative 'spec_helper'
require_relative '../bison_parser/states/parser_state'
require_relative '../bison_parser/states/parser_declarations'

module STCC
  describe ParserState do
    subject(:state) { ParserDeclarationsState.new(nil) }

  	describe 'symbols_to_a' do
      it 'start with char' do
        a = state.symbols_to_a("'=' tOP_ASGN")
        expect(a).to eq ["'='", "tOP_ASGN"]
      end

      it 'end with char' do
        a = state.symbols_to_a("tOP_ASGN ' '")
        expect(a).to eq ["tOP_ASGN", "' '"]
      end

      it 'comment inside' do
        a = state.symbols_to_a("tOP_ASGN /* a comment */ ' '")
        expect(a).to eq ["tOP_ASGN", "' '"]
      end
    end
  end
end