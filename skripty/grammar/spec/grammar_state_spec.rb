
#!/bin/ruby
# -*- encoding : utf-8 -*-
require_relative 'spec_helper'
require_relative '../translator'

module STCC
  describe BisonParser do
    subject(:parser) { BisonParser.new }

    describe 'read symbol' do
      it 'symbol multiline simple' do
        source = <<-END
mlhs_node     : {
                  $$ = attrset($1, $3);
                }
              ;
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['mlhs_node']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules.size).to eq 1
        action_symbol = nonterminal.rules[0].symbols[0]

        expect(action_symbol.class).to eq ActionSymbol
        expect(action_symbol.action.strip).to eq '$$ = attrset($1, $3);'
      end

      it 'symbol multiline multiple {}' do
        source = <<-END
mlhs_node     : {
                  if(a > b){
                    $$ = attrset($1, $3);
                  }
                }
              ;
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['mlhs_node']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules.size).to eq 1
        action_symbol = nonterminal.rules[0].symbols[0]

        expect(action_symbol.class).to eq ActionSymbol
      end

      it 'symbol singleline simple' do
        source = <<-END
mlhs_node     : { $$ = attrset($1, $3); }
              ;
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['mlhs_node']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules.size).to eq 1
        action_symbol = nonterminal.rules[0].symbols[0]

        expect(action_symbol.class).to eq ActionSymbol
        expect(action_symbol.action.strip).to eq '$$ = attrset($1, $3);'
      end
    end

    describe 'read more rules' do
      it 'two rules with |' do
        source = <<-END
top_stmts_exp   : none
                  {
                    $$ = NEW_BEGIN(0);
                  }
                | top_stmt something
                  {
                    $$ = newline_node($1);
                  }
                ;
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['top_stmts_exp']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules.size).to eq 2
        expect(nonterminal.rules[0].symbols[0].class).to eq NonterminalSymbol
        expect(nonterminal.rules[0].symbols[0].name).to eq 'none'
        expect(nonterminal.rules[0].symbols[1].class).to eq ActionSymbol
        expect(nonterminal.rules[0].symbols[1].action.strip).to eq '$$ = NEW_BEGIN(0);'

        expect(nonterminal.rules[1].symbols[0].class).to eq NonterminalSymbol
        expect(nonterminal.rules[1].symbols[0].name).to eq 'top_stmt'
        expect(nonterminal.rules[1].symbols[1].class).to eq NonterminalSymbol
        expect(nonterminal.rules[1].symbols[1].name).to eq 'something'
        expect(nonterminal.rules[1].symbols[2].class).to eq ActionSymbol
        expect(nonterminal.rules[1].symbols[2].action.strip).to eq '$$ = newline_node($1);'
      end

      it 'two rules with epsilon rule and comment' do
        source = <<-END
trailer         : /* none */
                | '\\n'
                | ','
                ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['trailer']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules[0].symbols).to eq []
        expect(nonterminal.rules[1].symbols.size).to eq 1
        expect(nonterminal.rules[2].symbols.size).to eq 1

        expect(nonterminal.rules[1].symbols[0].class).to eq TerminalSymbol
        expect(nonterminal.rules[2].symbols[0].class).to eq TerminalSymbol
      end

      it 'two nonterminal definitions' do
        source = <<-END
trailer         : /* none */
                | '\\n'
                | ','
                ;

top_compstmt    : top_stmts opt_terms
                  {
                    void_stmts($1);
                    $$ = $1;
                  }
                ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['trailer']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules[0].symbols).to eq []
        expect(nonterminal.rules[1].symbols.size).to eq 1
        expect(nonterminal.rules[2].symbols.size).to eq 1

        expect(nonterminal.rules[1].symbols[0].class).to eq TerminalSymbol
        expect(nonterminal.rules[2].symbols[0].class).to eq TerminalSymbol

        nonterminal = parser.grammar.nonterminals['top_compstmt']
        s = nonterminal.rules[0].symbols
        expect(s[0].class).to eq NonterminalSymbol
        expect(s[1].class).to eq NonterminalSymbol
        expect(s[2].class).to eq ActionSymbol

        expect(s[1].name).to eq 'opt_terms'
      end

      it 'two complex nonterminal definitions' do
        source = <<-END
program         : {
                    lex_state = EXPR_BEG;
                    local_push(0);
                    class_nest = 0;
                  }
                  top_compstmt
                  {
                    if($2 && !compile_for_eval) {
                      /* last expression should not be void */
                      if(nd_type($2) != NODE_BLOCK) {
                        void_expr($2);
                      } else {
                        NODE *node = $2;
                        while(node->nd_next) {
                          node = node->nd_next;
                        }
                        void_expr(node->nd_head);
                      }
                    }
                    top_node = block_append(top_node, $2);
                    class_nest = 0;
                    local_pop();
                  }
                ;

top_compstmt    : top_stmts opt_terms
                  {
                    void_stmts($1);
                    $$ = $1;
                  }
                ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['program']
        expect(nonterminal.class).to be NonterminalSymbol
        s = nonterminal.rules[0].symbols
        expect(s.size).to eq 3
        expect(s[0].class).to eq NonterminalSymbol # replaced with marker
        expect(s[1].class).to eq NonterminalSymbol
        expect(s[2].class).to eq ActionSymbol

        nonterminal = parser.grammar.nonterminals['top_compstmt']
        s = nonterminal.rules[0].symbols
        expect(s[0].class).to eq NonterminalSymbol
        expect(s[1].class).to eq NonterminalSymbol
        expect(s[2].class).to eq ActionSymbol

        expect(s[1].name).to eq 'opt_terms'
      end

      it 'more rules on one line' do
        source = <<-END
reswords        : keyword__LINE__ | keyword__FILE__ | keyword__ENCODING__
                | keyword_BEGIN | keyword_END
                ;
%%
END

        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['reswords']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules.length).to be 5
        expect(nonterminal.rules[0].symbols.map { |s| s.name }).to eq ['keyword__LINE__']
        expect(nonterminal.rules[1].symbols.map { |s| s.name }).to eq ['keyword__FILE__']
        expect(nonterminal.rules[2].symbols.map { |s| s.name }).to eq ['keyword__ENCODING__']
        expect(nonterminal.rules[3].symbols.map { |s| s.name }).to eq ['keyword_BEGIN']
        expect(nonterminal.rules[4].symbols.map { |s| s.name }).to eq ['keyword_END']
      end

      it 'right side with %prec' do
        source = <<-END
%left tLOWEST
%right r1 r2
%nonassoc 'D'
%%
command         : fcall %prec tLOWEST 'a'
                  {
                    $$ = $1;
                    $$->nd_args = $2;
                  }
                ;
%%
END
        parser.change_state(parser.states[:parserDeclarationsState])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['command']
        expect(nonterminal.class).to be NonterminalSymbol
        s = nonterminal.rules[0].symbols
        expect(s[0].class).to be NonterminalSymbol
        expect(s[1].class).to be TerminalSymbol
        expect(s[2].class).to be ActionSymbol
        expect(s.size).to eq 3

        expect(parser.grammar.terminals["'a'"].prec).to eq nil
        tLowest = parser.grammar.terminals['tLOWEST']
        expect(tLowest.prec.left?).to be true
        expect(tLowest.prec.right?).to be false
        expect(tLowest.prec.nonassoc?).to be false
        expect(tLowest.prec.priority > 0).to be true

        expect(parser.grammar.nonterminals['command'].rules[0].prec).to eq tLowest.prec
      end

      it 'right side default rule prec' do
        source = <<-END
%right 'a'
%left 'b'
%%
command         : 'a' fcall 'b'
                  {
                    $$ = $1;
                    $$->nd_args = $2;
                  }
                | fcall
                ;

fcall           : 'c'
                ;
%%
        END
        parser.change_state(parser.states[:parserDeclarationsState])
        parser.parse(source)

        grammar = parser.grammar
        expect(grammar.terminals["'a'"].prec.priority < grammar.terminals["'b'"].prec.priority).to be true
        expect(grammar.terminals["'a'"].prec.right?).to be true
        expect(grammar.nonterminals['command'].rules[0].prec).to eq grammar.terminals["'b'"].prec
        expect(grammar.nonterminals['command'].rules[1].prec).to eq nil
      end

      it 'right side with %prec' do
        source = <<-END
command         : fcall %prec 't'
                  {
                    $$ = $1;
                    $$->nd_args = $2;
                  }
                ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        nonterminal = parser.grammar.nonterminals['command']
        expect(nonterminal.class).to be NonterminalSymbol
        s = nonterminal.rules[0].symbols
        expect(s[0].class).to be NonterminalSymbol
        expect(s[1].class).to be ActionSymbol
        expect(s.size).to eq 2
      end

      it "terminal '\\n'" do
        file = File.open('data/newline.y')
        parser.change_state(parser.states[:grammarRules])
        parser.parse(file)

        nonterminal = parser.grammar.nonterminals['command']
        expect(nonterminal.class).to be NonterminalSymbol
        s = nonterminal.rules[0].symbols

        expect(s[0].class).to be TerminalSymbol
        expect(s[0].name).to eq "'\\n'"
        expect(s[1].class).to be TerminalSymbol
        expect(s[1].name).to eq "'\\n'"
        expect(s[2].class).to be TerminalSymbol
        expect(s[2].name).to eq "'a'"
        expect(s.size).to eq 3
      end

      it 'action containing multiline comment' do
        file = File.open('data/action_pipe.y')
        parser.change_state(parser.states[:grammarRules])
        parser.parse(file)

        nonterminal = parser.grammar.nonterminals['primary']
        expect(nonterminal.class).to be NonterminalSymbol
        expect(nonterminal.rules.length).to eq 2
        expect(nonterminal.rules[0].symbols[0].name).to eq 'k_for'
        expect(nonterminal.rules[0].symbols[9].class).to be ActionSymbol
        expect(nonterminal.rules[0].symbols.size).to eq 10
      end
    end
  end
end