
#!/bin/ruby
# -*- encoding : utf-8 -*-
require_relative 'spec_helper'
require_relative '../translator'

module STCC
  describe Grammar do
    subject(:parser) { BisonParser.new }

    describe 'compute first' do
      it 'without epsilon' do
        source = <<-END
program     : 'a' never
            | boolean never2
            ;

boolean     : 't'
            | 'f'
            ;

never       : 'X'
            ;

never2      : 'Y'
            ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        first = parser.grammar.nonterminals['program'].first.map { |s| s.name }.sort
        expect(first).to eq ["'a'", "'f'", "'t'", 'boolean']
        first = parser.grammar.nonterminals['boolean'].first.map { |s| s.name }.sort
        expect(first).to eq ["'f'", "'t'"]
        first = parser.grammar.nonterminals['never'].first.map { |s| s.name }.sort
        expect(first).to eq ["'X'"]
        first = parser.grammar.nonterminals['never2'].first.map { |s| s.name }.sort
        expect(first).to eq ["'Y'"]
      end

      it 'with epsilon' do
        source = <<-END
program     : 'b'
            | items
            ;

program_non_eps    : items 'e'
            ;

items       : /* empty */
            | items boolean
            ;

boolean     : 't'
            | 'f'
            ;

%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        first = parser.grammar.nonterminals['program'].first.map { |s| s.name }.sort
        expect(first).to eq ["'b'", "'f'", "'t'", EpsilonSymbol.get_instance.name, 'boolean', 'items']
        first = parser.grammar.nonterminals['program_non_eps'].first.map { |s| s.name }.sort
        expect(first).to eq ["'e'", "'f'", "'t'", 'boolean', 'items']
        first = parser.grammar.nonterminals['items'].first.map { |s| s.name }.sort
        expect(first).to eq ["'f'", "'t'", EpsilonSymbol.get_instance.name, "boolean", "items"]
      end

      it 'extend grammar with S\'' do
        source = <<-END
first_rule    : second_rule
              | 'x'
              ;

second_rule   : 'S'
              ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)

        grammar = parser.grammar
        expect(grammar.starting_symbol).not_to eq grammar.nonterminals['first_rule']
        expect(grammar.starting_symbol.rules[0].symbols.map { |s| s.name }.sort ).to eq ['first_rule', EpsilonSymbol.get_instance.name].sort
        expect(grammar.starting_symbol.first.sort).to eq (grammar.nonterminals['first_rule'].first | [(grammar.nonterminals['first_rule'])]).sort
        expect(grammar.starting_symbol.follow.sort).to eq [EpsilonSymbol.get_instance]
      end
    end

    describe 'compute follow' do
      it 'standard expression grammar' do
        source = <<-END
E2          : '+' T1 E2
            | /* empty */
            ;

T1          : F T2
            ;

T2          : '*' F T2
            | /* empty */
            ;

F           : '(' E2 ')'
            | 'i'
            ;

%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)
        
        grammar = parser.grammar
        expect(grammar.nonterminals['E2'].follow.map{ |s| s.name }.sort).to eq ["')'", EpsilonSymbol.get_instance.name].sort
        expect(grammar.nonterminals['T1'].follow.map{ |s| s.name }.sort).to eq ["')'", EpsilonSymbol.get_instance.name, "'+'"].sort
        expect(grammar.nonterminals['T2'].follow.map{ |s| s.name }.sort).to eq ["')'", EpsilonSymbol.get_instance.name, "'+'"].sort
        expect(grammar.nonterminals['F'].follow.map{ |s| s.name }.sort).to eq ["')'", EpsilonSymbol.get_instance.name, "'+'", "'*'"].sort
      end
    end

    describe 'eliminate epsilon productions' do
      it 'should convert actions before the end' do
        source = <<-END
A1          : 'a' { printf("action before 'b'");} 'b'
            ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)
        
        grammar = parser.grammar

        a1 = grammar.nonterminals['A1']
        expect(a1.rules.length).to eq 1
        expect(a1.rules[0].symbols[0].name).to eq "'a'"
        expect(a1.rules[0].symbols[2].name).to eq "'b'"
        marker = a1.rules[0].symbols[1]
        expect(marker.class).to eq NonterminalSymbol
        expect(marker.rules[0].symbols.length).to eq 1
        expect(marker.rules[0].symbols[0].class).to eq ActionSymbol
        expect(marker.rules[0].symbols[0].action.strip).to eq 'printf("action before \'b\'");'
        expect(marker.first).to eq [EpsilonSymbol.get_instance]
        expect(marker.follow.map{ |s| s.name }.sort).to eq ["'b'"]
      end

      it 'should not convert actions at the end' do
        source = <<-END
A2          : 'c' 'd' { $$ = 153; }
            ;
%%
END
        parser.change_state(parser.states[:grammarRules])
        parser.parse(source)
        
        grammar = parser.grammar

        a2 = grammar.nonterminals['A2']
        expect(a2.rules.length).to eq 1
        expect(a2.rules[0].symbols.length).to eq 3
        expect(a2.rules[0].symbols[2].class).to eq ActionSymbol
      end
    end
  end
end