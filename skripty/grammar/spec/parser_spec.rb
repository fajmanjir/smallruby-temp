
#!/bin/ruby
# -*- encoding : utf-8 -*-
require_relative 'spec_helper'
require_relative '../bison_parser/bison_parser'

module STCC
  describe ParserState do
    subject(:parser) { BisonParser.new }

    describe 'declarations' do
      it "%right '=' tOP_ASGN" do
        expect(parser.translate_line("%right '=' tOP_ASGN")).to eq true
      end
    end
  end
end