module STCC
  class TerminalSymbol < NonactionSymbol
    def to_s
      name
    end

    def ==(symbol)
      return false if symbol.class != self.class
      return @name == symbol.name
    end

    # precedence
    attr_accessor :prec
  end
end