require_relative 'precedence'

module STCC

  class NonactionSymbol < GrammarSymbol
    include Comparable

    # order by name
    def <=>(other)
      return 1 if other.nil?
      @name <=> other.name
    end

    def initialize(name)
      super(name)
    end
  end
end