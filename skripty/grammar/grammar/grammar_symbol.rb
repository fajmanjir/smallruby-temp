module STCC
  class GrammarSymbol
    def initialize(name)
      @name = name
    end

    def prec
      nil
    end

    attr_reader :name
  end
end