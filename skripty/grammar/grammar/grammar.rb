require_relative 'grammar_symbol'
require_relative 'nonaction_symbol'
require_relative 'grammar_rule'
require_relative 'action_symbol'
require_relative 'epsilon_symbol'
require_relative 'terminal_symbol'
require_relative 'nonterminal_symbol'

module STCC
  class Grammar
    def initialize
      @nonterminals = {}
      @terminals = {}
      @actions = {}
      @priority = 0
    end

    def add_nonterminal(name)
      Printer.error "nonterminal '#{name}' redefined" if @nonterminals.has_key?(name)
      Printer.error "Terminal #{name} passed instead of nonterminal" if /^(t[A-Z]|(keyword_)|(modifier_)|').*$/.match(name) && name != 'keyword_variable'
      Printer.error "Invalid nonterminal name #{name} passed to add_nonterminal" if name == '|' || name == "''"

      @nonterminals[name] = NonterminalSymbol.new(name)
    end

    def add_terminal(name)
      Printer.error "terminal '#{name}' redefined" if @terminals.has_key?(name)
      Printer.error "Invalid terminal name #{name} passed to add_terminal" if name == '|' || name == "''" || !/^(t[A-Z]|(keyword_)|(modifier_)|').*$/.match(name)
      @terminals[name] = TerminalSymbol.new(name)
    end

    def add_action(action)
      @actions["action#{@actions.length + 1}"] = action
    end

    def register_nonassoc(names)
      @priority += 1
      names.each do |name|
        terminal = resolve_terminal(name)
        terminal.prec = Precedence.new(@priority).nonassoc!
      end
      # p "grammar nonassoc: #{names} with priority #{@priority}"
    end

    def register_left(names)
      @priority += 1
      names.each do |name|
        terminal = resolve_terminal(name)
        terminal.prec = Precedence.new(@priority).left!
      end
      # p "grammar left: #{names} with priority #{@priority}"
    end

    def register_right(names)
      @priority += 1
      names.each do |name|
        terminal = resolve_terminal(name)
        terminal.prec = Precedence.new(@priority).right!
      end
      # p "grammar right: #{names} with priority #{@priority}"
    end

    # find or create nonterminal
    def resolve_nonterminal(name)
      Printer.error "Terminal #{name} passed to resolve_nonterminal" if /^(t[A-Z]|(keyword_)|(modifier_)|').*$/.match(name) && name != 'keyword_variable'
      exit if /^(t[A-Z]|(keyword_)).*$/.match(name) && name != 'keyword_variable'
      res = @nonterminals[name]
      res = add_nonterminal(name) unless res
      res
    end

    # find or create terminal
    def resolve_terminal(name)
      res = @terminals[name]
      res = add_terminal(name) unless res
      res
    end

    def find_symbol(name)
      symbol = @terminals[name]
      symbol = @nonterminals[name] unless symbol
      symbol
    end

    def rules_loaded
      extend_grammar
      add_markers
      compute_first
      compute_follow
    end

    # change starting symbol S to S' with rule S' -> S
    def extend_grammar
      start = @starting_symbol
      @starting_symbol = add_nonterminal('@Start')
      rule_builder = RuleBuilder.new(self)
      rule_builder.add_symbol(start)
      rule_builder.add_symbol(EpsilonSymbol.get_instance)
      @starting_symbol.add_rule(rule_builder.build())
    end

    def add_markers
      marker_count = 0
      markers = {}
      rule_builder = RuleBuilder.new(self)
      @nonterminals.each do |name, nonterminal|
        nonterminal.rules.each do |rule|
          idx = -1
          # find all nonterminals
          loop do 
            idx += 1
            break if idx >= rule.length - 1 # ignore action as the last symbol

            symbol = rule[idx]
            next unless symbol.class == ActionSymbol
            marker_count += 1
            marker = NonterminalSymbol.new("@marker#{marker_count}")

            rule_builder.add_symbol(symbol)
            marker.add_rule(rule_builder.build)

            # add marker to the rule
            rule[idx] = marker

            markers["@marker#{marker_count}"] = marker
          end
        end
      end
      @nonterminals.merge! markers
    end

    def compute_follow
      @starting_symbol.follow = [EpsilonSymbol.get_instance]
      loop do
        changed = false
        @nonterminals.each do |name, nonterminal|
          # next if nonterminal == @starting_symbol
          nonterminal.rules.each do |rule|
            idx = -1
            # find all nonterminals
            loop do 
              idx += 1
              break if idx >= rule.length

              symbol = rule[idx]
              next unless symbol.class == NonterminalSymbol
              
              follow_length = symbol.follow.length
              next_symbols = rule.symbols[idx + 1..-1]
              next_first = find_first(next_symbols)
              symbol.follow |= next_first - [EpsilonSymbol.get_instance]
              symbol.follow |= nonterminal.follow if next_first.include? (EpsilonSymbol.get_instance)
              changed = true if follow_length != symbol.follow.length
            end
          end
        end
        break unless changed
      end
    end

    def compute_first
      loop do
        changed = false
        @nonterminals.each do |name, nonterminal|
          nonterminal.rules.each do |rule|
            idx = -1
            loop do 
              idx += 1

              break if idx >= rule.length

              symbol = rule[idx]
              
              if symbol.class == NonterminalSymbol
                rule_first_length = rule.first.length

                rule.first |= [symbol] # add nonterminal
                rule.first |= symbol.first - [EpsilonSymbol.get_instance]
                if rule.first.length > rule_first_length
                  nonterminal.first |= rule.first
                  changed = true
                end

                break unless symbol.first.include?(EpsilonSymbol.get_instance)
              elsif symbol.class == TerminalSymbol
                unless rule.first.include?(symbol)
                  rule.first << symbol
                  nonterminal.first |= rule.first
                  changed = true
                end
                break
              end
            end
            if idx == rule.length && !rule.first.include?(EpsilonSymbol.get_instance)
              rule.first << EpsilonSymbol.get_instance
              nonterminal.first << EpsilonSymbol.get_instance
              changed = true
            end
          end
        end

        break unless changed
      end
    end

    # get first of rule
    def find_first(rule)
      first = []
      idx = -1
      loop do 
        idx += 1

        break if idx >= rule.length

        symbol = rule[idx]
        
        if symbol.class == NonterminalSymbol
          ruleFirstLength = first.length
          first |= symbol.first - [EpsilonSymbol.get_instance]
          break unless symbol.first.include?(EpsilonSymbol.get_instance)
        elsif symbol.class == TerminalSymbol
          first << symbol unless first.include?(symbol)
          break
        end
      end
      if idx == rule.length && !first.include?(EpsilonSymbol.get_instance)
        first << EpsilonSymbol.get_instance
      end
      first
    end

    attr_reader :nonterminals, :terminals, :actions
    attr_accessor :starting_symbol, :is_terminal_function
  end
end