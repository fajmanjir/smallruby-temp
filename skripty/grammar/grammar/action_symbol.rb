module STCC
  class ActionSymbol < GrammarSymbol
    include Comparable

    def initialize(action)
      @@name_index ||= 0
      @@name_index += 1
      super("action#{@@name_index}")
      @action = action # action code
    end

    # order by name
    def <=>(other)
      @name <=> other.name
    end

    attr_accessor :action, :local_vars
  end
end