module STCC
  class NonterminalSymbol < NonactionSymbol
    def initialize(name)
      super(name)
      @rules = []
      @first = [] # collection of terminals
      @follow = []
    end

    def add_rule(rule)
      @rules << rule
    end

    def to_s
      str = "#{name} (nonterminal) [\n"
      @rules.each do |rule|
        str <<= "  -> #{rule.to_s}\n"
      end
      str <<= "]\n"
    end

    attr_reader :rules
    attr_accessor :first, :follow
  end
end