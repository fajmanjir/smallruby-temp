module STCC
  class EpsilonSymbol < GrammarSymbol
    def to_s
      "eps"
    end

    class << self
      def get_instance
        @instance ||= self.new("@eps")
        @instance
      end
    end

    def first
      [self]
    end

    protected

    def initialize(name)
      super(name)
    end
  end
end