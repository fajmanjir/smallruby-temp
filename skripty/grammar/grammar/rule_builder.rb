module STCC
  class RuleBuilder
    def initialize(grammar)
      @grammar = grammar
      reset()
    end

    def add_action_symbol(content)
      action_symbol = ActionSymbol.new(content)
      @rule_symbols << action_symbol
      @grammar.add_action(action_symbol)
    end

    # fn - function to decide between terminal and nonterminal symbol
    def add_symbols_by_name(fn, *names)
      Printer.fatal_error 'add_symbols_by_name: first parameter must be Proc.' if ((!fn.nil?) && (!fn.is_a? Proc))

      prec = false
      names.each do |name|
        if prec
          @prec_symbol = @grammar.resolve_terminal(name)
          prec = false
        else
          if name == '%prec'
            prec = true
          else
            Printer.fatal_error '\';\' is missing (%% as symbol name!)' if name == '%%'
            add_symbol(resolve_symbol(name, fn))
          end
        end
      end
    end

    def add_symbol(symbol)
      @last_terminal = symbol if symbol.is_a? TerminalSymbol
      @rule_symbols << symbol
    end

    def build
      @prec_symbol ||= @last_terminal
      prec = (@prec_symbol.nil? ? nil : @prec_symbol.prec )
      # @rule_symbols << EpsilonSymbol.get_instance if @rule_symbols.length == 0
      res = GrammarRule.new(@rule_symbols, prec)
      reset()
      res
    end

  protected

    # find or create a symbol
    def resolve_symbol(name, fn)
      symbol = @grammar.find_symbol(name)
      return symbol unless symbol.nil?

      # all terminals containing more than 1 character already defined,
      # if not defined, 1 char is a terminal, nonterminal otherwise
      # return @grammar.add_terminal(name) if (/^'\\?.'$/.match(name) || (!fn.nil? && fn.call(name)))
      return @grammar.add_terminal(name) if (/^'\\?.'$/.match(name))
      return @grammar.add_terminal(name) if (fn && fn.call(name))

      @grammar.add_nonterminal(name)
    end

    # clear rule attributes
    def reset
      @rule_symbols = []
      @prec_symbol = nil
      @last_terminal = nil
    end
  end
end