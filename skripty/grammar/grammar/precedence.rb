module STCC
  class Precedence
    def initialize(priority)
      @priority = priority
      nonassoc!
    end

    def left!
      @type = :left
      self
    end

    def left?
      @type == :left
    end

    def right!
      @type = :right
      self
    end

    def right?
      @type == :right
    end

    def nonassoc!
      @type = :nonassoc
      self
    end

    def nonassoc?
      @type == :nonassoc
    end

    # attr_writer :priority

    def type
      Printer.fatal_error 'precedence type not defined' unless @type
      @type
    end

    def priority
      Printer.fatal_error 'precedence priority not defined' unless @priority
      @priority
    end

  end
end