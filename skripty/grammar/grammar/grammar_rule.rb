require 'forwardable'

module STCC
  class GrammarRule
    include Comparable
    extend Forwardable

    # prec is precedence symbol
    def initialize(symbols, prec)
      @symbols = symbols
      @prec = prec
      @first = []
    end

    def ==(rule)
      @symbols == rule.symbols
    end

    # shortcut
    def length
      @symbols.length
    end

    def <=>(other)
      idx = 0
      @symbols.each do |symbol|
        return symbol <=> other.symbols[idx] unless symbol == other.symbols[idx]
        idx += 1
      end
      0
    end

    def can_be_empty?
      @symbols.each do |symbol|
        return false if symbol.is_a?(TerminalSymbol)
        if symbol.is_a?(NonterminalSymbol)
          return false unless symbol.first.include?(EpsilonSymbol.get_instance)
        end
      end

      true
    end

    def to_s
      return 'eps' if @symbols.empty?
      @symbols.map { |symbol| symbol.name }.join(' ')
    end

    attr_reader :symbols, :prec
    attr_accessor :first
    def_delegators :@symbols, :[], :[]=
  end
end