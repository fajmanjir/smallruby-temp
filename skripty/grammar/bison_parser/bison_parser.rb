require_relative '../grammar/grammar.rb'
require_relative 'states/parser_state.rb'
require_relative 'states/init_skipper.rb'
require_relative 'states/parser_declarations.rb'
require_relative 'states/grammar_rules.rb'

module STCC
  class BisonParser
    def initialize
      # output listener
      @grammar = Grammar.new

      # initialize all states
      @states = {
        initSkipper: InitSkipperState.new(self),
        parserDeclarationsState: ParserDeclarationsState.new(self),
        grammarRules: GrammarRulesState.new(self)
      }

      # initial state
      @state = @states[:initSkipper]
    end

    def parse(input, start_line = 0, end_line = -1)
      puts "parsing grammar..."

      line_number = 0
      @unsuccessful_lines = []
      @lines_processed = 0

      begin
        begin
          input.each_line do |line|
            line_number += 1

            if start_line <= line_number
              break if end_line != -1 && end_line < line_number
              unsuccessful(line, line_number) unless translate_line(line)
# p line_number
              @lines_processed += 1
            end
          end
        rescue EndOfInput
          # Printer.log 'EndOfInput'
        end

        @grammar.rules_loaded

        Printer.fatal_error "EOF reached, missing ending bracket } (brace_level #{@states[:grammarRules].brace_level} instead of 0)" if @states[:grammarRules].brace_level != 0
      rescue FatalError => e
        Printer.fatal_error_msg = "FATAL ERROR, line #{line_number}: #{e.message}"
        unsuccessful(e.message, line_number)
      end

      puts 'parsing done'

      @grammar
    end
    
    # switch parser state, column = nil stands for skipping the rest of the line
    def change_state(state, column = 0)
      @column ||= 0
# p "column: #{@column} => #{@column + column}"
      @column += column
      @state = state
      @state_changed = true
    end

    def translate_line(line)
      @column = 0

      loop do
        @state_changed = false

        @success = @state.translate_line(line[@column..-1])
        break if (!@state_changed || @column == 0 || line[@column..-1].strip.length == 0)
      end

      @success
    end

    # unsuccessful line handler
    def unsuccessful(line, line_number)
      res = /^(\s*)(.*)/.match(line)
      # puts "#{res.captures[0] * 2}#{res.captures[1]}"
      @unsuccessful_lines << "#{line_number}: #{line}"

      # if @unsuccessful_lines.size >= $max_unsuccessful_lines
      #   print_stats()
      #   puts "\033[00;31m-------------------------------------------------------"
      #   puts "\033[1mlimit of #{$max_unsuccessful_lines} failures reached, quit\033[00m"
      #   exit
      # end
    end

    def is_terminal_function=(func)
      @grammar.is_terminal_function = func
    end

    attr_reader :states, :grammar, :unsuccessful_lines
  end
end