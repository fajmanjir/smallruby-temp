module STCC
  class ParserState
    def initialize(parser)
      @parser = parser
      @rules = []

      initialize_rules() # abstract

      @current_rules = @rules
    end

    def translate_line(line)
      before_rules(line)
      get_rules().each do |rule|
        begin
          unless (res = rule[:regexp].match(line)).nil?
            return true if rule[:block].call(res.captures)
          end
        rescue TranslateException
        end

      end
      false
    end

    # returns str, comments filled with spaces
    def remove_inline_comments(str)
      comments = str.scan(/(\/\*(.*?)(\*\/))/)
      # replace /* ... */ with spaces to preserve position
      if comments
        comments.each do |match|
          str.sub!(match[0], ' ' * match[0].length)
        end
      end
      str
    end

    # convert string like "tDOT '=' " to array of symbols, no action symbols present
    def symbols_to_a(str_symbols)
# puts "symbols_to_a: #{str_symbols}"
      return [] if str_symbols.nil? || str_symbols.strip == ''
      str_symbols = remove_inline_comments(str_symbols)

      str_symbols.strip!
      return [] if str_symbols.length == 0
      a = str_symbols.split("'", 3)


      if a.size == 1
        res = a[0].split
      else
        res = []
        left = symbols_to_a(a[0])
        res.push(*left) unless left.empty?
        # single character
        res << "'#{a[1]}'" unless a[1].nil?
        right = symbols_to_a(a[2])
        res.push(*right) unless right.empty?
      end

      res
    end

    protected

    # called before each input is matched against current rules
    def before_rules(input)
    end

    def add_rule(collection, regexp, block)
      if collection.nil?
        Printer.fatal_error "#{self.class.to_s} add_rule /#{regexp}/ failed, collection is undefined"
      end
      collection << { regexp: regexp, block: block }
    end

    # get active set of rules
    def get_rules
      @current_rules
    end

    # set active rules
    def apply_rules(rules)
      @current_rules = rules
    end
  end
end
