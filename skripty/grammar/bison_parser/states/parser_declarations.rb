module STCC
  class ParserDeclarationsState < ParserState

    protected

    def initialize_rules
      add_rule(@rules, /^%pure-parser$/, proc { |res| true })
      add_rule(@rules, /^%parse-param.*$/, proc { |res| true })
      add_rule(@rules, /^%lex-param.*$/, proc { |res| true })
      add_rule(@rules, /^%union\s*\{$/, proc { |res| @current_rules = @union_rules; true })
      add_rule(@rules, /^\s*$/, proc { |res|  true }) # empty line
      add_rule(@rules, /^\s*%%\s*.*$/, proc { |res| @parser.change_state(@parser.states[:grammarRules]); true }) # end of declarations
      # %token\n
      add_rule(@rules, /^%token\s*$/, proc { |res| @current_rules = @token_rules; true })
      # %token <id>   tIDENTIFIER tFID tGVAR tIVAR tCONSTANT tCVAR tLABEL /* a comment */
      add_rule(@rules, /^%token\s*(<[^>]+>)?\s*([\w ]*\w)\s*(\/\*.*\*\/)?\s*$/, proc { |res|
        res[1].split(' ').each do |terminal|
          @parser.grammar.add_terminal(terminal)
        end
        true
      })
      # %type <node> singleton strings string string1 xstring regexp
      add_rule(@rules, /^%type\s*(<[^>]+>)?\s*([\w ]*\w)\s*(\/\*[^\*]*\*\/)?\s*$/, proc { |res|
        res[1].split(' ').each do |nonterminal|
          @parser.grammar.add_nonterminal(nonterminal)
        end
        true
      })
      # skip block comment
      add_rule(@rules, /^\s*\/\*\s*$/, proc { |res| @current_rules = @comment_rules; true })
      add_rule(@rules, /^\s*%nonassoc ([^\n]+)\s*$/, proc { |res| @parser.grammar.register_nonassoc(symbols_to_a(res[0])); true })
      add_rule(@rules, /^\s*%left ([^\n]+)\s*$*/, proc { |res| @parser.grammar.register_left(symbols_to_a(res[0])); true })
      add_rule(@rules, /^\s*%right ([^\n]+)\s*$/, proc { |res| @parser.grammar.register_right(symbols_to_a(res[0])); true })

      # %union {
      # ...
      # }
      @union_rules = []
      add_rule(@union_rules, /^\s*\}\s*$/, proc { |res| @current_rules = @rules; true })
      add_rule(@union_rules, /^.*$/, proc { |res|  true }) # anything except }
      
      # %token
      #   keyword_class
      #   keyword_module
      # 
      @token_rules = []
      add_rule(@token_rules, /^\s*(\w+)\s*$/, proc { |res| @parser.grammar.add_terminal(res[0]) ; true })
      add_rule(@token_rules, /^\s*$/, proc { |res| @current_rules = @rules ; true }) # empty line - end of token rules
      # /*
      # *
      # */
      @comment_rules = []
      add_rule(@comment_rules, /^\s*\*\/\s*$/, proc { |res| @current_rules = @rules ; true })
      add_rule(@comment_rules, /.*/, proc { |res| ; true }) # skip line
    end
  end
end