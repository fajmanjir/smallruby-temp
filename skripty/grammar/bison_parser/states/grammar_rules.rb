require_relative '../../grammar/rule_builder'

module STCC
  class GrammarRulesState < ParserState

    attr_reader :brace_level, :base_pos

    protected

    def initialize_rules
      @rule_builder = RuleBuilder.new(@parser.grammar)
      @brace_level = 0

      # operation2      : tIDENTIFIER [...]
      add_rule(@rules, /\s*%%\s*/, proc { |res| raise EndOfInput.new })
      add_rule(@rules, /^((\w+)\s*:\s*).*$/, proc { |res|
        @current_nonterminal = @parser.grammar.resolve_nonterminal(res[1])
        @current_rules = @rightside_rules
        @parser.change_state(self, res[0].length)
        true
      })
      add_rule(@rules, /^\s*$/, proc { |res|true })

      @rightside_rules = []
      add_rule(@rightside_rules, /^(\s*;\s*)$/, proc { |res|
        Printer.fatal_error "; without definition of nonterminal, line: '#{res[0]}'" if @current_nonterminal.nil?
        @current_nonterminal.add_rule(@rule_builder.build)
        @current_rules = @rules
        @parser.grammar.starting_symbol ||= @current_nonterminal # first nonterminal unless already defined
        true
      })
      add_rule(@rightside_rules, /^(\s*\|).*$/, proc { |res|
        @current_nonterminal.add_rule(@rule_builder.build)
        @parser.change_state(self, res[0].length)
        true
      })
      add_rule(@rightside_rules, /^(.*)$/, proc { |res|
        handle_rule_sequence(res[0]); true })

      # content of an action:
      # { ... } or on more lines
      @action_rules = []
      add_rule(@action_rules, /^(\s*\/\*).*$/, proc { |res|
        add_action_content(res[0])
        @current_rules = @action_comment_rules
        @parser.change_state(self, res[0].length)
        true })
      add_rule(@action_rules, /^(.*((\{[^'])|(\}[^'])).*)$/, proc { |res| handle_action_rules(res[0]); true })
      add_rule(@action_rules, /^(.*)$/, proc { |res| add_action_content(res[0]); true })

      @action_comment_rules = []
      add_rule(@action_comment_rules, /\*\//, proc { |res|
        add_action_content(res[1])
        @current_rules = @action_rules
        change_state(self, res[1].length) if res[1]
        true})
      add_rule(@action_comment_rules, /^(.*)$/, proc { |res| add_action_content(res[0]); true })
    end

    # parse str and add symbols to collection
    def handle_rule_sequence(str)
      # 1. remove /* ... */
      # 2. more rules on one line
      # 3. contains { (not '{')
      # 4. handle left side, then change rules to action_rules

      str = remove_inline_comments(str)

      # more rules on one line
      pipe_pos = str.index(/ \|/)
      unless pipe_pos.nil?
        str = str[0...pipe_pos]
      end

      # find {, not '{'
      opening_brace_index = str.index(/((\{[^'])|(\{$))/)

      if opening_brace_index.nil?
        handle_nonactions(str)
        @parser.change_state(self, pipe_pos) unless pipe_pos.nil?
      else
        handle_nonactions(str[0...opening_brace_index])
        @action_content = nil
        @brace_level = 1
        @current_rules = @action_rules
        # continue on the same line
        @parser.change_state(self, opening_brace_index + 1)
      end
    end

    def before_rules(input)
# puts "input: '#{input[0...-1]}', brace_level: #{@brace_level},    \trules: #{@current_rules.to_s[0..50]}"
    end

    # parse right side of a rule and add them to the RuleBuilder
    def handle_nonactions(str)
      str.strip!
      return if str.empty?
      symbols = symbols_to_a(str)
      @rule_builder.add_symbols_by_name(@parser.grammar.is_terminal_function, *symbols)
    end

    # used inside of @action_rules only
    def handle_action_rules(str)
      opening_index = str.index('{')
      closing_index = str.index('}')
      if !opening_index.nil? and closing_index.nil?
        opening_count = str.scan('{').count
        @brace_level += opening_count
        add_action_content(str)
      elsif opening_index.nil? and !closing_index.nil?
        closing_count = str.scan('}').count
        @brace_level -= closing_count
        Printer.fatal_error 'more closing braces than opening' if @brace_level < 0
        if @brace_level == 0
          add_action_content(str[0...closing_index])
          @rule_builder.add_action_symbol(@action_content)
          @current_rules = @rightside_rules
          # continue on the same line
          @parser.change_state(self, closing_index + 1)
        else
          add_action_content(str)
        end
      elsif closing_index < opening_index
        @brace_level -= 1
        add_action_content(str[0...closing_index])
        if @brace_level == 0
          @current_rules = @rightside_rules
          @rule_builder.add_action_symbol(@action_content)
        end
        # continue on the same line
        @parser.change_state(self, closing_index + 1)
      else
        @brace_level += 1
        add_action_content(str)
        # continue on the same line, same rules
        @parser.change_state(self, opening_index + 1)
      end
    end

    def add_action_content(str)
      str ||= ''
      if @action_content
        @action_content += "\n#{str.strip}"
      else
        @action_content = str.strip
      end
    end
  end
end