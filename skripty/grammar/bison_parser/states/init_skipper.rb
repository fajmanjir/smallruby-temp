module STCC
  class InitSkipperState < ParserState

    protected

    def initialize_rules
      add_rule(@rules, /^(.*%\}).*$/, proc { |res|
        @parser.change_state(@parser.states[:parserDeclarationsState], res[0].length); true
      })
      add_rule(@rules, /^.*$/, proc { |res| true })
    end
  end
end