require_relative 'parser_builder'
require_relative 'dot_rule'
require_relative 'bu_parser_state'
require_relative 'bottom_up_parser'
require_relative 'actions/accept_action'
require_relative 'actions/shift_action'
require_relative 'actions/reduce_action'
require_relative 'actions/error_action'
require_relative 'actions/goto_action'

module STCC
  class BottomUpParserBuilder < ParserBuilder
    def build(grammar)
      @grammar = grammar
      @reduce_queue = [] # to be solved before reduce reduce
      @reduce_reduce_queue = [] # Reduce-Reduce to be solved last
      states = generate_states
      BottomUpParser.new(states, grammar)
    end

    # debug print
    def dump_states
      puts '' # empty line
      puts 'dump states:'
      @states.each do |state|
        puts "STATE #{state.state_id}"
        state.dump_rules
        puts ''
        state.dump_action_table
        puts '' # empty line
      end
    end

    attr_reader :states # for tests

  private

    def generate_states
      new_state_dot_rules = @grammar.starting_symbol.rules.map { |rule| DotRule.new(@grammar.starting_symbol, rule, 0) }
      new_state = BUParserState.new(new_state_dot_rules)
      new_state.closure!
      new_state.state_id = 0

      @states = [new_state]

      # all grammar symbols
      symbols = @grammar.terminals.merge(@grammar.nonterminals)
      symbols[EpsilonSymbol.get_instance.name] = EpsilonSymbol.get_instance
      # iterate over states
      idx = 0
      while idx < @states.length
        state = @states[idx]
        handle_new_states(state, symbols)
        idx += 1
        Printer.log "#{state.state_id} state length: #{@states.last.length}"
      end

      add_accept_state


      Printer.log 'Resolve reduce...' if @reduce_queue.length > 0
      resolve_reduce

      Printer.log 'Resolve reduce reduce conflicts...' if @reduce_reduce_queue.length > 0
      resolve_reduce_reduce

      Printer.log "generated states: #{@states.length}"
      @states
    end

    # try goto(state, symbol) for all symbols in symbols
    def handle_new_states(state, symbols)

      handle_reduce_actions(state)

      # get only suitable symbols
      # first = @grammar.nonterminals.to_a
      # first = [EpsilonSymbol.get_instance]
      # state.rules.each do |dot_rule|
      #   first |= dot_rule.nonterminal.first
      #   first |= dot_rule.nonterminal.follow if dot_rule.nonterminal.first.include?(EpsilonSymbol.get_instance)
      #   if first.map{|symbol| symbol.name}.include? "'='"
      #     Printer.fatal_error "NASEL JSEM = "
      #     exit
      #   end
      #   # Printer.fatal_error "NASEL JSEM tINTEGER" if first.map{|symbol| symbol.name}.include? "tINTEGER"
      # end

      first = symbols.map {|k, s| s}

# Printer.d "#{state.state_id}: first: #{first.map{|symbol| symbol.name}.join "\n - "}"

      # add states using goto function
      # symbols.each do |name, symbol|
      first.each do |symbol|

        new_state = goto(state, symbol)

        if (!new_state.nil?) && (!new_state.empty?)

          # get previous reference to the same state if not unique
          state_idx = @states.find_index(new_state)
          if state_idx
            new_state = @states[state_idx]
            new_state.add_predecessor(state)
          else
            new_state.state_id = @states.length
            @states << new_state
          end

          # fill actions table
          if symbol.is_a? NonterminalSymbol
            state.action_table[symbol] = GotoAction.new(new_state)
          else
            if state.action_table[symbol].nil? && state.default_action.is_a?(ErrorAction)
              state.action_table[symbol] = ShiftAction.new(new_state)
            else
              # resolve conflict
              old_action = state.action_table[symbol].nil? ? state.default_action : state.action_table[symbol]
              unless old_action.is_a?(ReduceAction)
                shift_action = ShiftAction.new(new_state)
                Printer.fatal_error "conflict detected:\n #{old_action} (original action)\n versus\n #{shift_action}"
              end

              resolve_shift_reduce(state, new_state, symbol, old_action)
            end
          end
        end
      end
    end

    #
    def resolve_shift_reduce(state, new_state, symbol, reduce_action)
      if reduce_action.dot_rule.prec.nil?
        if symbol.prec
          state.action_table[symbol] = ShiftAction.new(new_state)
        else
          if symbol.is_a?(TerminalSymbol)
            state.action_table[symbol] = ShiftAction.new(new_state)

            unless reduce_action.dot_rule.reads_nothing?
              # Printer.log "#{state.state_id}: adding to the queue: #{symbol.name}\n#{state.rules.map{|rule| "#{rule.nonterminal.name} -> #{rule.symbols.map{|s| s.name}.join("\n")}" }}\n"
              # @shift_reduce_la_queue << {state: state, new_state: new_state, symbol: symbol, reduce_action: reduce_action}
              Printer.warn "#{state.state_id}: conflict detected:\n #{reduce_action} (rule prec)\n versus\n #{state.action_table[symbol]}\nin state #{state.state_id}\n\nSymbol #{symbol.name} nor rule has assigned precedence\n" +
                               "defaults to shift (symbol #{symbol.name})"
            end
          else
            state.action_table[symbol] = ShiftAction.new(new_state)
          end
        end
      elsif symbol.prec.nil?
        # nothing
      elsif reduce_action.dot_rule.prec.priority < symbol.prec.priority
        state.action_table[symbol] = ShiftAction.new(new_state)
      elsif reduce_action.dot_rule.prec.priority == symbol.prec.priority
        if reduce_action.dot_rule.prec.right?
          state.action_table[symbol] = ShiftAction.new(new_state)
        elsif reduce_action.dot_rule.prec.nonassoc?
          state.action_table[symbol] = ErrorAction.new
        end
      end
    end

    def handle_reduce_actions(state)
      state.rules.each do |dot_rule|
        if dot_rule.can_reduce?
          action = ReduceAction.new(dot_rule)
          if state.default_action.is_a? ErrorAction
            state.default_action = action
          else
            common_follow = state.default_action.dot_rule.nonterminal.follow & action.dot_rule.nonterminal.follow
            if state.default_action.is_a?(ReduceAction) && common_follow.empty?
              action.dot_rule.nonterminal.follow.each do |symbol|
                if state.action_table.has_key? symbol
                  if state.action_table[symbol].is_a?(ReduceAction)
                    # resolve_reduce_reduce(state, action, common_follow)
                    @reduce_reduce_queue << {state:state, new_reduce: action, common_follow: common_follow} unless state.default_action.is_a?(ShiftAction)
                  else
                    # TODO: resolve Shift/Reduce
                    Printer.fatal_error "conflict detected:\n #{state.action_table[symbol]} (default action)\n versus\n #{action}\n TODO: Should implement!!!"

                  end
                else
                  state.action_table[symbol] = action
                end
              end
            else
              # both nonterminals have a common element in follow
              # state.default_action is not ErrorAction

              # TODO: solve non-conflict actions as conflict!!!

              # add non-conflict actions
              nonconflict_symbols = action.dot_rule.nonterminal.follow - common_follow
              nonconflict_symbols.each do |symbol|
                if state.action_table.has_key? symbol
                  common_follow << symbol # collides with a different action than default
                else
                  @reduce_queue << {state: state, symbol: symbol, new_reduce: action}
                end
              end

              # add to the queue to be solved last
              @reduce_reduce_queue << {state:state, new_reduce: action, common_follow: common_follow} unless state.default_action.is_a?(ShiftAction)
            end
          end
        end
      end
    end

    def resolve_reduce_reduce
      idx = 1
      @reduce_reduce_queue.each do |queue_item|

        new_reduce = queue_item[:new_reduce]
        state = queue_item[:state]
        common_follow = queue_item[:common_follow]

        Printer.d "resolve_reduce_reduce entered, commons: #{common_follow.map{|f| f.name}.join ', '}"

        new_accepted_symbols = [] # symbols accepted by state after performing new_reduce
        common_follow.each do |follow|
          next if new_accepted_symbols.include?(follow)
Printer.d "predecessor id: #{state.state_id}"

          if state.can_use_symbol_rec?(follow, new_reduce)
            new_accepted_symbols << follow
Printer.d "FOUND in NEW: #{follow.name}" # # # # # # # # # # # # # # # # # # # # # # # TODO: TEST!!! # # # # # # # # # # # # # # #  ## # ###   # # ## # #
          end
        end
        # end

        Printer.d ""
        Printer.d ""
        Printer.d 'after new action'

        if new_accepted_symbols.length != 0
          # apply new reduce when not in colission with old actions
          # old_accepted_symbols = []

          @old_nth_predecessors = {} # n => predecessors
          @old_accepted_symbols = []

          new_accepted_symbols.each do |symbol|
            next if @old_accepted_symbols.include?(symbol)

            if state.action_table.has_key?(symbol)

              # compare only with action table
              resolve_reduce_reduce_follow(state.action_table[symbol], symbol, new_reduce, state)

            else

              # compare only default action
              resolve_reduce_reduce_follow(state.default_action, symbol, new_reduce, state)

            end
          end

          # assign new reduce to remaining symbols
          acceptable_symbols = common_follow - @old_accepted_symbols
          acceptable_symbols.each do |symbol|
            state.action_table[symbol] = new_reduce
          end
        end

        Printer.log "Resolved Reduce-Reduce #{idx}/#{@reduce_reduce_queue.length}"
        idx += 1
      end

      @reduce_reduce_queue = []
    end

    def resolve_reduce_reduce_follow(old_action, symbol, new_reduce, state)
      if old_action.is_a?(ReduceAction)
        if state.can_use_symbol_rec?(symbol, old_action)
          @old_accepted_symbols << symbol
          Printer.fatal_error "#{state.state_id}: conflict detected:\n #{old_action} (on #{symbol.name})\n versus\n #{new_reduce}\n\n"
          return
        end

        # old_dot_rule = old_action.dot_rule
        #
        # old_length = old_dot_rule.real_length
        # @old_nth_predecessors[old_length] = state.get_nth_predecessors(old_length) unless @old_nth_predecessors.has_key?(old_length)
        #
        # @old_nth_predecessors[old_length].each do |predecessor|
        #   if predecessor.action_table.has_key?(old_dot_rule.nonterminal)
        #     if predecessor.action_table[old_dot_rule.nonterminal].parser_state.can_use_symbol_rec?(symbol, nil) ### Recursive?
        #       @old_accepted_symbols << symbol
        #       Printer.fatal_error "conflict detected:\n #{old_action} (on #{symbol.name})\n versus\n #{new_reduce}\n\n"
        #       return
        #     end
        #   end
        # end
      elsif old_action.is_a?(ShiftAction) && new_reduce.prec && (old_action.prec.nil? || new_reduce.prec.priority > old_action.prec.priority)
        # nothing, defaults to shift
      else
        @old_accepted_symbols << symbol
        Printer.fatal_error "#{state.state_id}: conflict detected: (resolve_reduce_reduce_follow)\n #{old_action} (on #{symbol.name})\n versus\n #{new_reduce}\n\n"
      end
    end

    def resolve_reduce
      idx = 1
      @reduce_queue.each do |queue_item|

        new_reduce = queue_item[:new_reduce]
        state = queue_item[:state]
        symbol = queue_item[:symbol]

        if state.can_use_symbol_rec?(symbol, new_reduce)
          Printer.d "Adding trivial: #{symbol}"
          state.action_table[symbol] = new_reduce
        end

        Printer.log "resolved reduce #{idx}/#{@reduce_queue.length}"
        idx += 1
      end

      @reduce_queue = []
    end

    def add_accept_state
      Printer.fatal_error 'empty input grammar' if @states.length < 2

      accept_state = goto(goto(@states[0], @states[0][0][0]), EpsilonSymbol.get_instance)
      accept_state_index = @states.find_index(accept_state)
      accept_state_ref = @states[accept_state_index]
      accept_state_ref.default_action = AcceptAction.new
    end

    # return new state closure
    def goto(state, symbol)

      new_state = BUParserState.new
      new_state.add_predecessor(state)
      previous_dot_rules = []
      loop do
        new_state.rules.each do |dot_rule|
          previous_dot_rules << dot_rule unless previous_dot_rules.include? dot_rule
        end

        new_state.rules = []
        change = false
        state.each do |dot_rule|

          if dot_rule.next_symbol == symbol
            new_dot_rule = DotRule.new(dot_rule.nonterminal, dot_rule.rule, dot_rule.pos + 1)
            if dot_rule.length >= dot_rule.pos + 1 && !previous_dot_rules.include?(new_dot_rule)
              new_state << new_dot_rule
              change = true
            end
          # elsif

          end
        end

        unless change
          new_state.rules = previous_dot_rules
          break
        end

        old_length = new_state.rules.length
        new_state.closure!

        new_state.rules.each do |dot_rule|
          previous_dot_rules << dot_rule unless previous_dot_rules.include? dot_rule
        end

        if new_state.rules.length == old_length
          # closure added no state
          new_state.rules = previous_dot_rules
          break
        end

      end

      new_state
    end
  end
end