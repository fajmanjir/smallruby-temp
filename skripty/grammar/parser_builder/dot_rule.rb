require 'forwardable'

module STCC
  class DotRule
    include Comparable
    extend Forwardable

    def initialize(nonterminal, rule, pos)
      @nonterminal = nonterminal
      @rule = rule
      @pos = pos
      Printer.fatal_error 'DotRule pos out of rule range' if @rule.length < @pos
    end

    def ==(dot_rule)
      return false if dot_rule.class != self.class
      @nonterminal == dot_rule.nonterminal && @rule == dot_rule.rule && @pos == dot_rule.pos
    end

    def [](idx)
      return EpsilonSymbol.get_instance if idx == @rule.length
      @rule[idx]
    end

    def length
      @rule.length
    end

    # symbol after dot
    def next_symbol
      idx = @pos
      loop do
        break if idx == @rule.length
        symbol = self[idx]
        return symbol unless symbol.is_a?(ActionSymbol)
        idx += 1
      end

      nil
    end

    def last_symbol
      @rule[-1]
    end

    # can be reduced
    def can_reduce?
      idx = @pos
      loop do
        break if idx == @rule.length
        return false if @rule[idx].is_a?(NonactionSymbol) || @rule[idx].is_a?(EpsilonSymbol)
        idx += 1
      end

      true
    end

    def has_action?
      @rule.length > 0 && last_symbol.is_a?(ActionSymbol)
    end

    def reads_nothing?
      can_reduce? && @pos == 0
    end

    # get length without action
    def real_length
      (@rule.length > 0 && last_symbol.is_a?(ActionSymbol)) ? @rule.length - 1 : @rule.length
    end

    def <=>(other)
      return @nonterminal <=> other.nonterminal if @nonterminal != other.nonterminal
      return @rule <=> other.rule if @rule != other.rule
      @pos <=> other.pos
    end

    def to_s
      res = "#{@nonterminal.name} ->"
      idx = 0
      @rule.symbols.each do |symbol|
        res += ' .' if idx == @pos
        res += " #{symbol.name}"
        idx += 1
      end
      res += ' .' if idx == @pos
      res
    end

    attr_reader :rule, :pos, :nonterminal
    def_delegators :@rule, :prec, :symbols
  end
end