require 'forwardable'

module STCC
  # Bottom-Up parser state
  class BUParserState
    extend Forwardable

    def initialize(dot_rules = [])
      @rules = dot_rules
      # Symbol => ParserAction
      @action_table = {}
      @default_action = ErrorAction.new
      @predecessors = []

      @state_id = -1
      @can_reduce = false
    end

    def ==(other)
      return false unless self.class == other.class
      sorted_rules = @rules.sort
      sorted_other_rules = other.rules.sort
      sorted_rules == sorted_other_rules # TODO: add new parameters to comparison?
    end

    # print state info
    def dump_rules
      puts "state #{@state_id} with length #{length}:"
      self.each do |dot_rule|
        puts dot_rule
      end
    end

    def dump_action_table
      # puts 'dump parsing table'
      @action_table.each do |symbol, action|
        puts "#{symbol.name} => #{action}"
      end
      puts "default: #{default_action}"
    end

    # create closure from collection of rules
    def closure!
      # loop over collection of dot rules
      idx = 0
      while idx < @rules.length
        dot_rule = @rules[idx]
        # loop over symbols in dot rule
        expanded = []
        old_expanded = expanded
        if dot_rule.pos < dot_rule.length
          dot_rule_symbol_idx = dot_rule.pos
          # expanded = []
          loop do
            expanded_symbol = expand_symbol(dot_rule[dot_rule_symbol_idx])
            expanded_symbol.each do |exp_dot_rule|
              unless @rules.include?(exp_dot_rule)
                expanded << exp_dot_rule
              end
            end
            dot_rule_symbol_idx += 1

            break if old_expanded == expanded

            old_expanded = expanded

            # break if (!expanded.include? EpsilonSymbol.get_instance)
            # expanded.delete EpsilonSymbol.get_instance
          end
        end
        expanded.each do |exp_dot_rule|
          @rules << exp_dot_rule unless @rules.include?(exp_dot_rule)
          @can_reduce = true if exp_dot_rule.can_reduce?
# p "add dot_rule: #{exp_dot_rule}"
        end
        idx += 1
      end

      # check type
      @rules.each do |dot_rule|
        Printer.fatal_error "closure! method expects to sort DotRule instances, instead #{dot_rule.class} given" unless dot_rule.is_a?(DotRule)
      end

      @rules.sort
    end

    # use of the symbol does not lead to error
    def can_use_symbol?(symbol)
      (!default_action.is_a?(ErrorAction) && (!action_table.has_key?(symbol) || !action_table[symbol].is_a?(ErrorAction))) ||
          (action_table.has_key?(symbol) && !action_table[symbol].is_a?(ErrorAction))
    end

    # use of the symbol does not lead to error - recursive
    def can_use_symbol_rec?(symbol, new_reduce, call_id = nil)

      if call_id.nil?
        @@call_id ||= 1
        @@call_id += 1
        call_id = @@call_id
      end

      # prevent infinite loops
      return false if call_id == @call_id

      @call_id = call_id

      Printer.d ' ================================================================'
      Printer.d "begin state #{@state_id}"

      if new_reduce.nil?
        Printer.d "\n#{@state_id}: nil"
        if @action_table.has_key?(symbol)
          Printer.d "#{@state_id}: symbol: #{symbol.name}  <== 1a - has key(symbol)"
          if @action_table[symbol].is_a?(ErrorAction)
            Printer.d "#{@state_id}: symbol: #{symbol.name}    FALSE <== 1a"
            return false
          elsif @action_table[symbol].is_a?(ShiftAction)
            Printer.d "#{@state_id}: symbol: #{symbol.name}    TRUE  <== 1a"
            return true
          end
        end
        new_reduce = @default_action

        return false if new_reduce.is_a?(ErrorAction)
        return true if new_reduce.is_a?(ShiftAction) || new_reduce.is_a?(AcceptAction)
      else
        return false if @action_table.has_key?(symbol) && @action_table[symbol].is_a?(ErrorAction)
        Printer.d "#{@state_id}: symbol: #{symbol.name}  <== 1b"
      end

      # number of reduced symbols without ending action
      new_n = new_reduce.dot_rule.real_length

      Printer.d "#{@state_id}: symbol: #{symbol.name}, n = #{new_n}  <== 3"

      new_nth_predecessors = get_nth_predecessors(new_n)
      # new_accepted_symbols = [] # symbols accepted by state after performing new_reduce
      new_nth_predecessors.each do |predecessor|
        Printer.d " - predecessor: #{predecessor.state_id}"

        if predecessor.action_table.has_key?(new_reduce.dot_rule.nonterminal)
          new_state = predecessor.action_table[new_reduce.dot_rule.nonterminal].parser_state
          # Printer.d "#{@state_id} <- #{new_state.state_id} can_use_symbol_rec? #{new_state.can_use_symbol_rec?(symbol, nil) ? 'true' : 'false' } <== 3"
          return true if new_state.can_use_symbol_rec?(symbol, nil, call_id)
        end

        # if predecessor.can_use_symbol?(follow)
        #   new_accepted_symbols << follow
        #   break
        # end
      end

      Printer.d "#{@state_id}: can_use_symbol_rec? false <== 5"

      false
    end

    def add_predecessor(state)
      @predecessors << state unless @predecessors.include? state
    end

    def get_nth_predecessors(n)
      return [self] if n == 0
      return @predecessors if n == 1

      preds = []
      @predecessors.each do |p|
        preds |= p.get_nth_predecessors(n-1)
      end

      preds
    end

    attr_accessor :rules, :action_table, :default_action, :state_id, :can_reduce, :predecessors
    def_delegators :@rules, :[], :[]=, :length, :each, :map, :<<, :empty?, :include?

    private

    def expand_symbol(symbol)
      # return [] unless symbol.is_a? NonterminalSymbol

      return [] if (!symbol.is_a? NonterminalSymbol) #|| symbol.first == [EpsilonSymbol.get_instance]

      expanded = []
      symbol.rules.each do |rule|
        expanded << DotRule.new(symbol, rule, 0)
      end
      expanded
    end

  end
end