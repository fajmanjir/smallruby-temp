module STCC
  class ParserBuilder
    # abstract
    def build(grammar)
      raise NotImplementedError
    end
  end
end