module STCC
  class BottomUpParser
    def initialize(states, grammar)
      @states = states
      @grammar = grammar
    end

    attr_accessor :states, :grammar
  end
end