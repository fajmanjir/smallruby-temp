require_relative 'parser_action'

module STCC
  class AcceptAction < ParserAction
    def to_s
      'Accept'
    end
  end
end