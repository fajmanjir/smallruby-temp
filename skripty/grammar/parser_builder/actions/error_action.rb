require_relative 'parser_action'

module STCC
  class ErrorAction < ParserAction
    def to_s
      'Error'
    end
  end
end