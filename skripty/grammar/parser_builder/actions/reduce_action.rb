require_relative 'parser_action'

module STCC
  class ReduceAction < ParserAction
    # reduce according to the DotRule instance
    def initialize(dot_rule)
      @dot_rule = dot_rule
    end

    def to_s
      "Reduce #{@dot_rule}"
    end

    attr_reader :dot_rule
  end
end