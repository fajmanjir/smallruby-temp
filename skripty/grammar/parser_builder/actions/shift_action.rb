require_relative 'parser_action'

module STCC
  class ShiftAction < ParserAction
    # state to be pushed onto the stack
    def initialize(bu_parser_state)
      @parser_state = bu_parser_state
    end

    def to_s
      "Shift #{@parser_state.state_id}"
    end

    # state to be pushed onto the stack
    attr_reader :parser_state
  end
end