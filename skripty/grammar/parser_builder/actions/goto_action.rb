require_relative 'parser_action'

module STCC
  class GotoAction < ParserAction
    # state to be pushed onto the stack
    def initialize(bu_parser_state)
      @parser_state = bu_parser_state
    end

    def to_s
      "Goto #{@parser_state.state_id}"
    end

    # state to be pushed onto the stack
    attr_reader :parser_state
  end
end