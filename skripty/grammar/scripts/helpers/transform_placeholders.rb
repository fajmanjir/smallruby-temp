
result_numeric_placeholder = /@([\d]+)/
result_numeric_prefix = '@'
result_variable = 'result'

res = ''

File.open(ARGV[0]).each_line do |line|

  str2 = line.gsub('@@', 'result')
  str3 = str2.dup
  str2.scan(result_numeric_placeholder) { |res| str3.sub!("#{result_numeric_prefix}#{res[0]}", "#{result_variable}#{res[0]}") }

  str4 = str3.gsub(/([^!])!([^!])/, '\1!!\2')

  res << str4
end

File.open(ARGV[0], 'w') { |file| file.write(res) }

# puts res