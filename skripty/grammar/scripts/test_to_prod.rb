
source = File.open('../../../snippets/fix_test.st')

File.open('../../../snippets/fix_prod.st', 'w') { |target|
  res = ''
  source.each_line do |line|
    next if /Transcript show:/.match(line)
    line.gsub!('TestingParser', 'ProductionParser')

    # puts line
    res << line
  end
  target.write(res)
}