#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "usage: ./print_dot.sh input.dot output.ps"
    exit 1
fi

dot -Tps $1 -o $2