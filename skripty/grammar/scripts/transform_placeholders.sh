#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo "usage: ./transform_placeholders.sh <action_file>"
    exit 1
fi

ruby helpers/transform_placeholders.rb "$1"