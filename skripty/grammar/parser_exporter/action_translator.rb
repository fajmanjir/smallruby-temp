module STCC
  class ActionTranslator

    def initialize
      @result_variable = 'result' # where the value of a nonterminal is stored
      @result_placeholder = '@@'
      @result_numeric_placeholder = /@([\d]+)/
      @result_numeric_prefix = '@'
    end

    # generate template of actions, block should comment out given code
    def generate_template(grammar)
      Printer.fatal_error 'ActionTranslator.generate_template: missing block' unless block_given?

      res = ''

      if grammar.actions.length == 0
        res << <<-END

# no action found

END
      end

      grammar.actions.each do |name, action|
        res << "add_action '#{name}', <<-END\n"

        res << yield(action.action)

        res << "END\n\n"
      end

      res
    end

    # translate actions in grammar according to the source DSL
    def translate(grammar, dsl)
      @translations = {}
      @local_vars = {}
      instance_eval dsl

      @translations.each do |name, action|
        if grammar.actions.has_key? name
          action2 = action.gsub(/([^!])!([^!])/, '\1!!\2')
          grammar.actions[name].action = action2
          grammar.actions[name].local_vars = @local_vars[name] unless grammar.actions[name].local_vars.nil?
        else
          Printer.warn "ActionTranslator: action #{name} was not found in grammar"
        end
      end
    end

    # change value placeholders in parameter Action
    def transform_placeholders(action)
      str2 = action.action.gsub(@result_placeholder, @result_variable)
      str3 = str2.dup
      str2.scan(@result_numeric_placeholder) { |res| str3.sub!("#{@result_numeric_prefix}#{res[0]}", "#{@result_variable}#{res[0]}") }
      action.action = str3
    end

    # DSL

    # add action
    def add_action(name, action)
      # action.local_vars = local_vars
      @translations[name] = action
    end

    # add local variable declarations (space separated string) for action with name
    def add_local_vars(name, local_vars)
      @local_vars[name] = local_vars
    end

    attr_accessor :result_variable

    private

  end
end