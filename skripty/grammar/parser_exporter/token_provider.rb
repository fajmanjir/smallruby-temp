module STCC
  class TokenProvider

    def initialize
      @nonterminals = {}
    end

    def symbol_code(symbol)
      return terminal_code symbol if symbol.is_a? TerminalSymbol
      return nonterminal_code symbol if symbol.is_a? NonterminalSymbol
      return 0 if symbol.is_a? EpsilonSymbol
      Printer.fatal_error "TokenProvider.symbol.code: cannot handle symbol #{symbol}"
    end

    def terminal_code(terminal)
      if /^'[\\]?.'$/.match(terminal.name)
        if terminal.name.length == 3
          return terminal.name[1].ord # ascii code
        else
          return case terminal.name[2]
                 when 'v'; "\v".ord
                 when 't'; "\t".ord
                 when 'f'; "\f".ord
                 when 'r'; "\r".ord
                 when 'n'; "\n".ord
                 when 'r'; "\r".ord
                 when 'b'; "\b".ord
                 else
                   Printer.fatal_error "terminal symbol code #{terminal.name} not found"
                 end
        end
      end

      case terminal.name
               when 'keyword_class';	258
               when 'keyword_module';	259
               when 'keyword_def';	260
               when 'keyword_undef';	261
               when 'keyword_begin';	262
               when 'keyword_rescue';	263
               when 'keyword_ensure';	264
               when 'keyword_end';	265
               when 'keyword_if';	266
               when 'keyword_unless';	267
               when 'keyword_then';	268
               when 'keyword_elsif';	269
               when 'keyword_else';	270
               when 'keyword_case';	271
               when 'keyword_when';	272
               when 'keyword_while';	273
               when 'keyword_until';	274
               when 'keyword_for';	275
               when 'keyword_break';	276
               when 'keyword_next';	277
               when 'keyword_redo';	278
               when 'keyword_retry';	279
               when 'keyword_in';	280
               when 'keyword_do';	281
               when 'keyword_do_cond';	282
               when 'keyword_do_block';	283
               when 'keyword_do_LAMBDA';	284
               when 'keyword_return';	285
               when 'keyword_yield';	286
               when 'keyword_super';	287
               when 'keyword_self';	288
               when 'keyword_nil';	289
               when 'keyword_true';	290
               when 'keyword_false';	291
               when 'keyword_and';	292
               when 'keyword_or';	293
               when 'keyword_not';	294
               when 'modifier_if';	295
               when 'modifier_unless';	296
               when 'modifier_while';	297
               when 'modifier_until';	298
               when 'modifier_rescue';	299
               when 'keyword_alias';	300
               when 'keyword_defined';	301
               when 'keyword_BEGIN';	302
               when 'keyword_END';	303
               when 'keyword__LINE__';	304
               when 'keyword__FILE__';	305
               when 'keyword__ENCODING__';	306
               when 'tIDENTIFIER';	307
               when 'tFID';	308
               when 'tGVAR';	309
               when 'tIVAR';	310
               when 'tCONSTANT';	311
               when 'tCVAR';	312
               when 'tLABEL';	313
               when 'tINTEGER';	314
               when 'tFLOAT';	315
               when 'tRATIONAL';	316
               when 'tIMAGINARY';	317
               when 'tSTRING_CONTENT';	318
               when 'tCHAR';	319
               when 'tNTH_REF';	320
               when 'tBACK_REF';	321
               when 'tREGEXP_END';	322
               when 'tUPLUS';	323
               when 'tUMINUS';	324
               when 'tPOW';	325
               when 'tCMP';	326
               when 'tEQ';	327
               when 'tEQQ';	328
               when 'tNEQ';	329
               when 'tGEQ';	330
               when 'tLEQ';	331
               when 'tANDOP';	332
               when 'tOROP';	333
               when 'tMATCH';	334
               when 'tNMATCH';	335
               when 'tDOT2';	336
               when 'tDOT3';	337
               when 'tAREF';	338
               when 'tASET';	339
               when 'tLSHFT';	340
               when 'tRSHFT';	341
               when 'tCOLON2';	342
               when 'tCOLON3';	343
               when 'tOP_ASGN';	344
               when 'tASSOC';	345
               when 'tLPAREN';	346
               when 'tLPAREN_ARG';	347
               when 'tRPAREN';	348
               when 'tLBRACK';	349
               when 'tLBRACE';	350
               when 'tLBRACE_ARG';	351
               when 'tSTAR';	352
               when 'tDSTAR';	353
               when 'tAMPER';	354
               when 'tLAMBDA';	355
               when 'tSYMBEG';	356
               when 'tSTRING_BEG';	357
               when 'tXSTRING_BEG';	358
               when 'tREGEXP_BEG';	359
               when 'tWORDS_BEG';	360
               when 'tQWORDS_BEG';	361
               when 'tSYMBOLS_BEG';	362
               when 'tQSYMBOLS_BEG';	363
               when 'tSTRING_DBEG';	364
               when 'tSTRING_DEND';	365
               when 'tSTRING_DVAR';	366
               when 'tSTRING_END';	367
               when 'tLAMBEG';	368
               when 'tLOWEST';	369
               when 'tUMINUS_NUM';	370
               when 'tLAST_TOKEN';	371
               else
                 Printer.fatal_error "Terminal code of #{terminal} not found"
             end
    end

    def nonterminal_code(nonterminal)
      return @nonterminals[nonterminal] if @nonterminals.has_key? nonterminal
      @nonterminals[nonterminal] = '_' + (@nonterminals.length + 1).to_s
    end

  end
end