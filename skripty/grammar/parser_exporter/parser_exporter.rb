module STCC
  class ParserExporter

    def initialize(parser)
      @parser = parser
    end

    # export custom parser in target language
    def export_parser
      raise NotImplementedError
    end
  end
end