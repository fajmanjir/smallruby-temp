require 'forwardable'

require_relative 'parser_exporter'
require_relative 'action_translator'
require_relative 'token_provider'

module STCC
  # bottom up parser exporter to SmallTalk
  class SmallTalkBUParserExporter < ParserExporter
    extend Forwardable

    def initialize(parser)
      super(parser)

      @action_translator = ActionTranslator.new

      # defaults
      @namespace = 'Ruby'
      @package = 'ctu:smallruby/parser'
      @category = 'Ruby Parser - Grammar'
      @class_name = 'TestingParser'
      @terminals_enum = ''

      @error_call = 'Transcript error:'
      @print_call = 'Transcript show:'
      @state_prefix = 's_' # state method prefix
      @state_number_separator = '_'
      @default_suffix = '_default'
      @next_token_declaration = 'nextLexem'
      @next_token = "self #{@next_token_declaration}"
      @error_message = 'unexpected token'
      @last_message_variable = 'lastMessage'

      @max_iterations = 10000

      # debug
      @debug = false

      # checks MessageNotUnderstood exceptions for possible errors
      # turn off to improve performance
      @secure = true

    end

    def generate_actions_template(filename = nil)
      res = @action_translator.generate_template(@parser.grammar) do |action|
        ret = ''
        # comment out all lines
        action.split("\n").each do |line|
          ret << "    \"#{escape_double_quotes(line)}\"\n"
        end

        ret
      end

      if filename
        File.open(filename, 'w') do |file|
          file.write(res);
        end
      end
      res
    end

    def translate_actions_from_file(filename)
      file = File.read(filename)

      @action_translator.translate(@parser.grammar, file)
    end

    # export custom parser in target language
    def export_parser(filename = nil)

      @parser.grammar.actions.each do |name, action|
        @action_translator.transform_placeholders(action)
      end

      res = generate_header
      res <<= generate_body
      res <<= generate_footer

      if filename
        File.open(filename, 'w') do |file|
          file.write(res);
        end
      end
      res
    end

    def generate_body
      @token_provider = TokenProvider.new

      res = <<-END
!#{ @class_name } methodsFor:'parsing'!

start
    "Main parser loop"
    | iterationsLeft |
    iterationsLeft := #{ @max_iterations }.

    self lexem: (inputReader nextLexem).

    [ (self stop not) and: [iterationsLeft > 0] ] whileTrue: [
        [
            self perform: ((String streamContents: [ :stream |
                stream nextPutAll: '#{ @state_prefix }';
                       nextPutAll: (self stateStack last printString);
                       nextPutAll: '_';
                       nextPutAll: (self lexem tokenType printString)
                ]) asSymbol).
        ] on: MessageNotUnderstood do:[ :msg |
END
      if @secure
        res <<= <<-END
            ((msg message selector) = (String streamContents: [ :stream |
                stream nextPutAll: '#{ @state_prefix }';
                       nextPutAll: (self stateStack last printString);
                       nextPutAll: '_';
                       nextPutAll: (self lexem tokenType printString)
            ])) ifFalse: [
                msg signal.
            ].
END
      end

      if @debug
        res <<= <<-END
            #{print "(self stateStack last printString, ': default - not found ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ); cr", false }
END
      end

      res <<= <<-END
            [
                self perform: ((String streamContents: [ :stream |
                    stream nextPutAll: '#{ @state_prefix }';
                           nextPutAll: (self stateStack last printString);
                           nextPutAll: '#{ @default_suffix }'
                    ]) asSymbol).
            ] on: MessageNotUnderstood do:[ :msg |
      END

      if @secure
        res <<= <<-END
                ((msg message selector) = (String streamContents: [ :stream |
                    stream nextPutAll: '#{ @state_prefix }';
                           nextPutAll: (self stateStack last printString);
                           nextPutAll: '#{ @default_suffix }'
                ])) ifFalse: [
                    msg signal.
                ].
END
      end

      res <<= <<-END
                #{ raise_error "('default of state ', (stateStack last printString), ' not found')", false }
            ].
        ].
        iterationsLeft := iterationsLeft - 1.
    ].

    (iterationsLeft = 0) ifTrue: [
        #{ raise_error "(stateStack last printString, ': ERROR: max loops count #{@max_iterations} reached')", false }
    ].

    ^ self topNode

    "Created: / 21-10-2015 / 17:20:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 22:02:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!#{ @class_name } methodsFor:'states'!

END
      @parser.states.each do |state|
        state.action_table.each do |symbol, action|

          res <<= <<-END
#{ @state_prefix }#{ state.state_id }_#{ @token_provider.symbol_code(symbol) }
#{ serialize_action(action) }
!

END
        end

        res <<= <<-END
#{ @state_prefix }#{ state.state_id }#{ @default_suffix }
#{ serialize_action(state.default_action) }
!

END
      end
      res
    end

    def generate_header
      <<-END
"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 11-01-2016 at 20:42:53'                !

"{ Package: '#{@package}' }"

"{ NameSpace: #{ @namespace } }"

Object subclass:##{ @class_name }
           instanceVariableNames:'inputReader lexemPushbackStack lexem astBuilder
           internHandler stateStack symbolStack resultStack stop topNode'
    classVariableNames:''
    poolDictionaries:''
    category:'#{@category}'
!

!#{ @class_name } class methodsFor:'instance creation'!

new
    ^  self basicNew initialize; yourself

"Created: / 21-10-2015 / 17:32:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!#{ @class_name } methodsFor:'accessing'!

astBuilder
    ^ astBuilder
!

astBuilder:something
    astBuilder := something.
!

inputReader
    ^ inputReader
!

inputReader:something
    inputReader := something.
!

internHandler
    ^ internHandler
!

internHandler:something
    internHandler := something.
!

lexem
    "get last read lexem"
    ^ lexem
!

lexem:something
    "set last read lexem"
    lexem := something.
!

lexemPushbackStack
    ^ lexemPushbackStack
!

lexemPushbackStack:something
    lexemPushbackStack := something.
!

stateStack
    ^ stateStack
!

stateStack:something
    stateStack := something.
!

symbolStack
    ^ symbolStack
!

symbolStack:something
    symbolStack := something.
!

stop
    ^ stop
!

stop:something
    stop := something.
!

topNode
    ^ topNode
!

topNode:something
    topNode := something.
!

resultStack
    ^ resultStack
!

resultStack:something
    resultStack := something.
! !

!#{ @class_name } methodsFor:'initialization'!

initialize
    self stateStack: (OrderedCollection new).
    self symbolStack: (OrderedCollection new).
    self resultStack: (OrderedCollection new).
    self lexemPushbackStack: (OrderedCollection new).
    self astBuilder: (ASTBuilder initializeWithParser: self).
    self internHandler: (InternAdapter new). "TODO: pass as a parameter to initialize"

    "Created: / 21-10-2015 / 16:41:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 21-10-2015 / 22:47:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!#{ @class_name } methodsFor:'parsing'!

parse: aString
    self inputReader: (InputReader initializeWithString: aString).
    self stateStack removeAll.
    self symbolStack removeAll.
    self lexemPushbackStack removeAll.
    self resultStack removeAll.
    self topNode: nil.
    self stop: false.

    self stateStack add: 0.
    "self symbolStack add: 0."

    ^ self start.
! !

!#{ @class_name } methodsFor:'accessing behaviour'!

#{ @next_token_declaration }
    "Get next lexem from inputReader or from the lexem stack"
    self symbolStack add: (self lexem node).
    (lexemPushbackStack isEmpty) ifTrue: [
        self lexem: (inputReader nextLexem).
    ] ifFalse: [
        self lexem: (lexemPushbackStack removeLast).
    ].
    ^ self lexem

    "Created: / 21-10-2015 / 17:15:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 18:29:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

END
    end

    def generate_footer
      '  !'
    end

    def serialize_action(action)

      pre_declarations = "    \"#{escape_double_quotes(action.to_s)}\"\n"
      body = ''
      # body <<= "    #{print "(' pre, symbols: ', symbolStack size printString); cr", false }\n" if @debug
      body <<= "    Transcript cr.\n" if @debug && action.is_a?(ShiftAction)
      body <<= "    #{print "(stateStack last printString, ': #{escape_single_quotes(action.to_s)}'); cr", false }\n" if @debug
      locals = []
      if action.is_a? ShiftAction
        body <<= <<-END
    self stateStack add: #{action.parser_state.state_id}.
    #{ @next_token }.
END
      elsif action.is_a? GotoAction
        body <<= <<-END
    self stateStack add: #{action.parser_state.state_id}.
END
      elsif action.is_a? ReduceAction
        if action.dot_rule.last_symbol.is_a? ActionSymbol
          locals << result_variable
          # add all variables except of action symbol in the end
          values = action.dot_rule.symbols[0...-1].each_with_index.map { |symbol, index| "#{result_variable}#{index + 1}" }

          # fill local variables
          values.each_with_index do |symbol, index|
            pos = values.length - 1 - index
            if pos > 0
              body <<= "    #{result_variable}#{index + 1} := self symbolStack at: (self symbolStack size - #{pos}).\n"
            else
              body <<= "    #{result_variable}#{index + 1} := self symbolStack at: (self symbolStack size).\n"
            end
          end

          locals.push(*values)

          if values.length > 0
            body <<= <<-END

    "default: $$ := $1"
    #{result_variable} := #{result_variable}1.
END
          else
            body <<= <<-END

    "default: $$ := nil"
    #{result_variable} := nil.
END
          end

          locals.push(*action.dot_rule.last_symbol.local_vars) unless action.dot_rule.last_symbol.local_vars.nil?

          body <<= <<-END

#{action.dot_rule.last_symbol.action}
END

          if action.dot_rule.length > 1
            count = values.length
            body <<= <<-END
    self stateStack removeLast: #{count}.
    self symbolStack removeLast: #{count}.
    self symbolStack add: #{result_variable}.
            END
          elsif action.dot_rule.length == 1
            body <<= <<-END
    "self stateStack removeLast."
    "self symbolStack removeLast."
    self symbolStack add: #{result_variable}.
            END
          else # action.dot_rule.length == 0
            Printer.fatal_error 'action.dot_rule.length == 0 with action symbol - nonsense'
    #         body <<= <<-END
    # "self stateStack removeLast."
    # self symbolStack add: #{result_variable}.
    #         END
          end
        else
          # without action symbol in the end
          if action.dot_rule.length > 1
            count = action.dot_rule.length
            body <<= <<-END
    self stateStack removeLast: #{count}.
    self symbolStack removeLast: #{count - 1}.
END
          elsif action.dot_rule.length == 1
            body <<= <<-END
    self stateStack removeLast.
            END
          else # length == 0
            body <<= <<-END
    "self stateStack removeLast."
    self symbolStack add: nil.
            END
          end

        end

        body <<= <<-END
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: '#{ @state_prefix }';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '_#{ @token_provider.nonterminal_code(action.dot_rule.nonterminal) }'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        #{ raise_error "((stateStack last printString), ': Unexpected nonterminal #{escape_single_quotes(action.dot_rule.nonterminal.name)}')", false }
    ].
END
      elsif action.is_a? AcceptAction
        body <<= <<-END
    self symbolStack removeLast. "remove epsilon"
    self stop: true.
END
      elsif action.is_a? ErrorAction
        body <<= <<-END
    #{ raise_error "('#{@error_message} ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) ", false }
        END
      else
        Printer.fatal_error 'SmallTalkBUExporter.serialize_action: undefined action'
      end

      res = ''
      res <<= "#{pre_declarations}" unless pre_declarations.empty?
      res <<= "    | #{locals.join ' '} |\n" unless locals.empty?
      res <<= body
    end

    attr_accessor :namespace, :category, :class_name, :terminals_enum, :error_call, :error_message, :state_prefix,
                  :default_suffix, :max_iterations, :next_token, :debug, :secure
    def_delegators :@action_translator, :result_variable

    private

    # format error call
    def raise_error(message, escape = true, dot = true)
      if escape
        # escape apostrophe
        return "#{@error_call} '#{escape_single_quotes(message)}'."
      end

      "#{@error_call} #{message}#{'.' if dot}"
    end

    # format standard output call
    def print(message, escape = true, dot = true)
      if escape
        # escape apostrophe
        return "#{@print_call} '#{escape_single_quotes(message)}'."
      end

      "#{@print_call} #{message}#{'.' if dot}"
    end

    def escape_single_quotes(str)
      str2 = str.gsub("'", "''")
      str3 = str2.gsub('\\n', '{{NEW_LINE}}')
      str3.gsub('!', '!!')
    end

    def escape_double_quotes(str)
      str2 = str.gsub("\"", "\"\"")
      str3 = str2.gsub('\\n', '{{NEW_LINE}}')
      str3.gsub('!', '!!')
    end

  end
end