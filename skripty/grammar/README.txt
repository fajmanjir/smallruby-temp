===================================
=== SMALLTALK COMPILER-COMPILER ===
===================================

*--------*
| USAGE  |
*--------*

./run.sh <project name>

Produces following files (prefixed with project name) in directory 'out':
 - .cfg.dist - template for action translations
 - .dot - dot representation of states automaton created from a grammar
 - .ps - postscript image of states automaton created from a grammar
 - .st - final parser in SmallTalk (requires InputReader to work)
