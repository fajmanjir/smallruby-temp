#!/usr/bin/env bash

if [ "$#" -lt 1 ]; then
    echo "usage: ./run.sh [--verbose] <project_name>"
    exit 1
fi

time ruby starter.rb $1 $2