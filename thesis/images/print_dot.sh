#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "usage: ./print_dot.sh input.dot output.ps"
    exit 1;
fi

dot -Tpdf $1 -o $2
