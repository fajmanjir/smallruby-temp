For the new Ruby parser, I derived the design of the Rubinius lexical analyzer. And because the implementation of the new Ruby parser is really complex, I will discuss the most important parts of the tokenizer, especially the main loop, which is responsible for recognizing input characters and determining their purpose.

\section{Branches}

% goto motivation
In programming languages, there are two types of jump statements. These are known as conditional and unconditional branches. The first type performs branch only under certain conditions, while the second type jumps unconditionally, regardless of the any condition.

A \texttt{goto} statement or unconditional branch can be avoided in high-level programming languages, but this can hardly be done in low-level programming languages. This is because the high-level programming constructs such as \texttt{if} command are translated into low-level programming language equivalent of goto. \cite{GuideToAssemblyLanguage}

A \texttt{goto} statement allows us to perform an unconditional jump from the \texttt{goto} command to a statement equipped with a label. A \texttt{goto} statement is sometimes used in C language to increase performance of critical programs, such as system drivers. We can see an example of this goto statement on figure~\ref{fig:gotoDiagram}. \cite{tutorialspoint:goto}

\begin{figure}[htbp]
	\centering
	\includegraphics[scale=0.95, trim=3.5cm 8.2cm 5.3cm 2cm, clip=false]{images/cpp_goto_statement.jpg}
	\vspace{220pt}
	\caption{Example of a goto statement}
	\label{fig:gotoDiagram}
\end{figure}

\pagebreak

% use in low-level languages
% low readability
However, goto statements are always recommended not to be used, because Its usage implies some disadvantages. In particular, their usage in a program considerably decreases code readability. 

There are two types of jumps in the code:

\begin{itemize}
	\item Forward jump
	\item Backward jump
\end{itemize}

As a goto can perform both forward or backward jump, it is significantly harder for a programmar to trace the program flow. \cite{WebCodeExpert:goto}

\section{Goto in SmallTalk}

In the source code of Rubinius there we encounter many occurences of a goto statement. Almost whole main function of its lexical analyzer, the \texttt{yylex}, is written using goto statements. SmallTalk, however, does not support goto statements, so we must use a different construct to simulate the original functionality.

% Exception instead of goto
For this purpose, we will use exception handling mechanism instead of goto statement, which is not present. Let's create class \texttt{Goto}, whose purpose is to encapsulate the block of code, which should be executed next, and to raise an~exception containing this block, so that this code block can be executed by the exception handler.

Furthermore, we define these code block in advance even before entering the code sequence, which handles goto statements, and then we pass them with the exception. This allows us to write blocks of code only once, save them into local variables and then invoke when needed without redefining them on all places they are used on.

\begin{alltt}
|blockRetry blockNormalNewline ... |
blockRetry := [
    "code after original retry label"
    \dots
].
blockNormalNewline := [
    \dots
    Goto block: blockRetry.
].

"goto loop"
\dots
\end{alltt}

% example discussion
In the previous example, we have transformed code snippets, whose beginnings were originally marked with labels (an identifier followed by a colon) into SmallTalk code blocks. These block are stored in local variables and will be passed as arguments to a method of Goto class. This method then raises an exception containing the block reference. Goto exception handler loop will be discussed later in section~\ref{sec:exceptionHanglingLoop}.

% goto realization
Now, let's focus on the line which realizes goto parts:

\begin{alltt}
   Goto block: aBlock.
\end{alltt}

This new bit of code seems similar to the original C language code. It is obvious that SmallTalk has no built-in Goto construct, therefore we have to create a new class named Goto.

% Exceptions in SmallTalk
Java or C++ handles exceptions in a way that the block, that is supposed to throw an exception, is surrounded by a try-catch block. This is very similar in SmallTalk. Smalltalk exception handling only use on: do: method on a~block instead of try-catch.

The major difference, however, is discovered when we need to raise an exception. Java, C++ and similar programming languages utilize keyword \texttt{throw} to raise an exception. SmallTalk does not contain \texttt{throw} keyword, but makes use of \texttt{Error} class. This class comprises \texttt{signal} method, which substitutes keyword \texttt{throw}. The method causes an Error class (or its descendant class) to be raised.\cite{TutorialsPoint:JavaExceptionHandling}\cite{GNUSmalltalk:Exceptions}

% Goto class
And because our goto substitute uses exceptions, we need to derive the native Error class. The \texttt{block:} is a class method that arranges creation of a new instance of Goto and stores given block reference in instance variable. Then the \texttt{signal} message is sent and our goto handling loop takes place.

\section{Exception handling loop}
\label{sec:exceptionHanglingLoop}

At this moment we are able to raise an exception with appropriate block to be executed, but the exception is not caught. This is done by goto loop:

\begin{alltt}
    "Goto loop"
    block := retryBlock.
    [true] whileTrue: [
        [block value] on: Goto do: [ :goto |
            block := goto block.
        ].
    ]. 
\end{alltt}

First, we store code block, which will be executed first, to a local variable \texttt{block}. Then we enter an endless loop. Every time we run the current block a Goto exception is thrown to perform the unconditional jump.

Now we need to make sure that we leave the loop sometime. The output of the \texttt{yylex} function is an integer value, therefore we can directly return a~value from one of the local blocks and leave the whole method.

\section{Different levels}

Now we may perform a basic goto command in SmallTalk. However, Rubinius does not use only basic jumps.

\begin{alltt}
start:
if( [ condition ] )\{
    [ action A ]
    innerLabel:
    [ action B ]
\}
[ action C ]
\end{alltt}

In \texttt{yylex} function, we can find a label even in the middle of an if statement. Thus we need to take care of these situations:

\begin{enumerate}
	\item condition is met;
	\item condition is not met;
	\item jump to the label in the if statement.
\end{enumerate}

In the first case, we reach the label and we naturally continue behind it, in a more formal way we follow actions this order: action A, followed by action B, action~C.

The second case causes to jump after the if block, thus executes only Action C. The last case jumps right into the middle of the if block regardless the condition. This executes only action B followed by action C.

The problem is that the original implementation allows as many labels in one block as needed, whilst our SmallTalk implementation of a goto block cannot contain another labeled block.

% block separation
This can be figured out by separating blocks by the end of inner block and adding extra gotos before the inner label and to the end of the inner block.

\newpage

We can illustrate the situation as follows:
\begin{alltt}
start:
if( [ condition ] )\{
    [ action A ]
    goto innerLabel;
    innerLabel:
    [ action B ]
    goto end;
\}
goto end:
end:
[ action C ]
return result;
\end{alltt}

This may look a bit complicated or needless, but in this form we can straightforwardly transform the code into SmallTalk using our \texttt{goto} construct.

\begin{alltt}
|startBlock innerBlock endBlock|
startBlock := [
    ( [ condition ] )ifTrue: [
        [ action A ]
        Goto block: innerBlock.
    ].
    Goto block: endBlock.
].
innerBlock := [
    [ action B ]
    Goto block: endBlock.
].
endBlock := [
    [ action C ]
    ^ result
].
\end{alltt}

This the exact transcription of the previous schematics. Finally, we only have to set \texttt{startBlock} as entry point and enter the loop. Using this separation of code parts, we don't have to repeat pieces of code twice or more times.