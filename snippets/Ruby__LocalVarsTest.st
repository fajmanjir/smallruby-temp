"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:15'                !

"{ NameSpace: Ruby }"

TestCase subclass:#LocalVarsTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser - Test'
!

!LocalVarsTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!LocalVarsTest methodsFor:'tests'!

testBvDefined
    |lv tbl1|

    tbl1 := VTable initializeWithPrev: nil.

    lv := LocalVars new.
    lv vars add: (OrderedCollection new add: 5; add: 6; yourself).
    lv vars add: (OrderedCollection new add: 7; yourself).
    lv args add: (OrderedCollection new add: 10; yourself).
    lv args add: (OrderedCollection new add: 10; yourself).

    self assert: (lv bvDefined: 5).
    self assert: (lv bvDefined: 6).
    self assert: (lv bvDefined: 7).
    self assert: (lv bvDefined: 10).
    self assertFalse: (lv bvDefined: 1).
    self assertFalse: (lv bvDefined: 4).
    self assertFalse: (lv bvDefined: 11).

    "Created: / 24-10-2015 / 17:34:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 07-02-2016 / 15:18:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testLocalId
    |lv|

    lv := LocalVars new.
    lv vars add: (OrderedCollection new add: 5; add: 6; yourself).
    lv vars add: (OrderedCollection new add: 7; yourself).
    lv args add: (OrderedCollection new add: 10; yourself).
    lv args add: (OrderedCollection new add: 10; yourself).

    self assert: (lv localId: 5).
    self assert: (lv localId: 6).
    self assertFalse: (lv localId: 7).
    self assert: (lv localId: 10).
    self assertFalse: (lv localId: 1).
    self assertFalse: (lv localId: 4).
    self assertFalse: (lv localId: 11).

    "empty collection"
    lv := LocalVars new.
    self assertFalse: (lv localId: 5).
    self assertFalse: (lv localId: 10).

    "Created: / 24-10-2015 / 17:41:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

