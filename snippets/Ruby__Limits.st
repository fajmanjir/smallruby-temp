'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#Limits
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Limits class methodsFor:'enum'!

CHAR_BIT
    "Length of a char variable in bits according to the C libraries."
    ^ 8

    "Created: / 03-04-2015 / 16:01:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

