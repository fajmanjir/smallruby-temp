'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

TestCase subclass:#GotoTest
	instanceVariableNames:'inputReader'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser - Test'
!

!GotoTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!GotoTest methodsFor:'tests'!

testGoto
    |block1|

    block1 := [ ^ #block1code ].
    self should: [ Goto block: block1 ] raise: Goto.

    [ Goto block: block1 ] on: Goto do: [ :goto |
        self assert: (goto block = block1).          
        ^ 'success'
    ].
    self error: 'invalid block returned'.
    ^ 'fail'                            
    "|o|
    o := Array new:2.
    self assert: ( o size == 2 ).
    self should: [ o at:0 ] raise:Error.
    self shouldnt: [ o at:1 ] raise:Error."

    "Created: / 31-03-2015 / 11:37:03 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

