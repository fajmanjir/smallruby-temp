"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 11-01-2016 at 20:42:53'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#TestingParser
           instanceVariableNames:'inputReader lexemPushbackStack lexem astBuilder
           internHandler stateStack symbolStack resultStack stop topNode'
    classVariableNames:''
    poolDictionaries:''
    category:'Ruby Parser - Grammar'

!


Ruby::TestingParser removeSelector:#'s_86_41'!
Ruby::TestingParser removeSelector:#'s_86_365'!
Ruby::TestingParser removeSelector:#'s_86_263'!
Ruby::TestingParser removeSelector:#'s_86_270'!
Ruby::TestingParser removeSelector:#'s_86_264'!
Ruby::TestingParser removeSelector:#'s_86_295'!
Ruby::TestingParser removeSelector:#'s_86_296'!
Ruby::TestingParser removeSelector:#'s_86_297'!
Ruby::TestingParser removeSelector:#'s_86_298'!
Ruby::TestingParser removeSelector:#'s_86_299'!
Ruby::TestingParser removeSelector:#'s_86_292'!
Ruby::TestingParser removeSelector:#'s_86_293'!
Ruby::TestingParser removeSelector:#'s_86_336'!
Ruby::TestingParser removeSelector:#'s_86_337'!
Ruby::TestingParser removeSelector:#'s_86_43'!
Ruby::TestingParser removeSelector:#'s_86_45'!
Ruby::TestingParser removeSelector:#'s_86_42'!
Ruby::TestingParser removeSelector:#'s_86_47'!
Ruby::TestingParser removeSelector:#'s_86_37'!
Ruby::TestingParser removeSelector:#'s_86_325'!
Ruby::TestingParser removeSelector:#'s_86_94'!
Ruby::TestingParser removeSelector:#'s_86_38'!
Ruby::TestingParser removeSelector:#'s_86_326'!
Ruby::TestingParser removeSelector:#'s_86_62'!
Ruby::TestingParser removeSelector:#'s_86_330'!
Ruby::TestingParser removeSelector:#'s_86_60'!
Ruby::TestingParser removeSelector:#'s_86_331'!
Ruby::TestingParser removeSelector:#'s_86_327'!
Ruby::TestingParser removeSelector:#'s_86_328'!
Ruby::TestingParser removeSelector:#'s_86_329'!
Ruby::TestingParser removeSelector:#'s_86_334'!
Ruby::TestingParser removeSelector:#'s_86_335'!
Ruby::TestingParser removeSelector:#'s_86_340'!
Ruby::TestingParser removeSelector:#'s_86_341'!
Ruby::TestingParser removeSelector:#'s_86_332'!
Ruby::TestingParser removeSelector:#'s_86_333'!
Ruby::TestingParser removeSelector:#'s_86_63'!
Ruby::TestingParser removeSelector:#'s_86_58'!
Ruby::TestingParser removeSelector:#'s_86_268'!
Ruby::TestingParser removeSelector:#'s_86_282'!
Ruby::TestingParser removeSelector:#'s_86_272'!
Ruby::TestingParser removeSelector:#'s_86_125'!
Ruby::TestingParser removeSelector:#'s_86_269'!
Ruby::TestingParser removeSelector:#'s_86_265'!
Ruby::TestingParser removeSelector:#'s_86_345'!
Ruby::TestingParser removeSelector:#'s_86_93'!
Ruby::TestingParser removeSelector:#'s_86_0'!
Ruby::TestingParser removeSelector:#'s_86_61'!
Ruby::TestingParser removeSelector:#'s_86_351'!
Ruby::TestingParser removeSelector:#'s_86_283'!
Ruby::TestingParser removeSelector:#'s_86_368'!
Ruby::TestingParser removeSelector:#'s_86_284'!
Ruby::TestingParser removeSelector:#'s_86_44'!
Ruby::TestingParser removeSelector:#'s_86_10'!
Ruby::TestingParser removeSelector:#'s_86_59'!
Ruby::TestingParser removeSelector:#'s_86_124'!

Ruby::TestingParser removeSelector:#'s_38_349'!
Ruby::TestingParser removeSelector:#'s_86_349'!

"modifiers if, rescue, unless, until, while"
Ruby::TestingParser removeSelector:#'s_561_295'!
Ruby::TestingParser removeSelector:#'s_561_296'!
Ruby::TestingParser removeSelector:#'s_561_297'!
Ruby::TestingParser removeSelector:#'s_561_298'!
Ruby::TestingParser removeSelector:#'s_561_299'!


!Ruby::TestingParser methodsFor:'parsing'!

s_86_342
    "Reduce primary_value -> primary . action259"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce primary_value -> primary . action259'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    astBuilder valueExpr: result1.
    result := result1.
    (result isNil) ifTrue: [
        result := astBuilder NEW_NIL: nil.
    ].

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__26'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal primary_value').
    ].
!

s_86_91
    "Reduce primary_value -> primary . action259"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce primary_value -> primary . action259'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    astBuilder valueExpr: result1.
    result := result1.
    (result isNil) ifTrue: [
        result := astBuilder NEW_NIL: nil.
    ].

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__26'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal primary_value').
    ].
!

s_86_46
    "Reduce primary_value -> primary . action259"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce primary_value -> primary . action259'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    astBuilder valueExpr: result1.
    result := result1.
    (result isNil) ifTrue: [
        result := astBuilder NEW_NIL: nil.
    ].

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__26'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal primary_value').
    ].
!

s_86_default
    "Reduce arg -> primary . action182"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce arg -> primary . action182'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.


    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__21'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal arg').
    ].
!

s_106_default
    "Reduce var_ref -> user_variable . action402"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce var_ref -> user_variable . action402'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := astBuilder getTable: result1.
    (result isNil) ifTrue: [
        result := astBuilder NEW_BEGIN: nil positions: nil.
    ].

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__28'
        ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal var_ref').
    ].
!

s_106_344
     "Reduce var_lhs -> user_variable . action404"
     | result result1 |
     Transcript show: (stateStack last printString, ': Reduce var_lhs -> user_variable . action404'); cr.
     result1 := self symbolStack at: (self symbolStack size).

     "default: $$ := $1"
     result := result1.

     result := astBuilder assignable: result1 val: nil.

     self stateStack removeLast: 1.
     self symbolStack removeLast: 1.
     self symbolStack add: result.
     [
         self perform: ((String streamContents: [ :stream |
             stream nextPutAll: 's_';
                    nextPutAll: (self stateStack last printString);
                    nextPutAll: '__29'
             ]) asSymbol).
     ] on: MessageNotUnderstood do:[ :msg |
         Transcript error: ((stateStack last printString), ': Unexpected nonterminal var_lhs').
     ].
!

s_745_default
    "Reduce cname -> tCONSTANT ."
    Transcript show: (stateStack last printString, ': Reduce cname -> tCONSTANT .'); cr.
    self stateStack removeLast.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__104'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal cname').
    ].
!

s_745_342
    "Reduce primary -> primary_value tCOLON2 tCONSTANT . action215"
    | result result1 result2 result3 |
    Transcript show: (stateStack last printString, ': Reduce primary -> primary_value tCOLON2 tCONSTANT . action215'); cr.
    result1 := self symbolStack at: (self symbolStack size - 2).
    result2 := self symbolStack at: (self symbolStack size - 1).
    result3 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := astBuilder NEW_COLON2: result1 i: result3 positions: nil.

    self stateStack removeLast: 3.
    self symbolStack removeLast: 3.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__22'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal primary').
    ].

!

s_287_default
    "Reduce symbol -> tSYMBEG sym . action392"
    | result result1 result2 |
    Transcript show: (stateStack last printString, ': Reduce symbol -> tSYMBEG sym . action392'); cr.
    result1 := self symbolStack at: (self symbolStack size - 1).
    result2 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    "inputReader state: (StateExpr EXPR_END)."
    result := result2.

    self stateStack removeLast: 2.
    self symbolStack removeLast: 2.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__43'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal symbol').
    ].

!

s_394_46
    "Reduce keyword_variable -> keyword_self . action396"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce keyword_variable -> keyword_self . action396'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := TokenType keyword_self.

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__41'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal keyword_variable').
    ].

!

s_395_46
    "Reduce keyword_variable -> keyword_nil . action395"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce keyword_variable -> keyword_nil . action395'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := TokenType keyword_nil.

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__41'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal keyword_variable').
    ].

!

s_396_46
    "Reduce keyword_variable -> keyword_true . action397"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce keyword_variable -> keyword_true . action397'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := TokenType keyword_true.

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__41'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal keyword_variable').
    ].

!

s_397_46
    "Reduce keyword_variable -> keyword_false . action398"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce keyword_variable -> keyword_false . action398'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := TokenType keyword_false.

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__41'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal keyword_variable').
    ].

!

s_399_46
    "Reduce keyword_variable -> keyword__FILE__ . action399"
    | result result1 |
    Transcript show: (stateStack last printString, ': Reduce keyword_variable -> keyword__FILE__ . action399'); cr.
    result1 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := TokenType keyword__FILE__.

    self stateStack removeLast: 1.
    self symbolStack removeLast: 1.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__41'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal keyword_variable').
    ].

!

s_911_default
    "Reduce brace_block -> keyword_do @marker38 opt_block_param compstmt keyword_end . action347"
    | result result1 result2 result3 result4 result5 |
    Transcript show: (stateStack last printString, ': Reduce brace_block -> keyword_do @marker38 opt_block_param compstmt keyword_end . action347'); cr.
    result1 := self symbolStack at: (self symbolStack size - 4).
    result2 := self symbolStack at: (self symbolStack size - 3).
    result3 := self symbolStack at: (self symbolStack size - 2).
    result4 := self symbolStack at: (self symbolStack size - 1).
    result5 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := astBuilder NEW_ITER: result3 b: result4 positions: nil.
    "result nd_set_line: result2."
    "bv_pop($<vars>1);"

    self stateStack removeLast: 5.
    self symbolStack removeLast: 5.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__94'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal brace_block').
    ].

!

s_912_default
    "Reduce brace_block -> '{' @marker37 opt_block_param compstmt '}' . action345"
    | result result1 result2 result3 result4 result5 |
    Transcript show: (stateStack last printString, ': Reduce brace_block -> ''{'' @marker37 opt_block_param compstmt ''}'' . action345'); cr.
    result1 := self symbolStack at: (self symbolStack size - 4).
    result2 := self symbolStack at: (self symbolStack size - 3).
    result3 := self symbolStack at: (self symbolStack size - 2).
    result4 := self symbolStack at: (self symbolStack size - 1).
    result5 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

    result := astBuilder NEW_ITER: result3 b: result4 positions: nil.
    "nd_set_line($$, $<num>2);"
    "bv_pop($<vars>1);"

    self stateStack removeLast: 5.
    self symbolStack removeLast: 5.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__94'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal brace_block').
    ].

!

s_545_307
    "Reduce operation2 -> tIDENTIFIER ."
    Transcript show: (stateStack last printString, ': Reduce operation2 -> tIDENTIFIER .'); cr.
    self stateStack removeLast.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__121'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal operation2').
    ].

! !
