'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

TestCase subclass:#ExpressionGrammarTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Grammar - Test'
!

!ExpressionGrammarTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!ExpressionGrammarTest methodsFor:'tests'!

test1
    |grammar res|

    grammar := ExpressionGrammar new.

    res := grammar parse: '1'.
    self assert: (res type = (NodeType NODE_NUMBER)).

    res := grammar parse: '1 + 2'.
    self assert: (res type = (NodeType NODE_CALL)).
    self assert: (res u2 = '+').
    self assert: (res u1 type = (NodeType NODE_NUMBER)).
    self assert: (res u1 u1 = 1).
    self assert: (res u3 type = (NodeType NODE_NUMBER)).
    self assert: (res u3 u1 = 2).

    res := grammar parse: '1 + 2 * 3'.
    self assert: (res type = (NodeType NODE_CALL)).
    self assert: (res u1 type = (NodeType NODE_NUMBER)).
    self assert: (res u1 u1 = 1).
    self assert: (res u2 = '+').
    self assert: (res u3 type = (NodeType NODE_CALL)).
    self assert: (res u3 u2 = '*').
    self assert: (res u3 u1 u1 = 2).
    self assert: (res u3 u3 u1 = 3).

    res := grammar parse: '1 + 2 + 3'.
    self assert: (res type = (NodeType NODE_CALL)).
    self assert: (res u2 = '+').
    self assert: (res u1 type = (NodeType NODE_CALL)).
    self assert: (res u1 u2 = '+').
    self assert: (res u3 u1 = 3).
    self assert: (res u1 u1 u1 = 1).
    self assert: (res u1 u3 u1 = 2).

    "Modified: / 24-10-2015 / 11:33:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

