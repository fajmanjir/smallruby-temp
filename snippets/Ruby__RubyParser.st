"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 07-02-2016 at 11:55:58'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#RubyParser
	instanceVariableNames:'reader builder'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!RubyParser class methodsFor:'instance creation'!

new
^  self basicNew initialize; yourself

"Created: / 21-10-2015 / 17:32:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!RubyParser class methodsFor:'initialize'!

initializeWithInputReader: reader builder: builder
    "comment stating purpose of this message"

    |parser state|

    self error: 'not implemented'.

    "state := ParserState new.
    state initialize.  "  "TODO: initialize by default"

    "parser := self new.
    parser reader: reader;
           builder: builder;
           parserState: state.

    ^ parser
     "

    "Created: / 23-03-2015 / 21:25:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 19-09-2015 / 20:04:11 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!RubyParser class methodsFor:'states'!

s_0__1
    "Goto 1"
    self stateStack add: 1.
! !

!RubyParser methodsFor:'accessing'!

astBuilder
    ^ astBuilder
!

astBuilder:something
    astBuilder := something.
!

builder
    ^ builder
!

builder:something 
    builder := something.
!

inputReader
    ^ inputReader
!

inputReader:something
    inputReader := something.
!

internHandler
    ^ internHandler
!

internHandler:something
    internHandler := something.
!

lexem
    "get last read lexem"
    ^ lexem
!

lexem:something
    "set last read lexem"
    lexem := something.
!

lexemPushbackStack
    ^ lexemPushbackStack
!

lexemPushbackStack:something
    lexemPushbackStack := something.
!

reader
    ^ reader
!

reader:something
    reader := something.
!

resultStack
    ^ resultStack
!

resultStack:something
    resultStack := something.
!

stateStack
    ^ stateStack
!

stateStack:something
    lexemPushbackStack := something.
!

stop
    ^ stop
!

stop:something
    stop := something.
!

symbolStack
    ^ stateStack
!

symbolStack:something
    symbolStack := something.
! !

!RubyParser methodsFor:'initialization'!

initialize
    self stateStack: (OrderedCollection new).
    self symbolStack: (OrderedCollection new).
    self resultStack: (OrderedCollection new).
    self lexemPushbackStack: (OrderedCollection new).
    self astBuilder: (ASTBuilder new).
    self internHandler: (InternAdapter new). "TODO: pass as a parameter to initialize"

    "Created: / 21-10-2015 / 16:41:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 21-10-2015 / 22:47:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!RubyParser methodsFor:'parsing'!

parse: aString
self inputReader: (InputReader initializeWithString: aString).
    self stateStack removeAll.
    self symbolStack removeAll.
    self lexemPushbackStack removeAll.
    self resultStack removeAll.
    self stop: false.

    self stateStack add: 0.
    "self symbolStack add: 0."

    ^ self start.
!

start
    "Starting state"
    |iterationsLeft|
    iterationsLeft := 10000.

    self nextLexem.

    [ (self stop not) and: [iterationsLeft > 0] ] whileTrue: [
        [
            self perform: ((String streamContents: [ :stream |
                stream nextPutAll: 's_';
                       nextPutAll: (self stateStack last asString);
                       nextPutAll: '_';
                       nextPutAll: (self symbolStack last tokenType)
                ]) asSymbol).
        ] on: MessageNotUnderstood do:[ :msg |
            [
                self perform: ((String streamContents: [ :stream |
                    stream nextPutAll: 's_';
                           nextPutAll: (self stateStack last asString);
                           nextPutAll: '_default'
                    ]) asSymbol).
            ] on: MessageNotUnderstood do:[ :msg |
                Transcript error:  ('default of state ', (stateStack last asString), ' not found').
            ].
        ].
        iterationsLeft := iterationsLeft - 1.
    ].

    ^ #ast

    "Created: / 21-10-2015 / 17:20:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 22:02:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

