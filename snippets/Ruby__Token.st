'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#Token
	instanceVariableNames:'position flags'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Token methodsFor:'* uncategorized *'!

parserPreciseMbcLen
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 09-04-2015 / 13:54:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Token methodsFor:'accessing'!

flags
    ^ flags
!

flags:something
    flags := something.
!

position
    ^ position
!

position:something
    position := something.
! !

