'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#ExprParser
	instanceVariableNames:'inputReader lexemPushbackStack lexem astBuilder internHandler
		stateStack symbolStack resultStack stop'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Grammar'
!

!ExprParser class methodsFor:'instance creation'!

new
    ^  self basicNew initialize; yourself

"Created: / 21-10-2015 / 17:32:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExprParser methodsFor:'accessing'!

astBuilder
    ^ astBuilder
!

astBuilder:something
    astBuilder := something.
!

inputReader
    ^ inputReader
!

inputReader:something
    inputReader := something.
!

internHandler
    ^ internHandler
!

internHandler:something
    internHandler := something.
!

lexem
    "get last read lexem"
    ^ lexem
!

lexem:something
    "set last read lexem"
    lexem := something.
!

lexemPushbackStack
    ^ lexemPushbackStack
!

lexemPushbackStack:something
    lexemPushbackStack := something.
!

resultStack
    ^ resultStack
!

resultStack:something
    resultStack := something.
!

stateStack
    ^ stateStack
!

stateStack:something
    stateStack := something.
!

stop
    ^ stop
!

stop:something
    stop := something.
!

symbolStack
    ^ symbolStack
!

symbolStack:something
    symbolStack := something.
! !

!ExprParser methodsFor:'accessing behaviour'!

nextLexem
    "Get next lexem from inputReader or from the lexem stack"
    self symbolStack add: (self lexem node).
    (lexemPushbackStack isEmpty) ifTrue: [
        self lexem: (inputReader nextLexem).
    ] ifFalse: [
        self lexem: (lexemPushbackStack removeLast).
    ].
    ^ self lexem

    "Created: / 21-10-2015 / 17:15:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 18:29:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExprParser methodsFor:'initialization'!

initialize
    self stateStack: (OrderedCollection new).
    self symbolStack: (OrderedCollection new).
    self resultStack: (OrderedCollection new).
    self lexemPushbackStack: (OrderedCollection new).
    self astBuilder: (ASTBuilder new).
    self internHandler: (InternAdapter new). "TODO: pass as a parameter to initialize"

    "Created: / 21-10-2015 / 16:41:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 21-10-2015 / 22:47:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExprParser methodsFor:'parsing'!

parse: aString
    self inputReader: (InputReader initializeWithString: aString).
    self stateStack removeAll.
    self symbolStack removeAll.
    self lexemPushbackStack removeAll.
    self resultStack removeAll.
    self stop: false.

    self stateStack add: 0.
    "self symbolStack add: 0."

    ^ self start.
!

start
    "Main parser loop"
    | iterationsLeft |
    iterationsLeft := 10000.

    self lexem: (inputReader nextLexem).

    [ (self stop not) and: [iterationsLeft > 0] ] whileTrue: [
        [
            self perform: ((String streamContents: [ :stream |
                stream nextPutAll: 's_';
                       nextPutAll: (self stateStack last printString);
                       nextPutAll: '_';
                       nextPutAll: (self lexem tokenType printString)
                ]) asSymbol).
        ] on: MessageNotUnderstood do:[ :msg |
            ((msg message selector) = (String streamContents: [ :stream |
                stream nextPutAll: 's_';
                       nextPutAll: (self stateStack last printString);
                       nextPutAll: '_';
                       nextPutAll: (self lexem tokenType printString)
            ])) ifFalse: [
                msg signal.
            ].
            Transcript show: (self stateStack last printString, ': default - not found ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ); cr.
            [
                self perform: ((String streamContents: [ :stream |
                    stream nextPutAll: 's_';
                           nextPutAll: (self stateStack last printString);
                           nextPutAll: '_default'
                    ]) asSymbol).
            ] on: MessageNotUnderstood do:[ :msg |
                ((msg message selector) = (String streamContents: [ :stream |
                    stream nextPutAll: 's_';
                           nextPutAll: (self stateStack last printString);
                           nextPutAll: '_default'
                ])) ifFalse: [
                    msg signal.
                ].
                Transcript error: ('default of state ', (stateStack last printString), ' not found').
            ].
        ].
        iterationsLeft := iterationsLeft - 1.
    ].

    (iterationsLeft = 0) ifTrue: [
        Transcript error: (stateStack last printString, ': ERROR: max loops count 10000 reached').
    ].

    ^ self symbolStack last

    "Created: / 21-10-2015 / 17:20:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 22:02:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExprParser methodsFor:'states'!

s_0_314
    "Shift 1"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 1'); cr.
    self stateStack add: 1.
    self nextLexem.

!

s_0_47
    "Shift 2"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 2'); cr.
    self stateStack add: 2.
    self nextLexem.

!

s_0_61
    "Shift 3"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 3'); cr.
    self stateStack add: 3.
    self nextLexem.

!

s_0__1
    "Goto 4"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Goto 4'); cr.
    self stateStack add: 4.

!

s_0__2
    "Goto 5"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Goto 5'); cr.
    self stateStack add: 5.

!

s_0__3
    "Goto 6"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Goto 6'); cr.
    self stateStack add: 6.

!

s_0_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_10_default
    "Reduce arg -> '=' arg ."
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Reduce arg -> ''='' arg .'); cr.
    self stateStack removeLast: 2.
    self symbolStack removeLast: 1.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__2'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal arg').
    ].

!

s_11_default
    "Accept"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Accept'); cr.
    self symbolStack removeLast. "remove epsilon"
    self stop: true.

!

s_12_default
    "Reduce program -> arg ';' ."
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Reduce program -> arg '';'' .'); cr.
    self stateStack removeLast: 2.
    self symbolStack removeLast: 1.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__1'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal program').
    ].

!

s_13_default
    "Reduce arg -> primary tSTAR ."
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Reduce arg -> primary tSTAR .'); cr.
    self stateStack removeLast: 2.
    self symbolStack removeLast: 1.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__2'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal arg').
    ].

!

s_14_default
    "Reduce primary -> tINTEGER '+' . action2"
    | result result1 result2 |
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Reduce primary -> tINTEGER ''+'' . action2'); cr.
    result1 := self symbolStack at: (self symbolStack size - 1).
    result2 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

Transcript show: 'R primary'.
    self stateStack removeLast: 2.
    self symbolStack removeLast: 2.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__3'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal primary').
    ].

!

s_15_default
    "Reduce program -> '/' primary ';' ."
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Reduce program -> ''/'' primary '';'' .'); cr.
    self stateStack removeLast: 3.
    self symbolStack removeLast: 2.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__1'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal program').
    ].

!

s_1_43
    "Shift 7"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 7'); cr.
    self stateStack add: 7.
    self nextLexem.

!

s_1_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_2_314
    "Shift 8"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 8'); cr.
    self stateStack add: 8.
    self nextLexem.

!

s_2__3
    "Goto 9"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Goto 9'); cr.
    self stateStack add: 9.

!

s_2_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_3_314
    "Shift 1"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 1'); cr.
    self stateStack add: 1.
    self nextLexem.

!

s_3_61
    "Shift 3"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 3'); cr.
    self stateStack add: 3.
    self nextLexem.

!

s_3__2
    "Goto 10"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Goto 10'); cr.
    self stateStack add: 10.

!

s_3__3
    "Goto 6"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Goto 6'); cr.
    self stateStack add: 6.

!

s_3_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_4_0
    "Shift 11"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 11'); cr.
    self stateStack add: 11.
    self nextLexem.

!

s_4_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_5_59
    "Shift 12"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 12'); cr.
    self stateStack add: 12.
    self nextLexem.

!

s_5_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_6_352
    "Shift 13"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 13'); cr.
    self stateStack add: 13.
    self nextLexem.

!

s_6_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_7_352
    "Reduce primary -> tINTEGER '+' . action2"
    | result result1 result2 |
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Reduce primary -> tINTEGER ''+'' . action2'); cr.
    result1 := self symbolStack at: (self symbolStack size - 1).
    result2 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

Transcript show: 'R primary'.
    self stateStack removeLast: 2.
    self symbolStack removeLast: 2.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__3'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal primary').
    ].

!

s_7_default
    "Reduce arg -> tINTEGER '+' . action1"
    | result result1 result2 |
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Reduce arg -> tINTEGER ''+'' . action1'); cr.
    result1 := self symbolStack at: (self symbolStack size - 1).
    result2 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

Transcript show: 'R arg'.
    self stateStack removeLast: 2.
    self symbolStack removeLast: 2.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__2'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal arg').
    ].

!

s_8_43
    "Shift 14"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 14'); cr.
    self stateStack add: 14.
    self nextLexem.

!

s_8_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_9_59
    "Shift 15"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Shift 15'); cr.
    self stateStack add: 15.
    self nextLexem.

!

s_9_default
    "Error"
    Transcript show: (' pre, symbols: ', symbolStack size printString); cr.
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

! !

