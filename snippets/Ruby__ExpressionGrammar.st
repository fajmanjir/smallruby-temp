'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#ExpressionGrammar
	instanceVariableNames:'inputReader priorityStack lexemPushbackStack lexem astBuilder
		internHandler'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Grammar'
!

!ExpressionGrammar class methodsFor:'instance creation'!

new
    ^  self basicNew initialize; yourself

    "Created: / 21-10-2015 / 17:32:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExpressionGrammar methodsFor:'accessing'!

astBuilder
    ^ astBuilder
!

astBuilder:something
    astBuilder := something.
!

inputReader
    ^ inputReader
!

inputReader:something
    inputReader := something.
!

internHandler
    ^ internHandler
!

internHandler:something
    internHandler := something.
!

lexem
    "get last read lexem"
    ^ lexem

    "Modified (comment): / 21-10-2015 / 18:29:17 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

lexem:something
    "set last read lexem"
    lexem := something.

    "Modified (comment): / 21-10-2015 / 18:29:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

lexemPushbackStack
    ^ lexemPushbackStack
!

lexemPushbackStack:something
    lexemPushbackStack := something.
!

priorityStack
    ^ priorityStack
!

priorityStack:something
    priorityStack := something.

    "Modified (format): / 21-10-2015 / 17:08:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExpressionGrammar methodsFor:'accessing behaviour'!

nextLexem
    "Get next lexem from inputReader or from the lexem stack"
    (lexemPushbackStack isEmpty) ifTrue: [
        self lexem: (inputReader nextLexem).
    ] ifFalse: [
        self lexem: (lexemPushbackStack removeLast).
    ].
    ^  self lexem

    "Created: / 21-10-2015 / 17:15:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 18:29:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExpressionGrammar methodsFor:'initialization'!

initialize
    self priorityStack: (OrderedCollection new).
    self lexemPushbackStack: (OrderedCollection new).
    self astBuilder: (ASTBuilder new).
    self internHandler: (InternAdapter new). "TODO: pass as a parameter to initialize"

    "Created: / 21-10-2015 / 16:41:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 21-10-2015 / 22:47:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExpressionGrammar methodsFor:'parsing'!

parse: aString
    self inputReader: (InputReader initializeWithString: aString).
    self priorityStack removeAll.

    ^ self start.

    "Created: / 21-10-2015 / 16:46:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ExpressionGrammar methodsFor:'states'!

e1
    "Whole expression state"
    |arg1 arg2|

    arg1 := self num.
    arg2 := self e2.

    [arg2 isNil] whileFalse: [
        "arg2 is a CALL node"
        arg2 u1: (arg1).
        arg1 := arg2.
        priorityStack removeLast.
        arg2 := self e2.
    ].
    ^ arg1

    "Created: / 21-10-2015 / 17:47:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 24-10-2015 / 11:31:38 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

e2
    "E2 - begin with + or *"
    |arg1 arg2|
    arg1 := self nextLexem.

    [
        arg2 := self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 'e2_';
                   nextPutAll: (arg1 tokenType asString)
        ]) asSymbol).
        ^ arg2
    ] on: MessageNotUnderstood do:[ :msg |
        ^ nil
    ].

    "Created: / 21-10-2015 / 17:54:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 23-10-2015 / 16:32:17 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

e2_42
    "E2 *"
    |arg1|
    arg1 := self e3: 42.

    "arg1 is a node or nil"
    (arg1 isNil) ifTrue: [
        ^ nil
    ].
    ^ astBuilder NEW_CALL: nil m: 42 a: arg1 positions: nil.

    "Created: / 21-10-2015 / 18:35:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 21:03:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

e2_43
    "E2 +"
    |arg1|
    arg1 := self e3: 43.

    "arg1 is a node or nil"
    (arg1 isNil) ifTrue: [
        ^ nil
    ].
    ^ astBuilder NEW_CALL: nil m: 43 a: arg1 positions: nil.

    "Created: / 21-10-2015 / 18:09:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (format): / 21-10-2015 / 22:26:42 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

e3: anOpCode
    "E3 - evaluate operator priority"
    |operatorPriority arg2|
    operatorPriority := OperatorPriority find: anOpCode.
    (priorityStack last >= operatorPriority) ifTrue: [
        self lexemPushbackStack add: lexem.
        ^ nil
    ].
    priorityStack add: operatorPriority.
    arg2 := self e1.
    ^ arg2

    "Created: / 21-10-2015 / 18:25:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 20:46:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

num
    "read a number or variable"
    |arg1|
    arg1 := self nextLexem.
    (arg1 tokenType ~= (TokenType tINTEGER)) ifTrue: [
        self error: ('NUM: expexted tINTEGER, but given ' , (LexemPrinter printLexem: arg1 inputReader: inputReader)).
    ].
    ^ arg1 node

    "Created: / 21-10-2015 / 17:47:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 21:58:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

start
    "Starting state"
    self nextLexem.

    (lexem tokenType = 0) ifTrue: [
        ^ 'EMPTY SCRIPT'
    ] ifFalse: [
        lexemPushbackStack add: lexem.
    ].

    ((priorityStack isEmpty) or: [ priorityStack last ~= 0 ]) ifTrue: [
        priorityStack add: 0.
    ].

    ^ self e1.

    "Created: / 21-10-2015 / 17:20:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 22:02:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

