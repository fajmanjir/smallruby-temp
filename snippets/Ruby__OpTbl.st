"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:15'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#OpTbl
	instanceVariableNames:''
	classVariableNames:'opTbl parserState'
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

OpTbl class instanceVariableNames:'opTable'

"
 No other class instance variables are inherited by this class.
"
!

!OpTbl class methodsFor:'accessing'!

opTable
    ^ opTable
!

opTable:something
    opTable := something.
! !

!OpTbl class methodsFor:'helper'!

containsName: aName
    "search for name in opTbl"
    self op_tbl do: [ :item |
        (item name = aName) ifTrue: [
            ^ true
        ].
    ].

    ^ false

    "Created: / 11-02-2016 / 14:57:11 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

convert_op: id
    "search for operator (TokenType) in opTbl"
    |result|

    "Initialize the table if not done yet"
    "self op_tbl."

    result := id.

    self op_tbl do: [ :item |
        (item token = id) ifTrue: [
            ^ self parser_intern: (item name)
        ].
    ].

    ^ result

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 23-10-2015 / 22:10:46 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

op_tbl
    "Lazy loading of opTbl"
    (opTable isNil)
        ifTrue: [
            opTable := { 
                (OpTblItem initializeWithToken:  (TokenType tDOT2) name: '..') .
                (OpTblItem initializeWithToken:  (TokenType tDOT3) name: '...') .
                (OpTblItem initializeWithToken:  (TokenType tPOW) name: '**') .
                (OpTblItem initializeWithToken:  (TokenType tDSTAR) name: '**') .
                (OpTblItem initializeWithToken:  (TokenType tUPLUS) name: '+@') .
                (OpTblItem initializeWithToken:  (TokenType tUMINUS) name: '-@') .
                (OpTblItem initializeWithToken:  (TokenType tCMP) name: '<=>') .
                (OpTblItem initializeWithToken:  (TokenType tGEQ) name: '>=') .
                (OpTblItem initializeWithToken:  (TokenType tLEQ) name: '<=') .
                (OpTblItem initializeWithToken: (TokenType tEQ) name: '==') .
                (OpTblItem initializeWithToken: (TokenType tEQQ) name: '===') .
                (OpTblItem initializeWithToken: (TokenType tNEQ) name: '!!=') .
                (OpTblItem initializeWithToken: (TokenType tMATCH) name: '=~') .
                (OpTblItem initializeWithToken: (TokenType tNMATCH) name: '!!~') .
                (OpTblItem initializeWithToken: (TokenType tAREF) name: '[]') .
                (OpTblItem initializeWithToken: (TokenType tASET) name: '[]=') .
                (OpTblItem initializeWithToken: (TokenType tLSHFT) name: '<<') .
                (OpTblItem initializeWithToken: (TokenType tRSHFT) name: '>>') .
                (OpTblItem initializeWithToken: (TokenType tCOLON2) name: '::') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $!!) name: '!!') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $%) name: '%') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $&) name: '&') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $*) name: '*') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $+) name: '+') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $-) name: '-') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $/) name: '/') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $<) name: '<') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $>) name: '>') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $^) name: '^') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $`) name: '`') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $|) name: '|') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $~) name: '~')
            }.
        ].

    ^ opTable

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 23-10-2015 / 22:19:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!OpTbl class methodsFor:'internal'!

parser_intern2: name length: aLength
    "TODO: DEPRECATED from parser_intern method"
    ^ name
    "^ self parser_intern3: name length: aLength enc: (self parser_usascii_encoding)"

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 23-10-2015 / 11:48:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parser_intern3: name length: length
    "TODO: not implemented"
    self shouldImplement.

    ^ nil

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 11-02-2016 / 14:56:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parser_intern: aName
    "TODO: DEPRECATED, use InternalAdapter instead"
    |length|
    "(aName isMemberOf: String) ifTrue: [
        length := aName length.
    ] ifFalse: [
        length := 1.
    ]."
    ^ aName

    "^ self parser_intern2: name length: (length)"

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 23-10-2015 / 22:09:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parser_usascii_encoding
    "TODO: not implemented"
    ^ nil

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!OpTbl methodsFor:'accessing'!

parserState
    ^ parserState

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parserState: something
    parserState := something.

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

