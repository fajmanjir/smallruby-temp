"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#Lexem
	instanceVariableNames:'tokenType node'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!Lexem class methodsFor:'instance creation'!

initializeWithTokenType: aTokenType
    ^ self new tokenType: aTokenType; yourself

    "Created: / 22-09-2015 / 11:42:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

initializeWithTokenType: aTokenType content: aContent
    ^ self new tokenType: aTokenType; content: aContent; yourself

    "Created: / 04-10-2015 / 15:23:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

initializeWithTokenType: aTokenType node: aNode
    ^ self new tokenType: aTokenType; node: aNode; yourself

    "Created: / 22-09-2015 / 11:42:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

initializeWithZero
    ^ self new tokenType: 0; yourself

    "Created: / 04-10-2015 / 16:17:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Lexem methodsFor:'accessing'!

content
    ^ node

    "Created: / 04-10-2015 / 15:22:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

content:something
    node := something.

    "Created: / 04-10-2015 / 15:22:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node
    ^ node
!

node:something
    node := something.
!

tokenType
    ^ tokenType
!

tokenType:something
    tokenType := something.
! !

