"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#Node
	instanceVariableNames:'nd_file u1 u2 u3 positions nodeFlags'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!Node class methodsFor:'initialization'!

initializeWithPositions: aPositions type: aType
    "Factory method of Node - creates node with given type default u1, u2, u3 parts"

    "TODO: use positions"
    ^ self new type: aType; yourself

    "Created: / 22-09-2015 / 10:58:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Node class methodsFor:'instance creation'!

new
    "return an initialized instance"

    ^ self basicNew initialize.

    "Created: / 21-09-2015 / 12:43:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Node class methodsFor:'enum'!

LMASK
    ^ (1 bitShift: (4 * (Limits CHAR_BIT) - (self LSHIFT))) - 1

    "Created: / 26-10-2015 / 12:22:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

LSHIFT
    ^ self TYPESHIFT + 7

    "Created: / 26-10-2015 / 11:50:21 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FL_NEWLINE
    ^ 1 << 7

    "Created: / 07-02-2016 / 21:59:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

TYPEMASK
    ^ 16r7f << (self TYPESHIFT)
    "
     optional: comment giving example use
    "

"
 change the above template into real code;
 remove this comment.
 Then `accept' either via the menu 
 or via the keyboard (usually CMD-A).

 You do not need this template; you can also
 select any existing methods code, change it,
 and finally `accept'. The method will then be
 installed under the selector as defined in the
 actual text - no matter which method is selected
 in the browser.

 Or clear this text, type in the method from scratch
 and install it with `accept'.
"

    "Created: / 22-03-2015 / 12:09:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

TYPESHIFT
    ^ 8

    "Created: / 22-03-2015 / 12:07:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Node methodsFor:'accessing'!

nd_1st
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:03:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_1st: aNd_1st
    self u1: aNd_1st.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:03:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_2nd
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:04:03 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_2nd: aNd_2nd
    self u2: aNd_2nd.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:04:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_aid
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:04:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_aid: aNd_aid
    self u3: aNd_aid.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (format): / 25-10-2015 / 16:05:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ainfo
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:04:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ainfo: aNd_ainfo
    self u3: aNd_ainfo.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (format): / 25-10-2015 / 16:05:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_alen
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:05:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_alen: aNd_alen
    self u2: aNd_alen.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_argc
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_argc: aNd_argc
    self u2: aNd_argc.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_args
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_args: aNd_args
    self u3: aNd_args.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (format): / 25-10-2015 / 16:11:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_beg
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_beg: aNd_beg
    self u1: aNd_beg.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:11:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_body
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_body: aNd_body
    self u2: aNd_body

.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:11:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cflag
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cflag: aNd_cflag
    self u2: aNd_cflag.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:11:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cfnc
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cfnc: aNd_cfnc
    self u1: aNd_cfnc.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:12:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_clss
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_clss: aNd_clss
    self u1: aNd_clss.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:12:21 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cnt
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:07:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cnt: aNd_cnt
    self u3: aNd_cnt.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:12:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cond
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cond: aNd_cond
    self u1: aNd_cond.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:12:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cpath
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cpath: aNd_cpath
    self u1: aNd_cpath.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:13:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cval
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cval: aNd_cval
    self u3: aNd_cval.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:13:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_defn
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:11 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_defn: aNd_defn
    self u3: aNd_defn.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:13:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_else
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_else: aNd_else
    self u3: aNd_else.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:13:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_end
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_end: aNd_end
    self u2: aNd_end.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:13:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ensr
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:21 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ensr: aNd_ensr
    self u3: aNd_ensr.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:14:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_entry
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_entry: aNd_entry
    self u3: aNd_entry.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:14:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_file
    ^ nd_file
!

nd_file:something
    nd_file := something.
!

nd_frml
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_frml: aNd_frml
    self u2: aNd_frml.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:14:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_func
    ^ u1

    "Created: / 25-10-2015 / 15:58:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_func: aFunc
    self u1: aFunc.

    "Created: / 25-10-2015 / 15:59:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_head
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_head: aNd_head
    self u1: aNd_head.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:14:43 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_iter
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:42 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_iter: aNd_iter
    self u3: aNd_iter.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:14:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_line
    ^ (self nodeFlags) >> (Node LSHIFT)

    "Created: / 26-10-2015 / 11:52:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 12-02-2016 / 22:07:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_lit
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:08:46 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_lit: aNd_lit
    self u1: aNd_lit.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:15:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_mid
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:13 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_mid: aNd_mid
    self u2: aNd_mid.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:15:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_modl
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:16 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_modl: aNd_modl
    self u1: aNd_modl.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:15:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_nest
    ^ u3

    "Created: / 26-10-2015 / 11:45:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_nest: aNd_nest
    self u3: aNd_nest.

    "Created: / 26-10-2015 / 11:45:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_next
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_next: aNd_next
    self u3: aNd_next.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:15:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_noex
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_noex: aNd_noex
    self u3: aNd_noex.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:15:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_nth
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_nth: aNd_nth
    self u2: aNd_nth.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:16:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_oid
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_oid: aNd_oid
    self u1: aNd_oid.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:16:21 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_opt
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_opt: aNd_opt
    self u1: aNd_opt.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:16:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_orig
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_orig: aNd_orig
    self u3: aNd_orig.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:16:43 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_pid
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_pid: aNd_pid
    self u1: aNd_pid.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:16:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_plen
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:42 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_plen: aNd_plen
    self u2: aNd_plen.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:17:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_recv
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:45 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_recv: aNd_recv
    self u1: aNd_recv.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:17:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_resq
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_resq: aNd_resq
    self u2: aNd_resq.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:17:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rest
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rest: aNd_rest
    self u1: aNd_rest.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:17:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rval
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rval: aNd_rval
    self u2 value: aNd_rval.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:17:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_state
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:09:58 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_state: aNd_state
    self u3: aNd_state.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:17:58 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_stts
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_stts: aNd_stts
    self u1: aNd_stts.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:18:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_super
    ^ u3

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_super: aNd_super
    self u3: aNd_super.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:18:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tag
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tag: aNd_tag
    self u1: aNd_tag.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:18:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tbl
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tbl: aNd_tbl
    self u1: aNd_tbl.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:18:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_term
    ^ u2

    "Created: / 26-10-2015 / 12:29:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tval
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tval: aNd_tval
    self u2: aNd_tval.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:18:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_value
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_value: aNd_value
    self u2: aNd_value.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:19:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_var
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_var: aNd_var
    self u1: aNd_var.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:19:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_vid
    ^ u1

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_vid: aNd_vid
    self u1: aNd_vid.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:19:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_visi
    ^ u2

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:10:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_visi: aNd_visi
    self u2: aNd_visi.

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:19:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nodeFlags
    ^ nodeFlags
!

nodeFlags:something
    nodeFlags := something.
!

positions
    ^ positions
!

positions:something
    positions := something.
!

u1
    ^ u1
!

u1:something
    u1 := something.
!

u2
    ^ u2
!

u2:something
    u2 := something.
!

u3
    ^ u3
!

u3:something
    u3 := something.
! !

!Node methodsFor:'accessing behaviour'!

nd_paren
    ^ (self u2 bitShift: ((Limits CHAR_BIT) * -2))

    "Created: / 25-10-2015 / 17:58:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_set_line: aLine
    self nodeFlags: (((self nodeFlags) bitAnd: ((-1 bitShift: (self class LSHIFT)) bitInvert32)) bitOr: (aLine bitAnd: (self class LMASK)))

    "Created: / 07-02-2016 / 17:13:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_set_type: t
    "set new node type"
    |left right|
    left := ((self nodeFlags) bitAnd: (MathHelper bitInvert4Byte: (self class TYPEMASK))).

    right := (t << (self class TYPESHIFT)) & (self class TYPEMASK).
    nodeFlags := left | right.

    "Created: / 16-02-2016 / 16:46:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_type  
    "get node type"
    ^ (nodeFlags & (self class TYPEMASK)) >> (self class TYPESHIFT)

    "Created: / 08-02-2016 / 20:56:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_type: t
    "set new node type"
    |left right|
    left := ((self nodeFlags) bitAnd: (MathHelper bitInvert4Byte: (self class TYPEMASK))).

    right := (t << (self class TYPESHIFT)) & (self class TYPEMASK).
    nodeFlags := left | right.

    "Created: / 08-02-2016 / 20:56:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

type  
    "get node type"
    ^ (nodeFlags & (self class TYPEMASK)) >> (self class TYPESHIFT)

    "Created: / 22-03-2015 / 14:58:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 22-03-2015 / 16:01:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

type: t
    "set new node type"
    |left right|
    left := ((self nodeFlags) bitAnd: (MathHelper bitInvert4Byte: (self class TYPEMASK))).

    right := (t << (self class TYPESHIFT)) & (self class TYPEMASK).
    nodeFlags := left | right.

    "Created: / 22-03-2015 / 16:01:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-09-2015 / 16:09:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Node methodsFor:'initialization'!

initialize
    self nodeFlags: 0.

    self type: 0.

    "Created: / 21-09-2015 / 12:43:13 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 25-10-2015 / 16:02:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 13-02-2016 / 00:11:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

