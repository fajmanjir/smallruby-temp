"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#LocalVars
	instanceVariableNames:'vars args prev'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!LocalVars class methodsFor:'instance creation'!

initializeWithPrev: aPrev
    "return an initialized instance"

    ^ self basicNew prev: aPrev; initialize; yourself.

    "Created: / 07-02-2016 / 15:25:13 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

new
    ^ self basicNew initialize; yourself

    "Created: / 24-10-2015 / 16:46:58 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!LocalVars methodsFor:'accessing'!

args
    ^ args
!

args:something
    args := something.
!

prev
    ^ prev
!

prev:something
    prev := something.
!

vars
    ^ vars
!

vars:something
    vars := something.
! !

!LocalVars methodsFor:'initialize'!

initialize
    self vars: (VTable new).
    self args: (VTable new).

    "Created: / 24-10-2015 / 16:48:11 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 07-02-2016 / 15:25:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!LocalVars methodsFor:'queries'!

bvDefined: anId
    |a v|
    a := args.
    v := vars.

    [ v isNotNil ] whileTrue: [
        ((v tbl indexOf: anId) ~= 0) ifTrue: [
            ^ true
        ].

        ((a tbl indexOf: anId) ~= 0) ifTrue: [
            ^ true
        ].

        a := a prev.
        v := v prev.
    ].

    ^ false

    "Created: / 24-10-2015 / 17:03:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 12-02-2016 / 14:55:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

localId: anId
    "search for id only in the first table"
    |a v|
    a := args.
    v := vars.

    [ (v isNotNil) and: [v prev isNotNil] ] whileTrue: [
        a := a prev.
        v := v prev.
    ].

    ^ ((a tbl indexOf: anId) ~= 0) or: [ (v tbl indexOf: anId) ~= 0 ]

    "Created: / 24-10-2015 / 17:38:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 12-02-2016 / 14:12:17 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

localVar: anId
    vars tbl add: anId.
    ^ vars size - 1

    "Created: / 08-02-2016 / 23:18:13 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

