"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:22'                !

"{ NameSpace: Ruby }"

Validator subclass:#ParserValidator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Validator'
!

!ParserValidator class methodsFor:'validating'!

validateFile: aFile
    |parser|
    [
        parser := ProductionParser new.
        parser parse: (aFile contentsAsString).
        ^ 1 "success"
    ] on: Error do: [:e |
        Transcript show: e; cr.
        ^ 0
    ].

    "Created: / 15-02-2016 / 17:07:31 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-02-2016 / 23:16:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

validateFile: aFile stream: aStream
    |parser|
    [
        parser := ProductionParser new.
        parser parse: (aFile contentsAsString).
        ^ 1 "success"
    ] on: Error do: [:e |
        aStream nextPutAll: (e asString);
               nextPutAll: (String with: (Character cr)).

        ((parser inputReader input isNil) or: [parser inputReader input size = 0]) ifTrue: [
            ^ 0
        ].

        ^ ((parser inputReader pos - 1) / (parser inputReader input size))  asFixedPoint: 2
    ].

    "Created: / 15-02-2016 / 23:23:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-02-2016 / 17:25:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

