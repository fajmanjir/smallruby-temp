"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#ReservedKeywords
	instanceVariableNames:'keywordMap'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!ReservedKeywords class methodsFor:'initialization'!

initialize
    ^ self new initialize; yourself

    "Created: / 18-10-2015 / 16:45:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

new
    ^ self basicNew initialize; yourself

    "Created: / 12-02-2016 / 22:20:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ReservedKeywords methodsFor:'accessing'!

keywordMap
    ^ keywordMap
!

keywordMap:something
    keywordMap := something.
! !

!ReservedKeywords methodsFor:'accessing behaviour'!

find: aStr
    "find keyword by name"

    "ruby keyword contains 2-12 characters"
    ((aStr size < 2) or: [aStr size > 12]) ifTrue: [
        ^ nil
    ].

    [
        ^ (keywordMap at: aStr).
    ] on: KeyNotFoundError do: [
        ^ nil
    ].

    "Created: / 18-10-2015 / 17:33:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ReservedKeywords methodsFor:'initialization'!

initialize
    self keywordMap: (Dictionary new).
    keywordMap at: 'break' put: (Keyword initializeWithId: (TokenType keyword_break) id2: (TokenType keyword_break) state: (StateExpr EXPR_MID)).
    keywordMap at: 'else' put: (Keyword initializeWithId: (TokenType keyword_else) id2: (TokenType keyword_else) state: (StateExpr EXPR_BEG)).
    keywordMap at: 'nil' put: (Keyword initializeWithId: (TokenType keyword_nil) id2: (TokenType keyword_nil) state: (StateExpr EXPR_END)).
    keywordMap at: 'ensure' put: (Keyword initializeWithId: (TokenType keyword_ensure) id2: (TokenType keyword_ensure) state: (StateExpr EXPR_BEG)).
    keywordMap at: 'end' put: (Keyword initializeWithId: (TokenType keyword_end) id2: (TokenType keyword_end) state: (StateExpr EXPR_END)).
    keywordMap at: 'then' put: (Keyword initializeWithId: (TokenType keyword_then) id2: (TokenType keyword_then) state: (StateExpr EXPR_BEG)).
    keywordMap at: 'not' put: (Keyword initializeWithId: (TokenType keyword_not) id2: (TokenType keyword_not) state: (StateExpr EXPR_ARG)).
    keywordMap at: 'false' put: (Keyword initializeWithId: (TokenType keyword_false) id2: (TokenType keyword_false) state: (StateExpr EXPR_END)).
    keywordMap at: 'self' put: (Keyword initializeWithId: (TokenType keyword_self) id2: (TokenType keyword_self) state: (StateExpr EXPR_END)).
    keywordMap at: 'elsif' put: (Keyword initializeWithId: (TokenType keyword_elsif) id2: (TokenType keyword_elsif) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'rescue' put: (Keyword initializeWithId: (TokenType keyword_rescue) id2: (TokenType modifier_rescue) state: (StateExpr EXPR_MID)).
    keywordMap at: 'true' put: (Keyword initializeWithId: (TokenType keyword_true) id2: (TokenType keyword_true) state: (StateExpr EXPR_END)).
    keywordMap at: 'until' put: (Keyword initializeWithId: (TokenType keyword_until) id2: (TokenType modifier_until) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'unless' put: (Keyword initializeWithId: (TokenType keyword_unless) id2: (TokenType modifier_unless) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'return' put: (Keyword initializeWithId: (TokenType keyword_return) id2: (TokenType keyword_return) state: (StateExpr EXPR_MID)).
    keywordMap at: 'def' put: (Keyword initializeWithId: (TokenType keyword_def) id2: (TokenType keyword_def) state: (StateExpr EXPR_FNAME)).
    keywordMap at: 'and' put: (Keyword initializeWithId: (TokenType keyword_and) id2: (TokenType keyword_and) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'do' put: (Keyword initializeWithId: (TokenType keyword_do) id2: (TokenType keyword_do) state: (StateExpr EXPR_BEG)).
    keywordMap at: 'yield' put: (Keyword initializeWithId: (TokenType keyword_yield) id2: (TokenType keyword_yield) state: (StateExpr EXPR_ARG)).
    keywordMap at: 'for' put: (Keyword initializeWithId: (TokenType keyword_for) id2: (TokenType keyword_for) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'undef' put: (Keyword initializeWithId: (TokenType keyword_undef) id2: (TokenType keyword_undef) state: (StateExpr EXPR_FNAME)).
    keywordMap at: 'or' put: (Keyword initializeWithId: (TokenType keyword_or) id2: (TokenType keyword_or) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'in' put: (Keyword initializeWithId: (TokenType keyword_in) id2: (TokenType keyword_in) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'when' put: (Keyword initializeWithId: (TokenType keyword_when) id2: (TokenType keyword_when) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'retry' put: (Keyword initializeWithId: (TokenType keyword_retry) id2: (TokenType keyword_retry) state: (StateExpr EXPR_END)).
    keywordMap at: 'if' put: (Keyword initializeWithId: (TokenType keyword_if) id2: (TokenType modifier_if) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'case' put: (Keyword initializeWithId: (TokenType keyword_case) id2: (TokenType keyword_case) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'redo' put: (Keyword initializeWithId: (TokenType keyword_redo) id2: (TokenType keyword_redo) state: (StateExpr EXPR_END)).
    keywordMap at: 'next' put: (Keyword initializeWithId: (TokenType keyword_next) id2: (TokenType keyword_next) state: (StateExpr EXPR_MID)).
    keywordMap at: 'super' put: (Keyword initializeWithId: (TokenType keyword_super) id2: (TokenType keyword_super) state: (StateExpr EXPR_ARG)).
    keywordMap at: 'module' put: (Keyword initializeWithId: (TokenType keyword_module) id2: (TokenType keyword_module) state: (StateExpr EXPR_VALUE)).
    keywordMap at: 'begin' put: (Keyword initializeWithId: (TokenType keyword_begin) id2: (TokenType keyword_begin) state: (StateExpr EXPR_BEG)).
    keywordMap at: '__LINE__' put: (Keyword initializeWithId: (TokenType keyword__LINE__) id2: (TokenType keyword__LINE__) state: (StateExpr EXPR_END)).
    keywordMap at: '__FILE__' put: (Keyword initializeWithId: (TokenType keyword__FILE__) id2: (TokenType keyword__FILE__) state: (StateExpr EXPR_END)).
    keywordMap at: '__ENCODING__' put: (Keyword initializeWithId: (TokenType keyword__ENCODING__) id2: (TokenType keyword__ENCODING__) state: (StateExpr EXPR_END)).
    keywordMap at: 'END' put: (Keyword initializeWithId: (TokenType keyword_END) id2: (TokenType keyword_END) state: (StateExpr EXPR_END)).
    keywordMap at: 'alias' put: (Keyword initializeWithId: (TokenType keyword_alias) id2: (TokenType keyword_alias) state: (StateExpr EXPR_FNAME)).
    keywordMap at: 'BEGIN' put: (Keyword initializeWithId: (TokenType keyword_BEGIN) id2: (TokenType keyword_BEGIN) state: (StateExpr EXPR_END)).
    keywordMap at: 'defined?' put: (Keyword initializeWithId: (TokenType keyword_defined) id2: (TokenType keyword_defined) state: (StateExpr EXPR_ARG)).
    keywordMap at: 'class' put: (Keyword initializeWithId: (TokenType keyword_class) id2: (TokenType keyword_class) state: (StateExpr EXPR_CLASS)).
    keywordMap at: 'while' put: (Keyword initializeWithId: (TokenType keyword_while) id2: (TokenType modifier_while) state: (StateExpr EXPR_VALUE)).

    "Created: / 18-10-2015 / 16:45:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !


ReservedKeywords initialize!
