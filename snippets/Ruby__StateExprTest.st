'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

TestCase subclass:#StateExprTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser - Test'
!

!StateExprTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!StateExprTest methodsFor:'initialize / release'!

tearDown
    "common cleanup - invoked after testing."

    super tearDown

    "Modified (format): / 28-03-2015 / 15:24:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!StateExprTest methodsFor:'tests'!

testAnyShortcuts
    self assert: ((StateExpr EXPR_BEG_ANY) = 1601).
    self assert: ((StateExpr EXPR_ARG_ANY) = 48).
    self assert: ((StateExpr EXPR_END_ANY) = 14).

    "Created: / 02-04-2015 / 13:08:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

