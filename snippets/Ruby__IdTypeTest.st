'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ NameSpace: Ruby }"

TestCase subclass:#IdTypeTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST - Test'
!

!IdTypeTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!IdTypeTest methodsFor:'tests'!

test1
    |base|

    base := 512. "must be bigger than tLAST_TOKEN and have 4 least significant bits equal to zero"

    "isNotopId"
    self assert: (IdType isNotopId: ((TokenType tLAST_TOKEN) + 1)).
    self assertFalse: (IdType isNotopId: (TokenType tLAST_TOKEN)).
    "isLocalId"
    self assert: (IdType isLocalId: (base bitOr: (IdType ID_LOCAL))).
    self assertFalse: (IdType isLocalId: (base bitOr: (IdType ID_GLOBAL))).
    self assertFalse: (IdType isLocalId: (TokenType tLAST_TOKEN)).
    "isGlobalId"
    self assert: (IdType isGlobalId: (base bitOr: (IdType ID_GLOBAL))).
    self assertFalse: (IdType isGlobalId: (base bitOr: (IdType ID_ATTRSET))).
    "isInstanceId"          
    self assert: (IdType isInstanceId: (base bitOr: (IdType ID_INSTANCE))).
    self assertFalse: (IdType isInstanceId: (base bitOr: (IdType ID_ATTRSET))).
    "isAttrsetId"
    self assert: (IdType isAttrsetId: (base bitOr: (IdType ID_ATTRSET))).
    self assertFalse: (IdType isAttrsetId: (base bitOr: (IdType ID_GLOBAL))).
    "isConstId"
    self assert: (IdType isConstId: (base bitOr: (IdType ID_CONST))).
    self assertFalse: (IdType isConstId: (base bitOr: (IdType ID_ATTRSET))).
    "isClassId"
    self assert: (IdType isClassId: (base bitOr: (IdType ID_CLASS))).
    self assertFalse: (IdType isClassId: (base bitOr: (IdType ID_INSTANCE))).
    "isJunkId"
    self assert: (IdType isJunkId: (base bitOr: (IdType ID_JUNK))).
    self assertFalse: (IdType isJunkId: (base bitOr: (IdType ID_CLASS))).
    "isAsgnOrId"
    self assert: (IdType isAsgnOrId: (base bitOr: (IdType ID_GLOBAL))).
    self assert: (IdType isAsgnOrId: (base bitOr: (IdType ID_INSTANCE))).
    self assert: (IdType isAsgnOrId: (base bitOr: (IdType ID_CLASS))).
    self assertFalse: (IdType isAsgnOrId: (IdType ID_CLASS)).
    self assertFalse: (IdType isAsgnOrId: (base bitOr: (IdType ID_JUNK))).

    "Modified: / 19-10-2015 / 11:37:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

