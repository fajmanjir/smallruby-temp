"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ NameSpace: Ruby }"

Object subclass:#DebugTrace
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!DebugTrace class methodsFor:'displaying'!

mainSwitch: aText
    "main switch block debug"
    Transcript show: ('mainSwitch block: ' , aText); cr.

    "Created: / 19-09-2015 / 20:21:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

warning: aText
    "debug note, output can be disabled globally"
    Transcript show: ('warning: ' , aText); cr.

    "Created: / 04-10-2015 / 23:29:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

