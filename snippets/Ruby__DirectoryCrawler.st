"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#DirectoryCrawler
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Validator'
!

!DirectoryCrawler class methodsFor:'read'!

findAllWithSuffix: aSuffix dir: aDirName
    ^ aDirName asFilename recursiveDirectoryContents select: [ :filename |
        (filename copyFrom: (filename size - (aSuffix size - 1))) = aSuffix
    ].

    "Created: / 15-02-2016 / 16:15:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-02-2016 / 20:34:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

