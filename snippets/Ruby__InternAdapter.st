"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ NameSpace: Ruby }"

Object subclass:#InternAdapter
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Intern'
!

!InternAdapter methodsFor:'adaption'!

parserIntern: name

    self shouldImplement.

    ^ name

    "Created: / 18-10-2015 / 17:55:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 11-02-2016 / 14:02:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tokIntern: mb
    self error: 'Do not use InternAdapter#tokIntern.'.

    ^ mb

    "Created: / 18-10-2015 / 13:05:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 11-02-2016 / 12:15:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

