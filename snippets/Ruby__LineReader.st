"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ NameSpace: Ruby }"

Object subclass:#LineReader
	instanceVariableNames:'inputString position'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!LineReader class methodsFor:'instance creation'!

initializeWithString: aString
    ^ self basicNew inputString: aString; initialize; yourself.

    "Created: / 04-10-2015 / 12:35:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!LineReader methodsFor:'* uncategorized *'!

readLine
    "read the next line according to the position"
    |pos|

    self position: (self position + 1).
    pos := inputString nextIndexOf: (SpecialChar n) from: 1 to: (inputString size).

    self shouldImplement.

    "Created: / 04-10-2015 / 12:47:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!LineReader methodsFor:'accessing'!

inputString
    ^ inputString
!

inputString:something
    inputString := something.
!

position
    ^ position

    "Created: / 04-10-2015 / 12:40:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

position: something
    position := something.

    "Created: / 04-10-2015 / 12:40:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!LineReader methodsFor:'initialization'!

initialize
    "read the first line"

    position := 0.
    self readLine.

    "Created: / 04-10-2015 / 12:42:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

