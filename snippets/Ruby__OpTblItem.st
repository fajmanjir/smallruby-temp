'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#OpTblItem
	instanceVariableNames:'token name'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!


!OpTblItem class methodsFor:'initialize'!

initializeWithToken: token name: aName
    "create a pair token:name"
    ^ self new token: token; name: aName; yourself

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 23-10-2015 / 22:05:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!OpTblItem methodsFor:'accessing'!

name
    ^ name
!

name:something
    name := something.
!

token
    ^ token
!

token:something
    token := something.
! !

!OpTblItem class methodsFor:'documentation'!

version_HG

    ^ '$Changeset: <not expanded> $'
! !

