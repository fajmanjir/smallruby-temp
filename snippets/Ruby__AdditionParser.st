"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#AdditionParser
	instanceVariableNames:'inputReader lexemPushbackStack lexem astBuilder internHandler
		stateStack symbolStack resultStack stop topNode'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Grammar'
!

!AdditionParser class methodsFor:'instance creation'!

new
    ^  self basicNew initialize; yourself

"Created: / 21-10-2015 / 17:32:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!AdditionParser methodsFor:'accessing'!

astBuilder
    ^ astBuilder
!

astBuilder:something
    astBuilder := something.
!

inputReader
    ^ inputReader
!

inputReader:something
    inputReader := something.
!

internHandler
    ^ internHandler
!

internHandler:something
    internHandler := something.
!

lexem
    "get last read lexem"
    ^ lexem
!

lexem:something
    "set last read lexem"
    lexem := something.
!

lexemPushbackStack
    ^ lexemPushbackStack
!

lexemPushbackStack:something
    lexemPushbackStack := something.
!

resultStack
    ^ resultStack
!

resultStack:something
    resultStack := something.
!

stateStack
    ^ stateStack
!

stateStack:something
    stateStack := something.
!

stop
    ^ stop
!

stop:something
    stop := something.
!

symbolStack
    ^ symbolStack
!

symbolStack:something
    symbolStack := something.
!

topNode
    ^ topNode
!

topNode:something
    topNode := something.
! !

!AdditionParser methodsFor:'accessing behaviour'!

nextLexem
    "Get next lexem from inputReader or from the lexem stack"
    self symbolStack add: (self lexem node).
    (lexemPushbackStack isEmpty) ifTrue: [
        self lexem: (inputReader nextLexem).
    ] ifFalse: [
        self lexem: (lexemPushbackStack removeLast).
    ].
    ^ self lexem

    "Created: / 21-10-2015 / 17:15:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 18:29:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!AdditionParser methodsFor:'initialization'!

initialize
    self stateStack: (OrderedCollection new).
    self symbolStack: (OrderedCollection new).
    self resultStack: (OrderedCollection new).
    self lexemPushbackStack: (OrderedCollection new).
    self astBuilder: (ASTBuilder initializeWithParser: self).
    self internHandler: (InternAdapter new). "TODO: pass as a parameter to initialize"

    "Created: / 21-10-2015 / 16:41:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 21-10-2015 / 22:47:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!AdditionParser methodsFor:'parsing'!

parse: aString
    self inputReader: (InputReader initializeWithString: aString).
    self stateStack removeAll.
    self symbolStack removeAll.
    self lexemPushbackStack removeAll.
    self resultStack removeAll.
    self topNode: nil.
    self stop: false.

    self stateStack add: 0.
    "self symbolStack add: 0."

    ^ self start.
!

start
    "Main parser loop"
    | iterationsLeft |
    iterationsLeft := 10000.

    self lexem: (inputReader nextLexem).

    [ (self stop not) and: [iterationsLeft > 0] ] whileTrue: [
        [
            self perform: ((String streamContents: [ :stream |
                stream nextPutAll: 's_';
                       nextPutAll: (self stateStack last printString);
                       nextPutAll: '_';
                       nextPutAll: (self lexem tokenType printString)
                ]) asSymbol).
        ] on: MessageNotUnderstood do:[ :msg |
            ((msg message selector) = (String streamContents: [ :stream |
                stream nextPutAll: 's_';
                       nextPutAll: (self stateStack last printString);
                       nextPutAll: '_';
                       nextPutAll: (self lexem tokenType printString)
            ])) ifFalse: [
                msg signal.
            ].
            Transcript show: (self stateStack last printString, ': default - not found ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ); cr.
            [
                self perform: ((String streamContents: [ :stream |
                    stream nextPutAll: 's_';
                           nextPutAll: (self stateStack last printString);
                           nextPutAll: '_default'
                    ]) asSymbol).
            ] on: MessageNotUnderstood do:[ :msg |
                ((msg message selector) = (String streamContents: [ :stream |
                    stream nextPutAll: 's_';
                           nextPutAll: (self stateStack last printString);
                           nextPutAll: '_default'
                ])) ifFalse: [
                    msg signal.
                ].
                Transcript error: ('default of state ', (stateStack last printString), ' not found').
            ].
        ].
        iterationsLeft := iterationsLeft - 1.
    ].

    (iterationsLeft = 0) ifTrue: [
        Transcript error: (stateStack last printString, ': ERROR: max loops count 10000 reached').
    ].

    ^ self topNode

    "Created: / 21-10-2015 / 17:20:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-10-2015 / 22:02:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!AdditionParser methodsFor:'states'!

s_0_314
    "Shift 1"
    Transcript cr.
    Transcript show: (stateStack last printString, ': Shift 1'); cr.
    self stateStack add: 1.
    self nextLexem.

!

s_0__1
    "Goto 2"
    Transcript show: (stateStack last printString, ': Goto 2'); cr.
    self stateStack add: 2.

!

s_0__2
    "Goto 3"
    Transcript show: (stateStack last printString, ': Goto 3'); cr.
    self stateStack add: 3.

!

s_0_default
    "Reduce program -> ."
    Transcript show: (stateStack last printString, ': Reduce program -> .'); cr.
    "self stateStack removeLast."
    self symbolStack add: nil.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__1'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal program').
    ].

!

s_1_default
    |result|
    "Reduce arg -> tINTEGER ."
    Transcript show: (stateStack last printString, ': Reduce arg -> tINTEGER .'); cr.

    result := self symbolStack last u1.
    self symbolStack removeLast.
    self symbolStack add: result.
    self stateStack removeLast.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__2'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal arg').
    ].

    "Modified: / 09-02-2016 / 18:08:45 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

s_2_0
    "Shift 4"
    Transcript cr.
    Transcript show: (stateStack last printString, ': Shift 4'); cr.
    self stateStack add: 4.
    self nextLexem.

!

s_2_default
    "Error"
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_3_43
    "Shift 5"
    Transcript cr.
    Transcript show: (stateStack last printString, ': Shift 5'); cr.
    self stateStack add: 5.
    self nextLexem.

!

s_3_default
    "Reduce program -> arg ."
    Transcript show: (stateStack last printString, ': Reduce program -> arg .'); cr.
    self stateStack removeLast.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__1'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal program').
    ].

!

s_4_default
    "Accept"
    Transcript show: (stateStack last printString, ': Accept'); cr.
    self symbolStack removeLast. "remove epsilon"
    Transcript show: ('result: ', self symbolStack last asString); cr.
    self stop: true.

    "Modified: / 09-02-2016 / 18:11:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

s_5_314
    "Shift 1"
    Transcript cr.
    Transcript show: (stateStack last printString, ': Shift 1'); cr.
    self stateStack add: 1.
    self nextLexem.

!

s_5__2
    "Goto 6"
    Transcript show: (stateStack last printString, ': Goto 6'); cr.
    self stateStack add: 6.

!

s_5_default
    "Error"
    Transcript show: (stateStack last printString, ': Error'); cr.
    Transcript error: ('unexpected token ', (LexemPrinter findTokenType: (self lexem tokenType printString)) ) .

!

s_6_default
    "Reduce arg -> arg '+' arg . action1"
    | result result1 result2 result3 |
    Transcript show: (stateStack last printString, ': Reduce arg -> arg ''+'' arg . action1'); cr.
    result1 := self symbolStack at: (self symbolStack size - 2).
    result2 := self symbolStack at: (self symbolStack size - 1).
    result3 := self symbolStack at: (self symbolStack size).

    "default: $$ := $1"
    result := result1.

result := result1 + result3.
    self stateStack removeLast: 3.
    self symbolStack removeLast: 3.
    self symbolStack add: result.
    [
        self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 's_';
                   nextPutAll: (self stateStack last printString);
                   nextPutAll: '__2'
            ]) asSymbol).
    ] on: MessageNotUnderstood do:[ :msg |
        Transcript error: ((stateStack last printString), ': Unexpected nonterminal arg').
    ].

    "Modified: / 09-02-2016 / 18:04:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

