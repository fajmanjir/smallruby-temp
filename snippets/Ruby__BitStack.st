"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#BitStack
	instanceVariableNames:'value'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!BitStack class methodsFor:'initialize'!

initialize
    ^ self basicNew value: 0; yourself

    "Created: / 21-04-2015 / 18:11:46 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-02-2016 / 22:09:43 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

new
    ^ self basicNew value: 0; yourself

    "Created: / 16-02-2016 / 22:08:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!BitStack methodsFor:'accessing'!

value
    ^ value
!

value:something
    value := something.
! !

!BitStack methodsFor:'accessing - behavior'!

lexPop
    ^ value := (value >> 1) bitOr: (value bitAnd: 1).

    "Created: / 21-04-2015 / 18:10:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

pop
    ^ value := (value >> 1)

    "Created: / 21-04-2015 / 18:52:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

push: newBit
    ^ value := (value << 1) bitOr: (newBit bitAnd: 1).

    "Created: / 21-04-2015 / 18:42:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

top
    "top & 0x01"
    ^ (value bitAnd: 1) = 1.

    "Created: / 21-04-2015 / 18:54:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 24-10-2015 / 18:35:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !


BitStack initialize!
