"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:15'                !

"{ NameSpace: Ruby }"

TestCase subclass:#TestingParserTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Grammar - Test'
!

!TestingParserTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!TestingParserTest methodsFor:'syntax check'!

test2
    |parser res stringToParse|

    parser := TestingParser new.

    stringToParse := ('a').      

    Transcript show: stringToParse.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 01-02-2016 / 23:58:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testAssignNumber
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('abc = 15').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 17-02-2016 / 13:07:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testAssignable
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('abc = ddd()').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 17-02-2016 / 17:40:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testAttrReader
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('attr_reader :bins, :count').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 16:04:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:13 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testClassDefWithMethodInline
    |parser res stringToParse|

    parser := TestingParser new.

    stringToParse := ('class Tester;def tester;a = 10; b = 20;end;end'). " OK "   

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 11:21:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:17 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testClassDefWithMethodMultiline
    |parser res stringToParse|

    parser := TestingParser new.

    stringToParse := ('def tester
a = 10
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 11:21:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testComment
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('a = 5 # this is a comment', SpecialChar n, 'b').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 15:18:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testDefFuncEq
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('def ==(other);end').

    Transcript show: stringToParse; cr; cr.

    self shouldnt: [res := parser parse: stringToParse.] raise: Error.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:14:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testDefSelf
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('def self.from(ary); end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 16-02-2016 / 15:52:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testEmpty
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 18:23:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:31 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testEndOfInput
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('a').

    Transcript show: stringToParse; cr; cr.

    self shouldnt: [res := parser parse: stringToParse. ] raise: Error.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 11:38:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testEq
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('mod == @module').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:19:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testFcallParams
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('def new(arg1, arg2)
  b(x)
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 00:34:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 13-02-2016 / 00:14:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testFcallParamsExpand
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('def new(*arg1)
  b(x)
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 00:37:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testFcallWithTwoParams
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('a(1, 2)').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:13:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testFcallWithTwoParamsRaise
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('raise PrimitiveFailure, "Rubinius::VM.write_error primitive failed"').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:28:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testGetter
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('def size
entries()
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 16:30:16 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testIfElse
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('if other.kind_of? IncludedModule
  b = 5
else
  b = 10
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:24:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testIfModifier
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('a = 10 if b > 5').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 16-02-2016 / 15:30:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:09:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testIfModifierMultiline
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('def tester
a = 10 if b > 5
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 18:38:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testImplicitReturn
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('class Object
# Prints basic information about the object to stdout.
#
def __show__
Rubinius.primitive(:object_show)
123
end
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:31:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 12-02-2016 / 22:58:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testInclude
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('class Object
include Kernel
extend Forwardable
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:26:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testLambda
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('c.each do |x,y|
  b(x)
end').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 00:25:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testModulePrefix
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('copy = Rubinius::Type.class;').    "Works only with ending ;"

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 00:40:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testMultiline
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('a()
b').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 06-02-2016 / 11:23:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testOrOp
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('super || other.module').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:17:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testOrOpWithEq
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('super || other.module == @module').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:19:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testRequire
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('require ''kernel''').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 07-02-2016 / 12:27:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testTwoAssignments
    |parser res stringToParse|

    parser := TestingParser new. 

    stringToParse := ('a = 10; b = 20').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 12-02-2016 / 21:56:46 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testTwoNewlines
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('abc c

bcd').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 16-02-2016 / 14:59:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testYield
    |parser res stringToParse|

    parser := TestingParser new.              
    stringToParse := ('yield').

    Transcript show: stringToParse; cr; cr.

    res := parser parse: stringToParse.
    Transcript show: (NodeTypePrinter print: res indent: 0 globals: (parser inputReader symbolTable)).

    "Created: / 08-02-2016 / 22:50:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-02-2016 / 13:10:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

