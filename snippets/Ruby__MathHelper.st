'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#MathHelper
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!MathHelper class methodsFor:'bitwise'!

bitInvert4Byte: number
    "4 byte bitwise invertion"
    ^ (number | 16r100000000) bitInvert & 16r0FFFFFFFF

    "Created: / 22-03-2015 / 15:20:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

