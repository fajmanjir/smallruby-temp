'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

TestCase subclass:#LexemPrinterTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Helpers - Test'
!

!LexemPrinterTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!LexemPrinterTest methodsFor:'tests'!

testFindLexemDetails
    |inputReader lexem|

    inputReader := InputReader initializeWithString: '+='.



    lexem := inputReader nextLexem.
    self assert: ((LexemPrinter findLexemDetails: lexem inputReader: inputReader) = ', $+').

    lexem tokenType: (TokenType fromChar: $5).
    self assert: ((LexemPrinter findLexemDetails: lexem inputReader: inputReader) = '').

    "Created: / 19-10-2015 / 16:00:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testFindTokenType
    self assert: ((LexemPrinter findTokenType: 276) = 'keyword_break').
    self assert: ((LexemPrinter findTokenType: (TokenType fromChar: $F)) = '$F').
    self assert: ((LexemPrinter findTokenType: $F) = '$F').

    "Created: / 19-10-2015 / 16:00:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

