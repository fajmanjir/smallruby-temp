'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

TestCase subclass:#SpecialCharTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser - Test'
!

!SpecialCharTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!SpecialCharTest methodsFor:'initialize / release'!

tearDown
    "common cleanup - invoked after testing."

    super tearDown

    "Modified (format): / 28-03-2015 / 15:24:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!SpecialCharTest methodsFor:'tests'!

testIsSpace

    self assert: (SpecialChar isSpace: (SpecialChar space) asciiValue).
    self assert: (SpecialChar isSpace: (SpecialChar t) asciiValue).
    self assert: (SpecialChar isSpace: (SpecialChar n) asciiValue).
    self assert: (SpecialChar isSpace: (SpecialChar v) asciiValue).
    self assert: (SpecialChar isSpace: (SpecialChar f) asciiValue).
    self assert: (SpecialChar isSpace: (SpecialChar r) asciiValue).

    self assertFalse: (SpecialChar isSpace: $a asciiValue).
    self assertFalse: (SpecialChar isSpace: $A asciiValue).
    self assertFalse: (SpecialChar isSpace: $0 asciiValue).
    self assertFalse: (SpecialChar isSpace: $. asciiValue).

    "Created: / 02-04-2015 / 18:21:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

