"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#TokenBuilder
	instanceVariableNames:'token state idx buf flags astBuilder'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!TokenBuilder class methodsFor:'initialization'!

initializeWithASTBuilder: anASTBuilder state: aState
    "Create new token with a reference to the parser state"
    ^ self new astBuilder: anASTBuilder; token: (Token new); state: aState; yourself

    "Created: / 05-10-2015 / 00:29:45 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!TokenBuilder class methodsFor:'queries'!

MBCLEN_CHARFOUND_P                            
    ^ true

    "Created: / 09-04-2015 / 15:44:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 13-04-2015 / 12:23:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!TokenBuilder methodsFor:'accessing'!

astBuilder
    ^ astBuilder
!

astBuilder:something
    astBuilder := something.
!

buf
    ^ buf
!

buf:something
    buf := something.
!

flags
    ^ flags

    "Modified: / 27-04-2015 / 16:04:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

flags:something
    flags := something.

    "Modified: / 27-04-2015 / 16:04:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

idx
    ^ idx
!

idx:something
    idx := something.
!

last
    (idx > 0) ifTrue: [
        ^ buf last
    ] ifFalse: [
        ^ 0
    ].

    "Created: / 18-10-2015 / 12:24:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

state
    ^ state
!

state:something
    state := something.
!

token
    ^ token
!

token:something
    token := something.
! !

!TokenBuilder methodsFor:'accessing - behavior'!

add: c
    "Add char code to the token"
    self addChar: (Character value: c).

    "Created: / 09-04-2015 / 15:50:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 09-04-2015 / 23:00:16 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addChar: c
    "Add char object to the token"
    idx := idx + 1.
    buf := buf , c.

    "Created: / 09-04-2015 / 22:59:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addMbc: char enc: enc
    |len|
    len := state codelen: char enc: enc.
    self space: len.
    self mbcPut: char enc: enc.

    "Created: / 10-04-2015 / 21:11:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addMbchar: code
    |len|
    len := self parserPreciseMbcLen.
    (self class MBCLEN_CHARFOUND_P) ifFalse: [
        state compileError: ('invalid multibyte char (' , (state enc name) , ')').
        ^ -1
    ].

    self add: code.
    len := len - 1.
    state pos: ((state pos) + len).

    "TODO:"
    (len > 0) ifTrue: [
        self shouldImplement.
        "grammar.cpp:9726
         static int
         parser_tokadd_mbchar(rb_parser_state *parser_state, int c)"
    ].
    ^ code

    "Created: / 09-04-2015 / 13:53:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 05-10-2015 / 00:32:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 14-10-2015 / 18:00:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addUTF8: parserState stringLiteral: stringLiteral symbolLiteral: symbolLiteral regexpLiteral: regexpLiteral
    "If string_literal is true, then we allow multiple codepoints
    in \u{}, and add the codepoints to the current token.
    Otherwise we're parsing a character literal and return a single
    codepoint without adding it"

    |codePoint condition encoding|
    (regexpLiteral = nil) ifTrue: [
        self addChar: $\;
             addChar: $u.
    ].

    (parserState peekChar: ${) ifTrue: [
        "do while"
        condition := true.
        [ condition ] whileTrue: [
            (regexpLiteral ~= nil) ifTrue: [ self add: (parserState atRelative: 0) ].
            parserState nextc.
            codePoint := parserState scanHex: 6.
            (parserState scanLength = 0) ifTrue: [
                parserState yyError: 'invalid Unicode escape'.
                ^ 0
            ].
            (codePoint > 16r10ffff) ifTrue: [
                parserState yyError: 'invalid Unicode codepoint (too large)'.
                ^ 0
            ].

            parserState pos: ((parserState pos) + (parserState scanLength)).
            (regexpLiteral ~= nil) ifTrue: [
                self copy: (parserState scanLength).
            ] ifFalse: [ (codePoint >= 16r80) ifTrue: [
                encoding := state UTF8.
                (stringLiteral ~= nil) ifTrue: [
                    self addMbc: codePoint enc: encoding.
                ].
            ] ifFalse: [ (stringLiteral ~= nil) ifTrue: [
                self add: (Character value: codePoint).
            ].].].

            "while condition"
            condition := (stringLiteral ~= nil) and: [(parserState peekChar: (SpecialChar space)) or: [parserState peekChar: (SpecialChar t)]].
        ].

        (state peekChar: $}) ifFalse: [
            parserState yyError: 'unterminated Unicode escape'.
            ^ 0
        ].

        (regexpLiteral ~= nil) ifTrue: [
            self add: $}.
        ].
        parserState nextc.
    ] ifFalse: [
        "handle \uxxxx form"
        codePoint := parserState scanHex: 4.
        (parserState scanLength < 4) ifTrue: [
            parserState yyError: 'invalid Unicode escape'. 
            ^ 0
        ].

        parserState pos: ((parserState pos) + 4).
        (regexpLiteral ~= nil) ifTrue: [
            self copy: 4.
        ] ifFalse: [ (codePoint >= 16r80) ifTrue: [
            encoding := parserState UTF8.
            parserState enc name: encoding.
            (stringLiteral ~= nil) ifTrue: [
                self addMbc: codePoint enc: encoding.
            ].
        ] ifFalse: [ (stringLiteral ~= nil) ifTrue: [
            self add: codePoint.
        ].].].
    ].

    ^ codePoint

    "Created: / 09-04-2015 / 22:54:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 13-04-2015 / 12:25:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

copy: length
    self space: length.    
    buf := buf , (state lastNChars: length).

    "Created: / 10-04-2015 / 19:39:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

fix
    "do nothing - compatibility reason only"

    "Created: / 13-04-2015 / 11:18:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

intern

    "TODO:"
    DebugTrace warning: 'TokenBuilder intern not implemented'.

    "Created: / 25-04-2015 / 16:44:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 04-10-2015 / 23:32:42 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

mbcPut:arg1 enc:arg2
    "do nothing"

    "Created: / 10-04-2015 / 21:15:06 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

newToken
    "TODO: "

    self idx: 0.
    self buf: ''.

    "Created: / 19-09-2015 / 19:59:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-09-2015 / 20:44:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parserPreciseMbcLen
    ^ 1

    "Created: / 09-04-2015 / 13:54:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 05-10-2015 / 00:23:46 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

scanLength: length
    "empty method - only for compatibility reason"

    "Created: / 10-04-2015 / 19:40:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

space:length
    idx := idx + length.

    "Created: / 10-04-2015 / 20:00:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!TokenBuilder methodsFor:'build node'!

integerLiteral: aValue suffix: aSuffix
    "create an integer or rational number node"
    |type|
    type := TokenType tINTEGER.

    ((aSuffix bitAnd: (InputReader NUM_SUFFIX_R)) ~= 0) ifTrue: [
        "rb_intern call"
        "v = rb_funcall(rb_cObject, rb_intern(""Rational""), 1, v);"
        self shouldImplement.
        type := TokenType tRATIONAL.
    ].

    ^ self numberLiteral: aValue type: type suffix: aSuffix

    "Created: / 21-09-2015 / 14:43:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

numberLiteral: aValue type: aType suffix: aSuffix
    "create a number literal (float, rational, integer or imaginary)"
    |type|
    type := aType.
    ((aSuffix bitAnd: (InputReader NUM_SUFFIX_I)) ~= 0) ifTrue: [
        "v = rb_funcall(rb_cObject, rb_intern(""Complex""), 2, INT2FIX(0), v);"
        self shouldImplement.
        type := TokenType tIMAGINARY.
    ].

    "TODO: positions"

    "switch type"
    (type = (TokenType tFLOAT)) ifTrue: [
        ^ Lexem initializeWithTokenType: type node: (self astBuilder NEW_FLOAT: aValue positions: nil)
    ] ifFalse: [ (type = (TokenType tINTEGER)) ifTrue: [
        ^ Lexem initializeWithTokenType: type node: (self astBuilder NEW_NUMBER: aValue positions: nil)
    ] ifFalse: [ (type = (TokenType tRATIONAL)) ifTrue: [
        ^ Lexem initializeWithTokenType: type node: (self astBuilder NEW_RATIONAL: aValue positions: nil)
    ] ifFalse: [ (type = (TokenType tIMAGINARY)) ifTrue: [
        ^ Lexem initializeWithTokenType: type node: (self astBuilder NEW_IMAGINARY: aValue positions: nil)
    ] ifFalse: [ "default"
        ^ self integerLiteral: aValue suffix: aSuffix
    ] ] ] ].

    "Created: / 21-09-2015 / 14:52:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 22-09-2015 / 11:43:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

