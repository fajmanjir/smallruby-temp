"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#VTable
	instanceVariableNames:'prev tbl'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!VTable class methodsFor:'instance creation'!

initializeWithPrev: aPrev
    "return an initialized instance"

    ^ self basicNew prev: aPrev; initialize; yourself.

    "Created: / 07-02-2016 / 15:04:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

new
    "return an initialized instance"

    ^ self basicNew initialize.

    "Created: / 07-02-2016 / 15:02:43 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!VTable methodsFor:'accessing'!

prev
    ^ prev
!

prev:something
    prev := something.
!

tbl
    ^ tbl
!

tbl:something
    tbl := something.
! !

!VTable methodsFor:'behavior'!

size
    (tbl isNotNil) ifTrue: [
        ^ tbl size
    ] ifFalse: [
        ^ 0
    ].

    "Created: / 08-02-2016 / 23:20:45 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!VTable methodsFor:'initialization'!

initialize
    self tbl: (OrderedCollection new).

    "Created: / 07-02-2016 / 15:05:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

