'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ NameSpace: Ruby }"

TestCase subclass:#ReservedKeywordsTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST - Test'
!

!ReservedKeywordsTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!ReservedKeywordsTest methodsFor:'tests'!

testKeywords

    |keywords kw|

    keywords := ReservedKeywords initialize.

    kw := keywords find: 'begin'.
    self assert: (kw id1 = (TokenType keyword_begin)).
    self assert: (kw id2 = (TokenType keyword_begin)).
    self assert: (kw state = (StateExpr EXPR_BEG)).

    kw := keywords find: 'defined?'.
    self assert: (kw id1 = (TokenType keyword_defined)).
    self assert: (kw id2 = (TokenType keyword_defined)).
    self assert: (kw state = (StateExpr EXPR_ARG)).

    kw := keywords find: 'while'.
    self assert: (kw id1 = (TokenType keyword_while)).
    self assert: (kw id2 = (TokenType modifier_while)).
    self assert: (kw state = (StateExpr EXPR_VALUE)).

    "not a keyword"
    kw := keywords find: 'abc'.
    self assert: (kw isNil).

    "too short for a keyword"
    kw := keywords find: 'a'.
    self assert: (kw isNil).

    "too long for a keyword"
    kw := keywords find: 'meanwhileintheforest'.
    self assert: (kw isNil).

    "Created: / 18-10-2015 / 17:35:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

