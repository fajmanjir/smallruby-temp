"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ NameSpace: Ruby }"

TestCase subclass:#TokenTypeTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST - Test'
!

!TokenTypeTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!TokenTypeTest methodsFor:'tests'!

test1
    "TokenType class methods should not raise errors"

    self shouldnt: [ TokenType tAMPER] raise:Error.
    self shouldnt: [ TokenType fromChar: $0] raise:Error.
    self shouldnt: [ TokenType fromChar: $a] raise:Error.

    "
     self run:#test1
     self new test1
    "

    "Modified: / 19-09-2015 / 15:28:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testType
    "Token type should set and read the same value"
    |astBuilder number block|
    astBuilder := ASTBuilder new.
    number := astBuilder NEW_NUMBER: 123 positions: #numberPositions.
    self assert: (number type = (NodeType NODE_NUMBER)).
    self assert: (number u1 = 123).
    "self assert: (number positions = #numberPositions)."

    block := astBuilder NEW_BLOCK: #block positions: #blockPositions.
    self assert: (block type = (NodeType NODE_BLOCK)).

    "Created: / 21-09-2015 / 16:07:38 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

