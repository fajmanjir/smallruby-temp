'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:25'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

TestCase subclass:#EncodingTest
	instanceVariableNames:'inputReader'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser - Test'
!

!EncodingTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!EncodingTest methodsFor:'accessing'!

inputReader
    ^ inputReader
!

inputReader:something
    inputReader := something.
! !

!EncodingTest methodsFor:'initialize / release'!

setUp
    "common setup - invoked before testing."
    |in|
    in := '..abc'.
    inputReader := InputReader initializeWithString: in.

    "Modified: / 03-04-2015 / 11:31:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tearDown
    "common cleanup - invoked after testing."

    super tearDown

    "Modified (format): / 28-03-2015 / 15:24:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!EncodingTest methodsFor:'tests'!

testIsAlpha
    |enc|
    enc := Encoding new.

    self assertFalse: (enc isAlpha: (TokenType fromChar: $0)).
    self assertFalse: (enc isAlpha: (TokenType fromChar: $9)).

    self assert: (enc isAlpha: (TokenType fromChar: $a)).
    self assert: (enc isAlpha: (TokenType fromChar: $A)).
    self assert: (enc isAlpha: (TokenType fromChar: $f)).
    self assert: (enc isAlpha: (TokenType fromChar: $F)).
    self assert: (enc isAlpha: (TokenType fromChar: $g)).
    self assert: (enc isAlpha: (TokenType fromChar: $G)).
    self assertFalse: (enc isAlpha: (TokenType fromChar: $-)).
    self assertFalse: (enc isAlpha: (TokenType fromChar: $+)).
    self assertFalse: (enc isAlpha: (TokenType fromChar: $_)).

    "Created: / 14-04-2015 / 16:52:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testIsDigit
    |enc|
    enc := Encoding new.

    self assert: (enc isDigit: (TokenType fromChar: $0)).
    self assert: (enc isDigit: (TokenType fromChar: $9)).

    self assertFalse: (enc isDigit: (TokenType fromChar: $a)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $A)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $f)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $F)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $g)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $G)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $-)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $+)).
    self assertFalse: (enc isDigit: (TokenType fromChar: $-)).

    "Created: / 13-04-2015 / 22:25:43 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testIsXDigit
    |enc|
    enc := Encoding new.

    self assert: (enc isXDigit: (TokenType fromChar: $0)).
    self assert: (enc isXDigit: (TokenType fromChar: $9)).
    self assert: (enc isXDigit: (TokenType fromChar: $a)).
    self assert: (enc isXDigit: (TokenType fromChar: $A)).
    self assert: (enc isXDigit: (TokenType fromChar: $f)).
    self assert: (enc isXDigit: (TokenType fromChar: $F)).

    self assertFalse: (enc isXDigit: (TokenType fromChar: $g)).
    self assertFalse: (enc isXDigit: (TokenType fromChar: $G)).
    self assertFalse: (enc isXDigit: (TokenType fromChar: $-)).
    self assertFalse: (enc isXDigit: (TokenType fromChar: $+)).
    self assertFalse: (enc isXDigit: (TokenType fromChar: $-)).

    "Created: / 13-04-2015 / 22:23:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

