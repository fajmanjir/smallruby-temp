'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#OperatorPriority
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Grammar'
!

!OperatorPriority class methodsFor:'enum'!

t_42
    "*"
    ^  20

    "Created: / 21-10-2015 / 18:23:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

t_43
    "+"
    ^  10

    "Created: / 21-10-2015 / 18:22:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!OperatorPriority class methodsFor:'find a constant'!

find: anOpCode
    ^ self perform: ((String streamContents: [ :stream |
            stream nextPutAll: 't_';
                   nextPutAll: anOpCode asString
        ]) asSymbol)

    "Created: / 21-10-2015 / 18:32:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

