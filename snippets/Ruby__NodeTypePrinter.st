"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ NameSpace: Ruby }"

Object subclass:#NodeTypePrinter
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Helpers'
!

!NodeTypePrinter class methodsFor:'enum NodeType'!

node_0
    ^ 'NODE_SCOPE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_1
    ^ 'NODE_BLOCK'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_10
    ^ 'NODE_BREAK'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_100
    ^ 'NODE_IFUNC'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_101
    ^ 'NODE_DSYM'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_102
    ^ 'NODE_ATTRASGN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_103
    ^ 'NODE_PRELUDE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_104
    ^ 'NODE_LAMBDA'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_105
    ^ 'NODE_OPTBLOCK'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_106
    ^ 'NODE_LAST'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_107
    ^ 'NODE_FILE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_108
    ^ 'NODE_REGEX'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_109
    ^ 'NODE_NUMBER'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_11
    ^ 'NODE_NEXT'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_110
    ^ 'NODE_FLOAT'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_111
    ^ 'NODE_ENCODING'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_112
    ^ 'NODE_PREEXE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_113
    ^ 'NODE_RATIONAL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_114
    ^ 'NODE_IMAGINARY'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_12
    ^ 'NODE_REDO'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_13
    ^ 'NODE_RETRY'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_14
    ^ 'NODE_BEGIN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_15
    ^ 'NODE_RESCUE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_16
    ^ 'NODE_RESBODY'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_17
    ^ 'NODE_ENSURE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_18
    ^ 'NODE_AND'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_19
    ^ 'NODE_OR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_2
    ^ 'NODE_IF'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_20
    ^ 'NODE_MASGN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_21
    ^ 'NODE_LASGN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_22
    ^ 'NODE_DASGN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_23
    ^ 'NODE_DASGN_CURR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_24
    ^ 'NODE_GASGN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_25
    ^ 'NODE_IASGN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_26
    ^ 'NODE_IASGN2'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_27
    ^ 'NODE_CDECL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_28
    ^ 'NODE_CVASGN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_29
    ^ 'NODE_CVDECL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_3
    ^ 'NODE_CASE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_30
    ^ 'NODE_OP_ASGN1'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_31
    ^ 'NODE_OP_ASGN2'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_32
    ^ 'NODE_OP_ASGN_AND'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_33
    ^ 'NODE_OP_ASGN_OR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_34
    ^ 'NODE_OP_CDECL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_35
    ^ 'NODE_CALL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_36
    ^ 'NODE_FCALL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_37
    ^ 'NODE_VCALL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_38
    ^ 'NODE_SUPER'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_39
    ^ 'NODE_ZSUPER'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_4
    ^ 'NODE_WHEN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_40
    ^ 'NODE_ARRAY'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_41
    ^ 'NODE_ZARRAY'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_42
    ^ 'NODE_VALUES'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_43
    ^ 'NODE_HASH'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_44
    ^ 'NODE_RETURN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_45
    ^ 'NODE_YIELD'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_46
    ^ 'NODE_LVAR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_47
    ^ 'NODE_DVAR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_48
    ^ 'NODE_GVAR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_49
    ^ 'NODE_IVAR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_5
    ^ 'NODE_OPT_N'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_50
    ^ 'NODE_CONST'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_51
    ^ 'NODE_CVAR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_52
    ^ 'NODE_NTH_REF'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_53
    ^ 'NODE_BACK_REF'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_54
    ^ 'NODE_MATCH'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_55
    ^ 'NODE_MATCH2'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_56
    ^ 'NODE_MATCH3'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_57
    ^ 'NODE_LIT'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_58
    ^ 'NODE_STR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_59
    ^ 'NODE_DSTR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_6
    ^ 'NODE_WHILE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_60
    ^ 'NODE_XSTR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_61
    ^ 'NODE_DXSTR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_62
    ^ 'NODE_EVSTR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_63
    ^ 'NODE_DREGX'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_64
    ^ 'NODE_DREGX_ONCE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_65
    ^ 'NODE_ARGS'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_66
    ^ 'NODE_ARGS_AUX'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_67
    ^ 'NODE_OPT_ARG'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_68
    ^ 'NODE_KW_ARG'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_69
    ^ 'NODE_POSTARG'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_7
    ^ 'NODE_UNTIL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_70
    ^ 'NODE_ARGSCAT'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_71
    ^ 'NODE_ARGSPUSH'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_72
    ^ 'NODE_SPLAT'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_73
    ^ 'NODE_TO_ARY'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_74
    ^ 'NODE_BLOCK_ARG'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_75
    ^ 'NODE_BLOCK_PASS'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_76
    ^ 'NODE_DEFN'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_77
    ^ 'NODE_DEFS'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_78
    ^ 'NODE_ALIAS'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_79
    ^ 'NODE_VALIAS'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_8
    ^ 'NODE_ITER'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_80
    ^ 'NODE_UNDEF'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_81
    ^ 'NODE_CLASS'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_82
    ^ 'NODE_MODULE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_83
    ^ 'NODE_SCLASS'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_84
    ^ 'NODE_COLON2'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_85
    ^ 'NODE_COLON3'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_86
    ^ 'NODE_DOT2'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_87
    ^ 'NODE_DOT3'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_88
    ^ 'NODE_FLIP2'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_89
    ^ 'NODE_FLIP3'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_9
    ^ 'NODE_FOR'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_90
    ^ 'NODE_SELF'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_91
    ^ 'NODE_NIL'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_92
    ^ 'NODE_TRUE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_93
    ^ 'NODE_FALSE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_94
    ^ 'NODE_ERRINFO'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_95
    ^ 'NODE_DEFINED'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_96
    ^ 'NODE_POSTEXE'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_97
    ^ 'NODE_ALLOCA'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_98
    ^ 'NODE_BMETHOD'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_99
    ^ 'NODE_MEMO'

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!NodeTypePrinter class methodsFor:'printing'!

find: id
    "find node type name by number"
    |nodeTypeName|

    (id isMemberOf: Character) ifTrue: [
        DebugTrace warning: (String streamContents: [:stream |
        stream
            nextPutAll: 'NodeTypePrinter: find: Character type ($';
            nextPutAll: id asString;
            nextPutAll: ') given instead of integer'
        ]).
        ^ String streamContents: [:stream |
            stream
                nextPutAll: '$';
                nextPutAll: id asString
            ].               
    ].

    [
        nodeTypeName := self perform: (('node_' , id asString ) asSymbol).
    ] on: MessageNotUnderstood do: [
        self error: (id asString , ' is not a valid NodeType.').
    ].

    ^ nodeTypeName

    "Created: / 21-10-2015 / 21:12:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

formatNode: aNode subnode: aSubnode globals: aGlobals indent: anIndent
    "node to string"
    (aNode isKindOf: Node) ifFalse: [
        ((aNode isNil) or: [(aNode = true) or: [aNode = false]]) ifTrue: [
            ^ aNode printString
        ] ifFalse: [
            ^ (':', aNode printString)
        ].

    ].

    (aNode ~= aSubnode) ifTrue: [      
        ((aSubnode class = SmallInteger) and: [(aSubnode > ((1 << (IdType ID_SCOPE_SHIFT)) + (TokenType tLAST_TOKEN))) and: [aGlobals isNotNil]]) ifTrue: [
            ^ ('"', (aGlobals findStrById: (aSubnode)), '"')
        ] ifFalse: [
            ^ self print: aSubnode indent: anIndent globals: aGlobals
        ].
    ] ifFalse: [
        ^ 'nil'.
    ].

    "Created: / 12-02-2016 / 23:00:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-02-2016 / 18:42:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

print: aNode globals: aGlobals
    "node to string"
    ^ self print: aNode indent: 0 globals: aGlobals

    "Created: / 12-02-2016 / 22:57:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

print: aNode indent: anIndent
    "node to string"
    ^ self print: aNode indent: anIndent globals: nil

    "Created: / 17-01-2016 / 17:00:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 12-02-2016 / 22:57:13 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

print: aNode indent: anIndent globals: aGlobals
    "node to string"
    |indent newLine itemSeparator indentString|
    (aNode isKindOf: Node) ifFalse: [
        ^ aNode printString
    ].

    indent := anIndent + 1.


    "newLine := String with: (Character cr)."
    newLine := ''.
    itemSeparator := ','.
    indentString := ''.

    ^ (String streamContents:[:stream |
        "anIndent timesRepeat: [ stream nextPutAll: '  '].    "
        stream nextPutAll: ' [';
               nextPutAll: ':';
               nextPutAll: (self find: (aNode type));
               nextPutAll: ', ';
               nextPutAll: newLine.

        indent timesRepeat: [ stream nextPutAll: indentString ].
        stream nextPutAll: (self formatNode: aNode subnode: (aNode u1) globals: aGlobals indent: indent). 
        stream nextPutAll: itemSeparator.

        indent timesRepeat: [ stream nextPutAll: indentString ].
        stream nextPutAll: (self formatNode: aNode subnode: (aNode u2) globals: aGlobals indent: indent). 
        stream nextPutAll: itemSeparator.

        indent timesRepeat: [ stream nextPutAll: indentString ].
        stream nextPutAll: (self formatNode: aNode subnode: (aNode u3) globals: aGlobals indent: indent). 
        stream nextPutAll: newLine.

        anIndent timesRepeat: [ stream nextPutAll: indentString. ].
        stream nextPutAll: ']'.
    ])

    "Created: / 12-02-2016 / 22:56:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 14-02-2016 / 23:01:11 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

