"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#ParserState
	instanceVariableNames:'references pos eofp sourceline heredoc_end lineCount currentLine
		lastLine nextlineBeg input strTerm state commandStart lval
		mainSwitchDollarNumberBlock lex_gets_pos compileForEval tokline
		inDef inSingle inDefined'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!ParserState class methodsFor:'initialization'!

initializeWithString: str
    "create instance using given string"
    |inst|
    inst := self new.
    inst initialize.
    inst input: str.
    ^ inst

    "Created: / 28-03-2015 / 15:20:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'* uncategorized *'!

parseString: quote
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 28-03-2015 / 22:02:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'accessing'!

commandStart
    ^ commandStart
!

commandStart:something
    commandStart := something.
!

compileForEval
    ^ compileForEval
!

compileForEval:something
    compileForEval := something.
!

currentLine
    ^ currentLine
!

currentLine:something 
    currentLine := something.
!

eofp
    ^ eofp
!

eofp:something
    eofp := something.
!

heredoc_end
    ^ heredoc_end
!

heredoc_end:something
    heredoc_end := something.
!

inDef
    ^ inDef
!

inDef:something
    inDef := something.
!

inDefined
    ^ inDefined
!

inDefined:something
    inDefined := something.
!

inSingle
    ^ inSingle
!

inSingle:something
    inSingle := something.
!

input
    ^ input
!

input:something
    input := something.
!

lastLine
    ^ lastLine
!

lastLine:something 
    lastLine := something.
!

lex_gets_pos
    ^ lex_gets_pos
!

lex_gets_pos:something
    lex_gets_pos := something.
!

lineCount
    ^ lineCount
!

lineCount:something 
    lineCount := something.
!

lval
    ^ lval
!

lval:something
    lval := something.
!

mainSwitchDollarNumberBlock
    ^ mainSwitchDollarNumberBlock
!

mainSwitchDollarNumberBlock:something
    mainSwitchDollarNumberBlock := something.
!

nextlineBeg
    ^ nextlineBeg
!

nextlineBeg:something
    nextlineBeg := something.
!

pos
    ^ pos
!

pos:something 
    pos := something.
!

references
    ^ references
!

references:something
    references := something.
!

sourceline
    ^ sourceline
!

sourceline:something
    sourceline := something.
!

state
    ^ state
!

state:something
    state := something.
!

strTerm
    ^ strTerm
!

strTerm:something
    strTerm := something.
!

tokline
    ^ tokline
!

tokline:something
    tokline := something.
! !

!ParserState methodsFor:'accessing behaviour'!

at:offset 
    "get char code at specified position"
    
    ^ (input at: offset) asciiValue

    "Created: / 28-03-2015 / 14:53:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 01-04-2015 / 22:02:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

atRelative:offset 
    "get char code at specified offset from current position"
    
    ^ (input at: (pos + offset)) asciiValue

    "Created: / 03-04-2015 / 12:21:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

charAt:offset 
    "get char object at specified position"
    
    ^ (input at: offset)

    "Created: / 03-04-2015 / 12:02:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

charAtRelative:offset 
    "get char object at specified offset from current position"
    
    ^ (input at: (pos + offset))

    "Created: / 03-04-2015 / 12:23:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

getline
    "get line by invoking gets block"
    
    |line|

    line := self lex_gets.
    (line isNil) ifTrue:[
        ^ line
    ].
    self must_be_ascii_compatible:line.
    ^ line

    "Created: / 27-03-2015 / 16:53:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 30-01-2016 / 17:38:06 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

lex_gets
    |beg end pend pEnc|
        pEnc := self must_be_ascii_compatible: input.

        beg := 1.
        (lex_gets_pos notNil) ifTrue: [
            ((input length + 1) = lex_gets_pos) ifTrue: [
                ^ nil
            ].
            beg := lex_gets_pos.
        ].
        pend := input length + 1.
        end := beg.
        [
            [ end < pend ] whileTrue: [
                ((self charAt: end) = (SpecialChar n)) ifTrue: [
                    end := end + 1.
                    Break break.
                ].
                end := end + 1.
            ].
        ] on: Break do: [].

        lex_gets_pos := end.

        ^ Position initializeWithBeg: beg end: end

    "Created: / 30-01-2016 / 16:55:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 06-02-2016 / 18:35:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

lex_goto_eol
    "go to end of line"
    pos := currentLine end.

    "Created: / 27-03-2015 / 16:17:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 06-02-2016 / 16:52:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

must_be_ascii_compatible: line
    "TODO: not implemented"

    "Created: / 27-03-2015 / 17:06:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

peekChar: c
    "verify that character object at current position is c"

    "DEBUG"
    (c isMemberOf: Character) ifFalse: [
        self error: 'InputReader.peekChar: not given a Character argument'.
    ].
    "END DEBUG"

    ^ (pos < (currentLine end)) and: [c = (self charAt: pos)]

    "Created: / 09-04-2015 / 23:09:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 19-10-2015 / 23:24:17 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

peekChar: c atRelative: n
    "verify that character object at current position is c"

    "DEBUG"
    (c isMemberOf: Character) ifFalse: [
        self error: 'InputReader.peekChar: not given a Character argument'.
    ].
    "END DEBUG" 

    ^ ((pos + n) < (currentLine end)) and: [c = (self charAtRelative: n)]

    "Created: / 18-10-2015 / 12:34:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-10-2015 / 11:55:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

peekCharCode: c
    "verify that character code at current position is c"
    ^ (pos < (currentLine end)) and: [c = (self at: pos)]

    "Created: / 18-10-2015 / 12:38:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

peekCharCode: c atRelative: n
    "verify that character code at current position is c"
    ^ ((pos + n) < (currentLine end)) and: [c = (self atRelative: n)]

    "Created: / 18-10-2015 / 12:39:03 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-10-2015 / 12:04:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

pushback:arg
    "undo character read unless c == -1"
    (arg = -1) ifFalse: [
        pos := pos - 1.
    ].

    "Created: / 01-04-2015 / 22:03:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

stateP: flags
    "perform bitwise and with state instance variable"
    ^ (state bitAnd: flags) ~= 0

    "Created: / 01-04-2015 / 21:30:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 06-04-2015 / 15:10:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

stateP: flags state: aState
    "perform bitwise and with state instance variable"
    ^ (aState bitAnd: flags) ~= 0

    "Created: / 02-04-2015 / 19:11:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 06-04-2015 / 15:10:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'enum'!

EXPR_ARG
    ^ 16

    "Created: / 22-09-2015 / 15:29:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ARG_ANY
    ^ (self EXPR_ARG) bitOr: (self EXPR_CMDARG)

    "Created: / 22-09-2015 / 15:39:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_BEG
    ^ 1

    "Created: / 01-04-2015 / 21:36:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 22-09-2015 / 15:28:16 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_BEG_ANY
    ^ ((self EXPR_BEG) bitOr: (self EXPR_VALUE)) bitOr: ((self EXPR_MID) bitOr: (self EXPR_CLASS))

    "Created: / 22-09-2015 / 15:37:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_CLASS
    ^ 512

    "Created: / 01-04-2015 / 21:36:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 22-09-2015 / 15:32:11 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_CMDARG
    ^ 32

    "Created: / 22-09-2015 / 15:30:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_DOT
    ^ 256

    "Created: / 01-04-2015 / 21:36:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 22-09-2015 / 15:31:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_END
    ^ 2

    "Created: / 28-03-2015 / 21:55:38 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 22-09-2015 / 15:28:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ENDARG
    ^ 4

    "Created: / 22-09-2015 / 15:28:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ENDFN
    ^ 8

    "Created: / 22-09-2015 / 15:29:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_END_ANY
    ^ ((self EXPR_END) bitOr: (self EXPR_ENDARG)) bitOr: (self EXPR_ENDFN)

    "Created: / 22-09-2015 / 15:39:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_FNAME
    ^ 128

    "Created: / 01-04-2015 / 21:36:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 22-09-2015 / 15:31:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_MID
    ^ 64

    "Created: / 22-09-2015 / 15:30:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_VALUE
    ^ 1024

    "Created: / 01-04-2015 / 21:36:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 22-09-2015 / 15:32:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'initialization'!

initialize
    pos := 1.
    currentLine := Position new.
    lval := Lval new.
    "TODO: #12 initialize"

    "Created: / 23-03-2015 / 21:33:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 02-04-2015 / 17:22:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'reading'!

hereDocument: node
    "comment stating purpose of this message"

    self error: 'hereDocument not implemented'.

    "Created: / 28-03-2015 / 21:47:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

