"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#IdType
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!IdType class methodsFor:'enum'!

ID_ATTRSET
    ^ 16r04

    "Created: / 18-10-2015 / 19:56:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_CLASS
    ^ 16r06

    "Created: / 18-10-2015 / 19:57:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_CONST
    ^ 16r05

    "Created: / 18-10-2015 / 19:57:03 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_GLOBAL
    ^ 16r03

    "Created: / 18-10-2015 / 19:56:42 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_INSTANCE
    ^ 16r01

    "Created: / 18-10-2015 / 19:56:31 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_INTERNAL
    ^ self ID_JUNK

    "Created: / 18-10-2015 / 19:57:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_JUNK
    ^ 16r07

    "Created: / 18-10-2015 / 19:57:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_LOCAL
    ^ 16r00

    "Created: / 18-10-2015 / 19:56:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_SCOPE_MASK
    ^ 16r0f

    "Created: / 18-10-2015 / 19:56:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ID_SCOPE_SHIFT
    ^ 7

    "Created: / 18-10-2015 / 19:53:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

SYMBOL_FLAG
    ^ 16rE

    "Created: / 09-02-2016 / 23:34:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!IdType class methodsFor:'queries'!

ID2SYM: anId
    ^ ((anId >> (IdType ID_SCOPE_SHIFT)) << 8) | (IdType SYMBOL_FLAG)

    "Created: / 09-02-2016 / 23:27:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isAsgnOrId: anId
    |res|
    res := anId bitAnd: (self ID_SCOPE_MASK).
    ^ (self isNotopId: anId) and: [( res = (self ID_GLOBAL) ) or: [ res = (self ID_INSTANCE) ] or: [ res = (self ID_CLASS) ]]

    "Created: / 18-10-2015 / 20:05:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 19-10-2015 / 11:36:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isAttrsetId: anId
    ^ (self isNotopId: anId) and: [ (anId bitAnd: (self ID_SCOPE_MASK)) = (self ID_ATTRSET) ]

    "Created: / 18-10-2015 / 20:04:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isClassId: anId
    ^ (self isNotopId: anId) and: [ (anId bitAnd: (self ID_SCOPE_MASK)) = (self ID_CLASS) ]

    "Created: / 18-10-2015 / 20:04:56 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isConstId: anId
    ^ (self isNotopId: anId) and: [ (anId bitAnd: (self ID_SCOPE_MASK)) = (self ID_CONST) ]

    "Created: / 18-10-2015 / 20:04:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isGlobalId: anId
    ^ (self isNotopId: anId) and: [ (anId bitAnd: (self ID_SCOPE_MASK)) = (self ID_GLOBAL) ]

    "Created: / 18-10-2015 / 20:04:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isInstanceId: anId
    ^ (self isNotopId: anId) and: [ (anId bitAnd: (self ID_SCOPE_MASK)) = (self ID_INSTANCE) ]

    "Created: / 18-10-2015 / 20:04:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isJunkId: anId
    ^ (self isNotopId: anId) and: [ (anId bitAnd: (self ID_SCOPE_MASK)) = (self ID_JUNK) ]

    "Created: / 18-10-2015 / 20:05:13 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isLocalId: anId
    ^ (self isNotopId: anId) and: [ (anId bitAnd: (self ID_SCOPE_MASK)) = (self ID_LOCAL) ]

    "Created: / 18-10-2015 / 19:59:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isNotopId: anId
    ^ anId > (TokenType tLAST_TOKEN)

    "Created: / 18-10-2015 / 19:59:03 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

