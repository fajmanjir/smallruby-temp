'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#Keyword
	instanceVariableNames:'id1 id2 state'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!Keyword class methodsFor:'initialization'!

initializeWithId: anId1 id2: anId2 state: aState
    ^ self new id1: anId1; id2: anId2; state: aState; yourself

    "Created: / 18-10-2015 / 16:50:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Keyword methodsFor:'accessing'!

id1
    ^ id1
!

id1:something
    id1 := something.
!

id2
    ^ id2
!

id2:something
    id2 := something.
!

state
    ^ state
!

state:something
    state := something.
! !

