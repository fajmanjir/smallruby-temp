"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:22'                !

"{ NameSpace: Ruby }"

Error subclass:#Return
	instanceVariableNames:'val'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Commands'
!

!Return class methodsFor:'initialize'!

return: aValue
    "raise given return instance"

    self new val: aValue; signal.

    "Created: / 22-09-2015 / 11:28:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Return methodsFor:'accessing'!

node
    ^ node
!

node:something
    node := something.
!

val
    ^ val
!

val:something
    val := something.
! !

