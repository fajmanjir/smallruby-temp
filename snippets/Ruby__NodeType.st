'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#NodeType
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!NodeType class methodsFor:'enum'!

NODE_ALIAS
    ^ 78

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ALLOCA
    ^ 97

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_AND
    ^ 18

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ARGS
    ^ 65

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ARGSCAT
    ^ 70

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ARGSPUSH
    ^ 71

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ARGS_AUX
    ^ 66

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ARRAY
    ^ 40

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ATTRASGN
    ^ 102

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_BACK_REF
    ^ 53

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_BEGIN
    ^ 14

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_BLOCK
    ^ 1

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_BLOCK_ARG
    ^ 74

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_BLOCK_PASS
    ^ 75

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_BMETHOD
    ^ 98

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_BREAK
    ^ 10

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CALL
    ^ 35

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CASE
    ^ 3

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CDECL
    ^ 27

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CLASS
    ^ 81

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_COLON2
    ^ 84

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_COLON3
    ^ 85

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CONST
    ^ 50

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CVAR
    ^ 51

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CVASGN
    ^ 28

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_CVDECL
    ^ 29

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DASGN
    ^ 22

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DASGN_CURR
    ^ 23

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DEFINED
    ^ 95

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DEFN
    ^ 76

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DEFS
    ^ 77

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DOT2
    ^ 86

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DOT3
    ^ 87

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DREGX
    ^ 63

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DREGX_ONCE
    ^ 64

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DSTR
    ^ 59

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DSYM
    ^ 101

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DVAR
    ^ 47

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_DXSTR
    ^ 61

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ENCODING
    ^ 111

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ENSURE
    ^ 17

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ERRINFO
    ^ 94

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_EVSTR
    ^ 62

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FALSE
    ^ 93

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FCALL
    ^ 36

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FILE
    ^ 107

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FLIP2
    ^ 88

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FLIP3
    ^ 89

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FLOAT
    ^ 110

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_FOR
    ^ 9

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_GASGN
    ^ 24

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_GVAR
    ^ 48

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_HASH
    ^ 43

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_HEREDOC
    ^ self NODE_ARRAY

    "Created: / 28-03-2015 / 21:40:31 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_IASGN
    ^ 25

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_IASGN2
    ^ 26

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_IF
    ^ 2

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_IFUNC
    ^ 100

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_IMAGINARY
    ^ 114

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ITER
    ^ 8

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_IVAR
    ^ 49

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_KW_ARG
    ^ 68

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_LAMBDA
    ^ 104

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_LASGN
    ^ 21

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_LAST
    ^ 106

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_LIT
    ^ 57

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_LVAR
    ^ 46

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_MASGN
    ^ 20

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_MATCH
    ^ 54

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_MATCH2
    ^ 55

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_MATCH3
    ^ 56

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_MEMO
    ^ 99

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_MODULE
    ^ 82

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_NEXT
    ^ 11

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_NIL
    ^ 91

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_NTH_REF
    ^ 52

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_NUMBER
    ^ 109

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OPTBLOCK
    ^ 105

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OPT_ARG
    ^ 67

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OPT_N
    ^ 5

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OP_ASGN1
    ^ 30

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OP_ASGN2
    ^ 31

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OP_ASGN_AND
    ^ 32

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OP_ASGN_OR
    ^ 33

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OP_CDECL
    ^ 34

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_OR
    ^ 19

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_POSTARG
    ^ 69

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_POSTEXE
    ^ 96

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_PREEXE
    ^ 112

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_PRELUDE
    ^ 103

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_RATIONAL
    ^ 113

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_REDO
    ^ 12

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_REGEX
    ^ 108

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_RESBODY
    ^ 16

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_RESCUE
    ^ 15

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_RETRY
    ^ 13

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_RETURN
    ^ 44

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_SCLASS
    ^ 83

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_SCOPE
    ^ 0

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_SELF
    ^ 90

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_SPLAT
    ^ 72

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_STR
    ^ 58

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_STRTERM
    "nothing to gc"
    ^ self NODE_ZARRAY

    "Created: / 03-04-2015 / 15:51:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_SUPER
    ^ 38

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_TO_ARY
    ^ 73

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_TRUE
    ^ 92

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_UNDEF
    ^ 80

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_UNTIL
    ^ 7

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_VALIAS
    ^ 79

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_VALUES
    ^ 42

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_VCALL
    ^ 37

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_WHEN
    ^ 4

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_WHILE
    ^ 6

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_XSTR
    ^ 60

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_YIELD
    ^ 45

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ZARRAY
    ^ 41

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NODE_ZSUPER
    ^ 39

    "Created: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 19:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

