'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#StringType
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!StringType class methodsFor:'enum'!

STR_FUNC_ESCAPE
    ^ 16r01

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

STR_FUNC_EXPAND
    ^ 16r02

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

STR_FUNC_INDENT
    ^ 16r20

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

STR_FUNC_QWORDS
    ^ 16r08

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

STR_FUNC_REGEXP
    ^ 16r04

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

STR_FUNC_SYMBOL
    ^ 16r10

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

dquote
    ^ self STR_FUNC_EXPAND

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

dsym
    ^ (self STR_FUNC_SYMBOL) bitOr: (self STR_FUNC_EXPAND)

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

dword
    ^ (self STR_FUNC_QWORDS) bitOr: (self STR_FUNC_EXPAND)

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

regexp
    ^ ((self STR_FUNC_REGEXP) bitOr: (self STR_FUNC_ESCAPE)) bitOr: (self STR_FUNC_EXPAND)

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

squote
    ^ 0

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ssym
    ^ self STR_FUNC_SYMBOL

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

sword
    ^ self STR_FUNC_QWORDS

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

xquote
    ^ self STR_FUNC_EXPAND

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

