"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:15'                !

"{ NameSpace: Ruby }"

Object subclass:#RubySpecialConsts
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!RubySpecialConsts class methodsFor:'enum'!

Qfalse
    ^ 0

    "Created: / 10-02-2016 / 22:00:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

Qnil
    ^ 16r08

    "Created: / 10-02-2016 / 22:00:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

Qtrue
    ^ 16r14

    "Created: / 10-02-2016 / 22:00:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

Qundef
    ^ 16r34

    "Created: / 10-02-2016 / 22:00:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

