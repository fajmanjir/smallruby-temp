"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ NameSpace: Ruby }"

Object subclass:#GlobalSymbols
	instanceVariableNames:'symbols keywords'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!GlobalSymbols class methodsFor:'initialization'!

initializeWithKeywords: aKW
    ^  self basicNew keywords: aKW; initialize; yourself

    "Created: / 12-02-2016 / 12:52:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!GlobalSymbols methodsFor:'accessing'!

keywords
    ^ keywords
!

keywords:something
    keywords := something.
!

symbols
    ^ symbols
!

symbols:something
    symbols := something.
! !

!GlobalSymbols methodsFor:'accessing - behavior'!

newStrId: aName
    "Add a new string unless already exists"
    |id kw|

    kw := keywords find: aName.
    (kw isNotNil) ifTrue: [
        ^ kw id1
    ].

    id := symbols indexOf: aName.
    (id ~= 0) ifTrue: [
        ^ id + (TokenType tLAST_TOKEN)
    ].

    symbols add: aName.
    ^ symbols size + (TokenType tLAST_TOKEN)

    "Created: / 12-02-2016 / 12:19:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 12-02-2016 / 22:40:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!GlobalSymbols methodsFor:'initialization'!

initialize       
    symbols := OrderedCollection new.

    "Created: / 11-02-2016 / 15:46:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 11-02-2016 / 17:29:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!GlobalSymbols methodsFor:'queries'!

findIdByStr: aStr

    "Created: / 12-02-2016 / 22:41:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

findStrById: anId
    |res|
    (anId < (TokenType tLAST_TOKEN)) ifTrue: [
        ^ LexemPrinter findTokenType: anId
    ].

    [
        res := symbols at: ((anId >> (IdType ID_SCOPE_SHIFT)) - (TokenType tLAST_TOKEN)).
    ] on: SubscriptOutOfBoundsError do:[
        ^ anId asString
    ].

    ^ res

    "Created: / 12-02-2016 / 12:39:31 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 13-02-2016 / 00:02:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

lastId
    ^ symbols size

    "Created: / 11-02-2016 / 17:33:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

size
    ^ symbols size

    "Created: / 12-02-2016 / 12:38:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

