"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:07'                !

"{ NameSpace: Ruby }"

Object subclass:#LexemPrinter
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Helpers'
!

!LexemPrinter class methodsFor:'* uncategorized *'!

formatLexem:arg
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 19-10-2015 / 15:46:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!LexemPrinter class methodsFor:'enum TokenType'!

token_258
    ^ 'keyword_class'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_259
    ^ 'keyword_module'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_260
    ^ 'keyword_def'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_261
    ^ 'keyword_undef'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_262
    ^ 'keyword_begin'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_263
    ^ 'keyword_rescue'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_264
    ^ 'keyword_ensure'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_265
    ^ 'keyword_end'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_266
    ^ 'keyword_if'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_267
    ^ 'keyword_unless'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_268
    ^ 'keyword_then'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_269
    ^ 'keyword_elsif'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_270
    ^ 'keyword_else'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_271
    ^ 'keyword_case'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_272
    ^ 'keyword_when'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_273
    ^ 'keyword_while'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_274
    ^ 'keyword_until'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_275
    ^ 'keyword_for'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_276
    ^ 'keyword_break'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_277
    ^ 'keyword_next'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_278
    ^ 'keyword_redo'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_279
    ^ 'keyword_retry'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_280
    ^ 'keyword_in'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_281
    ^ 'keyword_do'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_282
    ^ 'keyword_do_cond'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_283
    ^ 'keyword_do_block'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_284
    ^ 'keyword_do_LAMBDA'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_285
    ^ 'keyword_return'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_286
    ^ 'keyword_yield'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_287
    ^ 'keyword_super'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_288
    ^ 'keyword_self'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_289
    ^ 'keyword_nil'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_290
    ^ 'keyword_true'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_291
    ^ 'keyword_false'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_292
    ^ 'keyword_and'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_293
    ^ 'keyword_or'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_294
    ^ 'keyword_not'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_295
    ^ 'modifier_if'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_296
    ^ 'modifier_unless'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_297
    ^ 'modifier_while'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_298
    ^ 'modifier_until'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_299
    ^ 'modifier_rescue'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_300
    ^ 'keyword_alias'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_301
    ^ 'keyword_defined'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_302
    ^ 'keyword_BEGIN'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_303
    ^ 'keyword_END'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_304
    ^ 'keyword__LINE__'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_305
    ^ 'keyword__FILE__'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_306
    ^ 'keyword__ENCODING__'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_307
    ^ 'tIDENTIFIER'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_308
    ^ 'tFID'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_309
    ^ 'tGVAR'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_310
    ^ 'tIVAR'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_311
    ^ 'tCONSTANT'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_312
    ^ 'tCVAR'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_313
    ^ 'tLABEL'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_314
    ^ 'tINTEGER'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_315
    ^ 'tFLOAT'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_316
    ^ 'tRATIONAL'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_317
    ^ 'tIMAGINARY'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_318
    ^ 'tSTRING_CONTENT'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_319
    ^ 'tCHAR'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_320
    ^ 'tNTH_REF'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_321
    ^ 'tBACK_REF'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_322
    ^ 'tREGEXP_END'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_323
    ^ 'tUPLUS'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_324
    ^ 'tUMINUS'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_325
    ^ 'tPOW'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_326
    ^ 'tCMP'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_327
    ^ 'tEQ'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_328
    ^ 'tEQQ'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_329
    ^ 'tNEQ'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_330
    ^ 'tGEQ'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_331
    ^ 'tLEQ'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_332
    ^ 'tANDOP'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_333
    ^ 'tOROP'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_334
    ^ 'tMATCH'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_335
    ^ 'tNMATCH'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_336
    ^ 'tDOT2'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_337
    ^ 'tDOT3'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_338
    ^ 'tAREF'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_339
    ^ 'tASET'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_340
    ^ 'tLSHFT'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_341
    ^ 'tRSHFT'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_342
    ^ 'tCOLON2'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_343
    ^ 'tCOLON3'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_344
    ^ 'tOP_ASGN'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_345
    ^ 'tASSOC'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_346
    ^ 'tLPAREN'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_347
    ^ 'tLPAREN_ARG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_348
    ^ 'tRPAREN'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_349
    ^ 'tLBRACK'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_350
    ^ 'tLBRACE'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_351
    ^ 'tLBRACE_ARG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_352
    ^ 'tSTAR'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_353
    ^ 'tDSTAR'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_354
    ^ 'tAMPER'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_355
    ^ 'tLAMBDA'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_356
    ^ 'tSYMBEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_357
    ^ 'tSTRING_BEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_358
    ^ 'tXSTRING_BEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_359
    ^ 'tREGEXP_BEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_360
    ^ 'tWORDS_BEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_361
    ^ 'tQWORDS_BEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_362
    ^ 'tSYMBOLS_BEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_363
    ^ 'tQSYMBOLS_BEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_364
    ^ 'tSTRING_DBEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_365
    ^ 'tSTRING_DEND'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_366
    ^ 'tSTRING_DVAR'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_367
    ^ 'tSTRING_END'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_368
    ^ 'tLAMBEG'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_369
    ^ 'tLOWEST'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_370
    ^ 'tUMINUS_NUM'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
!

token_371
    ^ 'tLAST_TOKEN'

    "Created: / 19-10-2015 / 12:28:34 / hhyperion <fajmaji1@fit.cvut.cz>"
! !

!LexemPrinter class methodsFor:'enum TokenType details'!

token_details_307: aLexem inputReader: anInputReader
    "tIDENTIFIER"
    ^ String streamContents: [:stream |
        stream
            nextPutAll: (', ''');
            nextPutAll: (anInputReader tokenBuilder buf);
            nextPutAll: ('''')
    ]

    "Created: / 19-10-2015 / 16:49:17 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

token_details_308: aLexem inputReader: anInputReader
    "tFID"
    ^ String streamContents: [:stream |
        stream
            nextPutAll: (', ');
            nextPutAll: (anInputReader tokenBuilder buf)
    ]

    "Created: / 19-10-2015 / 23:18:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

token_details_311: aLexem inputReader: anInputReader
    "tCONSTANT"
    ^ String streamContents: [:stream |
        stream
            nextPutAll: (', ''');
            nextPutAll: (anInputReader tokenBuilder buf);
            nextPutAll: ('''')
    ]

    "Created: / 19-10-2015 / 17:16:42 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

token_details_314: aLexem inputReader: anInputReader
    "tINTEGER"
    ^ String streamContents: [:stream |
        stream
            nextPutAll: (', ');
            nextPutAll: (aLexem node u1 asString)
    ]

    "Created: / 19-10-2015 / 16:42:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

token_details_318: aLexem inputReader: anInputReader
    "tSTRING_CONTENT"
    ^ String streamContents: [:stream |
        stream
            nextPutAll: (', ''');
            nextPutAll: (aLexem content);
            nextPutAll: ('''')
    ]

    "Created: / 26-10-2015 / 20:13:58 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

token_details_344: aLexem inputReader: anInputReader
    "tOP_ASGN"
    ^ String streamContents: [:stream |
        stream
            nextPutAll: (', ');
            nextPutAll: (self findTokenType: (aLexem content))
    ]

    "Created: / 19-10-2015 / 15:59:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!LexemPrinter class methodsFor:'printing'!

findLexemDetails: aLexem inputReader: anInputReader
    "find lexem by tokenType and format as string"

    (aLexem tokenType = 0) ifTrue: [
        ^ ''
    ].

    (aLexem tokenType isMemberOf: Character) ifTrue: [
        ^ ''
    ].

    [
        ^ self perform: (('token_details_' , (aLexem tokenType asString) , ':inputReader:' ) asSymbol) with: aLexem with: anInputReader
    ] on: MessageNotUnderstood do: [
        ^ ''
    ].

    "Created: / 19-10-2015 / 15:59:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

findTokenType: id
    "find token type name by number"
    |tokenTypeName|

    (id = 0) ifTrue: [
        ^ '---END OF SCRIPT---'
    ].

    (id isMemberOf: Character) ifTrue: [
        DebugTrace warning: (String streamContents: [:stream |
        stream
            nextPutAll: 'LexemPrinter: findTokenType: Character type ($';
            nextPutAll: id asString;
            nextPutAll: ') given instead of integer'
        ]).
        ^ String streamContents: [:stream |
            stream
                nextPutAll: '$';
                nextPutAll: id asString
            ].               
    ].

    [
        tokenTypeName := self perform: (('token_' , id asString ) asSymbol).
    ] on: MessageNotUnderstood do: [
        [
            tokenTypeName := String streamContents: [:stream |
            stream
                nextPutAll: '$';
                nextPutAll: id "asCharacter asString"
            ].
        ] on: MessageNotUnderstood do: [
            tokenTypeName := String streamContents: [:stream |
            stream
                nextPutAll: '$';
                nextPutAll: id asCharacter asString
            ].
        ].
    ].

    ^ tokenTypeName

    "Created: / 19-10-2015 / 12:17:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 06-02-2016 / 16:22:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

printLexem: aLexem inputReader: anInputReader
    |tokenTypeName details|
    tokenTypeName := self findTokenType: aLexem tokenType.
    details := self findLexemDetails: aLexem inputReader: anInputReader.

    ^ String streamContents: [:stream |
        stream
            nextPutAll: tokenTypeName;
            nextPutAll: ' ';
            nextPutAll: details
        ].

    "Created: / 19-10-2015 / 16:31:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

