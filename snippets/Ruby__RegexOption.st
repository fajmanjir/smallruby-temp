'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ NameSpace: Ruby }"

Object subclass:#RegexOption
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!RegexOption class methodsFor:'enum'!

RE_KCODE_EUC
    ^ 1024

    "Created: / 25-10-2015 / 19:30:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_KCODE_NONE
    ^ 512

    "Created: / 25-10-2015 / 19:30:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_KCODE_SJIS
    ^ 1536

    "Created: / 25-10-2015 / 19:31:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_KCODE_UTF8
    ^ 2048

    "Created: / 25-10-2015 / 19:31:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_OPTION_CAPTURE_GROUP
    ^ 256

    "Created: / 25-10-2015 / 19:29:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_OPTION_DONT_CAPTURE_GROUP
    ^ 128

    "Created: / 25-10-2015 / 19:29:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_OPTION_EXTENDED
    ^ 2

    "Created: / 25-10-2015 / 19:35:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_OPTION_IGNORECASE
    ^ 1

    "Created: / 25-10-2015 / 19:33:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_OPTION_MULTILINE
    ^ 4

    "Created: / 25-10-2015 / 19:33:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

RE_OPTION_ONCE
    ^ 8192

    "Created: / 25-10-2015 / 19:31:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

