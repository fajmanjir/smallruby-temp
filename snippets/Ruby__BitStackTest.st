'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

TestCase subclass:#BitStackTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser - Test'
!

!BitStackTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!BitStackTest methodsFor:'initialize / release'!

tearDown
    "common cleanup - invoked after testing."

    super tearDown

    "Modified (format): / 28-03-2015 / 15:24:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!BitStackTest methodsFor:'tests'!

testLexPop
    |stack|
    stack := BitStack initialize.
    stack value: 40.

    self assert: (stack lexPop = 20).
    self assert: (stack lexPop = 10).
    self assert: (stack lexPop = 5).
    self assert: (stack lexPop = 3).
    self assert: (stack lexPop = 1).
    self assert: (stack lexPop = 1).
    self assert: (stack lexPop = 1).
    self assert: (stack lexPop = 1).

    "Created: / 21-04-2015 / 18:36:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testPop
    |stack|
    stack := BitStack initialize.
    stack value: 40.

    self assert: (stack pop = 20).
    self assert: (stack pop = 10).
    self assert: (stack pop = 5).
    self assert: (stack pop = 2).
    self assert: (stack pop = 1).
    self assert: (stack pop = 0).
    self assert: (stack pop = 0).
    self assert: (stack pop = 0).

    "Created: / 21-04-2015 / 18:50:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testPush
    |stack|
    stack := BitStack initialize.
    stack value: 1.

    self assert: ((stack push: 1) = 3).
    self assert: ((stack push: 0) = 6).
    self assert: ((stack push: 1) = 13).
    self assert: ((stack push: 0) = 26).
    self assert: ((stack push: 0) = 52).

    "Created: / 21-04-2015 / 18:46:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testTop
    |stack|
    stack := BitStack initialize.
    stack value: 40.
    self assert: (stack top = 0).

    stack value: 10.
    self assert: (stack top = 0).

    stack value: 5.
    self assert: (stack top = 1).

    stack value: 2.
    self assert: (stack top = 0).
    
    stack value: 1.
    self assert: (stack top = 1).

    stack value: 0.
    self assert: (stack top = 0).

    "Created: / 21-04-2015 / 18:57:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

