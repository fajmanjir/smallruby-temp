'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#StateExpr
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!StateExpr class methodsFor:'shortcuts'!

EXPR_ARG
    ^ 1 bitShift: (StateBit EXPR_ARG_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ARG_ANY
    ^ (self EXPR_ARG) bitOr: (self EXPR_CMDARG)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_BEG
    ^ 1 bitShift: (StateBit EXPR_BEG_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_BEG_ANY
    ^ ((self EXPR_BEG) bitOr: (self EXPR_VALUE)) bitOr: ((self EXPR_MID) bitOr: (self EXPR_CLASS))

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_CLASS
    ^ 1 bitShift: (StateBit EXPR_CLASS_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_CMDARG
    ^ 1 bitShift: (StateBit EXPR_CMDARG_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_DOT
    ^ 1 bitShift: (StateBit EXPR_DOT_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_END
    ^ 1 bitShift: (StateBit EXPR_END_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ENDARG
    ^ 1 bitShift: (StateBit EXPR_ENDARG_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ENDFN
    ^ 1 bitShift: (StateBit EXPR_ENDFN_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_END_ANY
    ^ ((self EXPR_END) bitOr: (self EXPR_ENDARG)) bitOr: (self EXPR_ENDFN)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_FNAME
    ^ 1 bitShift: (StateBit EXPR_FNAME_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_MID
    ^ 1 bitShift: (StateBit EXPR_MID_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_VALUE
    ^ 1 bitShift: (StateBit EXPR_VALUE_BIT)

    "Created: / 27-03-2015 / 12:07:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

