"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:22'                !

"{ NameSpace: Ruby }"

Object subclass:#Validator
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Validator'
!

!Validator class methodsFor:'instance creation'!

new
    ^ self basicNew initialize; yourself

    "Created: / 15-02-2016 / 16:37:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Validator class methodsFor:'validating'!

validateDir: aDir
    |result files successSum filepath completeFiles|
    successSum := 0.
    completeFiles := 0.
    ^ (String streamContents:[:stream |
        files := DirectoryCrawler findAllWithSuffix: '.rb' dir: aDir.
        files do: [ :file |         
            filepath := String streamContents:[:fp |
                fp nextPutAll: (aDir asString);
                   nextPutAll: '/';
                   nextPutAll: file.
            ].
            stream nextPutAll: 'file ';
                   nextPutAll: file;
                   nextPutAll: ': '.
            result := (self validateFile: (filepath asFilename) stream: stream) * 100.
            successSum := successSum + result.
            stream nextPutAll: (result asString);
                   nextPutAll: ' %';
                   nextPutAll: (String with: (Character cr)).    

            (result = 100) ifTrue: [
                completeFiles := completeFiles + 1.
            ].

        ].

        (files size = 0) ifTrue: [
            ^ ('No Ruby file found in path ', (aDir asString))
        ].

        stream nextPutAll: (String with: (Character cr)); 
               nextPutAll: 'RESULT: ';
               nextPutAll: (((successSum / (files size)) asFixedPoint: 2) asString);
               nextPutAll: ' %';
               nextPutAll: (String with: (Character cr));
               nextPutAll: 'complete files: ';
               nextPutAll: (completeFiles asString);
               nextPutAll: ' / ';
               nextPutAll: (files size asString);
               nextPutAll: (String with: (Character cr)).
    ])

    "Created: / 15-02-2016 / 16:43:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-02-2016 / 21:51:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

validateFile: aFile
    Transcript warn: 'validateFile: should be overriden to work correctly'.

    "Created: / 15-02-2016 / 16:45:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-02-2016 / 23:25:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

validateFile: aFile stream: aStream
    Transcript warn: 'validateFile:stream: should be overriden to work correctly'.

    "Created: / 15-02-2016 / 23:24:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Validator methodsFor:'initialization'!

initialize

    "Created: / 15-02-2016 / 16:37:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

