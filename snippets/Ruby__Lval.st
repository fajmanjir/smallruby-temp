'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#Lval
	instanceVariableNames:'val node id num vars'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Lval methodsFor:'accessing'!

id
    ^ id
!

id:something
    id := something.
!

node
    ^ node
!

node:something
    node := something.
!

num
    ^ num
!

num:something
    num := something.
!

val
    ^ val
!

val:something
    val := something.
!

vars
    ^ vars
!

vars:something
    vars := something.
! !

