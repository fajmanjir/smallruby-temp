'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:24'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Error subclass:#Goto
	instanceVariableNames:'block'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Commands'
!

!Goto class methodsFor:'initialize'!

block: block
    "go to block"

    self new initBlock: block; signal.

    "Created: / 28-03-2015 / 23:08:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Goto methodsFor:'accessing'!

block
    ^ block
!

initBlock: b
    "comment stating purpose of this message"
    block := b

    "Created: / 28-03-2015 / 23:12:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

