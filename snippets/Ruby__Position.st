'From Smalltalk/X, Version:6.2.5.1565 on 18-02-2016 at 02:33:23'                !

"{ Package: 'ctu:smallruby/parser' }"

"{ NameSpace: Ruby }"

Object subclass:#Position
	instanceVariableNames:'beg end'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Position class methodsFor:'initialization'!

initializeWithBeg: beg end: end
    "fill the Position"
    ^ (self new beg: beg; end: end)

    "Created: / 02-04-2015 / 11:56:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Position methodsFor:'accessing'!

beg
    ^ beg
!

beg:something
    beg := something.
!

end
    ^ end
!

end:something
    end := something.
! !

