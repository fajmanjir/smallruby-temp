"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:09'                !

"{ NameSpace: Smalltalk }"

TestCase subclass:#MathHelperTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST - Test'
!

!MathHelperTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!MathHelperTest methodsFor:'initialize / release'!

setUp
    "common setup - invoked before testing."

    super setUp
!

tearDown
    "common cleanup - invoked after testing."

    super tearDown
! !

!MathHelperTest methodsFor:'tests'!

testBitInvert4Byte
    self assert:(4294967287 = (MathHelper bitInvert4Byte:8)).
    self assert:(4294967064 = (MathHelper bitInvert4Byte:231)).

    "Created: / 22-03-2015 / 15:29:17 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

