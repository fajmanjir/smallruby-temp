"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:09'                !

"{ NameSpace: Smalltalk }"

Error subclass:#Break
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Break class methodsFor:'initialize'!

break
    "signal break"

    self new signal.

    "Created: / 03-04-2015 / 12:32:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

