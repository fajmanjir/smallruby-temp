"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:07'                !

"{ NameSpace: Smalltalk }"

Object subclass:#MathHelper
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!MathHelper class methodsFor:'bitwise'!

bitInvert4Byte: number
    "4 byte bitwise invertion"
    ^ (number | 16r100000000) bitInvert & 16r0FFFFFFFF

    "Created: / 22-03-2015 / 15:20:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

