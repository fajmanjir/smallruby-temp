"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#ParserState
	instanceVariableNames:'references pos eofp sourceline heredoc_end lineCount gets
		currentLine lastLine nextlineBeg input strTerm state commandStart
		lval mainSwitchDollarNumberBlock'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!ParserState class methodsFor:'initialization'!

initializeWithString: str
    "create instance using given string"
    |inst|
    inst := self new.
    inst initialize.
    inst input: str.
    ^ inst

    "Created: / 28-03-2015 / 15:20:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'* uncategorized *'!

parseString: quote
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 28-03-2015 / 22:02:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'accessing'!

commandStart
    ^ commandStart
!

commandStart:something
    commandStart := something.
!

currentLine
    ^ currentLine
!

currentLine:something 
    currentLine := something.
!

eofp
    ^ eofp
!

eofp:something
    eofp := something.
!

heredoc_end
    ^ heredoc_end
!

heredoc_end:something
    heredoc_end := something.
!

input
    ^ input
!

input:something
    input := something.
!

lastLine
    ^ lastLine
!

lastLine:something 
    lastLine := something.
!

lex_gets
    ^ gets value.

    "Created: / 27-03-2015 / 16:59:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

lex_gets:something 
    gets := something.
!

lineCount
    ^ lineCount
!

lineCount:something 
    lineCount := something.
!

lval
    ^ lval
!

lval:something
    lval := something.
!

mainSwitchDollarNumberBlock
    ^ mainSwitchDollarNumberBlock
!

mainSwitchDollarNumberBlock:something
    mainSwitchDollarNumberBlock := something.
!

nextlineBeg
    ^ nextlineBeg
!

nextlineBeg:something
    nextlineBeg := something.
!

pos
    ^ pos
!

pos:something 
    pos := something.
!

references
    ^ references
!

references:something
    references := something.
!

sourceline
    ^ sourceline
!

sourceline:something
    sourceline := something.
!

state
    ^ state
!

state:something
    state := something.
!

strTerm
    ^ strTerm
!

strTerm:something
    strTerm := something.
! !

!ParserState methodsFor:'accessing behaviour'!

at:offset 
    "get char code at specified position"
    
    ^ (input at: offset) asciiValue

    "Created: / 28-03-2015 / 14:53:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 01-04-2015 / 22:02:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

atRelative:offset 
    "get char code at specified offset from current position"
    
    ^ (input at: (pos + offset)) asciiValue

    "Created: / 03-04-2015 / 12:21:44 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

charAt:offset 
    "get char object at specified position"
    
    ^ (input at: offset)

    "Created: / 03-04-2015 / 12:02:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

charAtRelative:offset 
    "get char object at specified offset from current position"
    
    ^ (input at: (pos + offset))

    "Created: / 03-04-2015 / 12:23:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

getline
    "get line by invoking gets block"
    
    |line|

    line := self lex_gets.
    (line == nil) ifTrue:[
        ^ line
    ].
    self must_be_ascii_compatible:line.
    ^ line

    "Created: / 27-03-2015 / 16:53:07 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 28-03-2015 / 10:55:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

lex_goto_eol
    "go to end of line"
    pos := currentLine end.

    "Created: / 27-03-2015 / 16:17:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 28-03-2015 / 11:27:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

must_be_ascii_compatible: line
    "TODO: not implemented"

    "Created: / 27-03-2015 / 17:06:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

peekChar: c
    "verify that character object at current position is c"
    ^ (pos < (currentLine end)) and: [c = (self charAt: pos)]

    "Created: / 09-04-2015 / 23:09:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 10-04-2015 / 19:12:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

pushback:arg
    "undo character read unless c == -1"
    (arg = -1) ifFalse: [
        pos := pos - 1.
    ].

    "Created: / 01-04-2015 / 22:03:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

stateP: flags
    "perform bitwise and with state instance variable"
    ^ (state bitAnd: flags) ~= 0

    "Created: / 01-04-2015 / 21:30:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 06-04-2015 / 15:10:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

stateP: flags state: aState
    "perform bitwise and with state instance variable"
    ^ (aState bitAnd: flags) ~= 0

    "Created: / 02-04-2015 / 19:11:05 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 06-04-2015 / 15:10:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'enum'!

EXPR_BEG
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 01-04-2015 / 21:36:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_CLASS
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 01-04-2015 / 21:36:22 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_DOT
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 01-04-2015 / 21:36:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_END
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 28-03-2015 / 21:55:38 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_FNAME
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 01-04-2015 / 21:36:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_VALUE
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 01-04-2015 / 21:36:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'initialization'!

initialize
    pos := 1.
    currentLine := Position new.
    lval := Lval new.
    "TODO: #12 initialize"

    "Created: / 23-03-2015 / 21:33:53 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 02-04-2015 / 17:22:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ParserState methodsFor:'reading'!

hereDocument: node
    "comment stating purpose of this message"

    self error: 'hereDocument not implemented'.

    "Created: / 28-03-2015 / 21:47:04 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

