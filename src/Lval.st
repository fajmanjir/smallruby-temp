'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#Lval
	instanceVariableNames:'val node id num vars'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Lval methodsFor:'accessing'!

id
    ^ id
!

id:something
    id := something.
!

node
    ^ node
!

node:something
    node := something.
!

num
    ^ num
!

num:something
    num := something.
!

val
    ^ val
!

val:something
    val := something.
!

vars
    ^ vars
!

vars:something
    vars := something.
! !

