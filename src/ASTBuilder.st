"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#ASTBuilder
	instanceVariableNames:'parserState'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!ASTBuilder class methodsFor:'initialize'!

initializeWithString:state 
    |builder|

    builder := self new.
    builder parserState:state.
    ^ builder

    "Created: / 21-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 21-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ASTBuilder methodsFor:'creation'!

NEW_ALIAS: n o: o positions: positions
    ^ self NEW_NODE: (NodeType NODE_ALIAS) a0: n a1: o a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ARGS: m o: o positions: positions
    ^ self NEW_NODE: (NodeType NODE_ARGS) a0: o a1: m a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ARGSCAT: a b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_ARGSCAT) a0: a a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ARGSPUSH: a b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_ARGSPUSH) a0: a a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ARGS_AUX: r b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_ARGS_AUX) a0: r a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ARRAY: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_ARRAY) a0: a a1: 1 a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ATTRASGN: r m: m a: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_ATTRASGN) a0: r a1: m a2: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_BACK_REF: n positions: positions
    ^ self NEW_NODE: (NodeType NODE_BACK_REF) a0: nil a1: n a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_BEGIN: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_BEGIN) a0: nil a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_BLOCK: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_BLOCK) a0: a a1: nil a2: nil positions: positions positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_BLOCK_ARG: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_BLOCK_ARG) a0: v a1: nil a2: (self local_cnt: v) positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_BLOCK_PASS: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_BLOCK_PASS) a0: nil a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_BMETHOD: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_BMETHOD) a0: nil a1: nil a2: b positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_BREAK: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_BREAK) a0: s a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CALL: r m: m a: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_CALL) a0: r a1: (RubyGrammar convert_op: m) a2: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CASE: h b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_CASE) a0: h a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CDECL: v val: val path: path positions: positions
    ^ self NEW_NODE: (NodeType NODE_CDECL) a0: v a1: val a2: path positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CLASS: n b: b s: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_CLASS) a0: n a1: (self NEW_SCOPE: nil b: b) a2: s positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_COLON2: c i: i positions: positions
    ^ self NEW_NODE: (NodeType NODE_COLON2) a0: c a1: i a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_COLON3: i positions: positions
    ^ self NEW_NODE: (NodeType NODE_COLON3) a0: nil a1: i a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CONST: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_CONST) a0: v a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CVAR: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_CVAR) a0: v a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CVASGN: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_CVASGN) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_CVDECL: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_CVDECL) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DASGN: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_DASGN) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DASGN_CURR: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_DASGN_CURR) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DEFINED: e positions: positions
    ^ self NEW_NODE: (NodeType NODE_DEFINED) a0: e a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DEFN: i a: a d: d p: p positions: positions
    ^ self NEW_NODE: (NodeType NODE_DEFN) a0: nil a1: i a2: (self NEW_SCOPE: a b: d) positions: positions.

    "Created: / 16-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DEFS: r i: i a: a d: d positions: positions
    ^ self NEW_NODE: (NodeType NODE_DEFS) a0: r a1: i a2: (self NEW_SCOPE: a b: d) positions: positions.

    "Created: / 16-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DOT2: b e: e positions: positions
    ^ self NEW_NODE: (NodeType NODE_DOT2) a0: b a1: e a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DOT3: b e: e positions: positions
    ^ self NEW_NODE: (NodeType NODE_DOT3) a0: b a1: e a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DSTR: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_DSTR) a0: (self REF: s) a1: 1 a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DSYM: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_DSYM) a0: (self REF: s) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DVAR: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_DVAR) a0: v a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_DXSTR: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_DXSTR) a0: (self REF: s) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ENCODING: n positions: positions
    ^ self NEW_NODE: (NodeType NODE_ENCODING) a0: (self REF: n) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ENSURE: b en: en positions: positions
    ^ self NEW_NODE: (NodeType NODE_ENSURE) a0: b a1: nil a2: en positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ERRINFO: positions
    ^ self NEW_NODE: (NodeType NODE_ERRINFO) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_EVSTR: n positions: positions
    ^ self NEW_NODE: (NodeType NODE_EVSTR) a0: nil a1: n a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_FALSE: positions
    ^ self NEW_NODE: (NodeType NODE_FALSE) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_FCALL: m a: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_FCALL) a0: nil a1: (RubyGrammar convert_op: m) a2: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_FILE: positions
    ^ self NEW_NODE: (NodeType NODE_FILE) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_FLOAT: l positions: positions
    ^ self NEW_NODE: (NodeType NODE_FLOAT) a0: (self REF: l) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_FOR: v i: i b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_FOR) a0: v a1: b a2: i positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_GASGN: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_GASGN) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_GVAR: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_GVAR) a0: v a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_HASH: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_HASH) a0: a a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_IASGN2: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_IASGN2) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_IASGN: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_IASGN) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_IF: c t: t e: e positions: positions
    ^ self NEW_NODE: (NodeType NODE_IF) a0: c a1: t a2: e positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_IFUNC: f c: c positions: positions
    ^ self NEW_NODE: (NodeType NODE_IFUNC) a0: f a1: c a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_IMAGINARY: l positions: positions
    ^ self NEW_NODE: (NodeType NODE_IMAGINARY) a0: (self REF: l) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ITER: a b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_ITER) a0: nil a1: (self NEW_SCOPE: a b: b) a2: nil positions: positions.

    "Created: / 16-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_IVAR: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_IVAR) a0: v a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_KW_ARG: i v: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_KW_ARG) a0: i a1: v a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_LAMBDA: a b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_ITER) a0: nil a1: (self NEW_SCOPE: a b: b) a2: nil positions: positions.

    "Created: / 16-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_LASGN: v val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_LASGN) a0: v a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_LIST: a positions: positions
    ^ self NEW_ARRAY: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_LIT: l positions: positions
    ^ self NEW_NODE: (NodeType NODE_LIT) a0: l a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_LVAR: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_LVAR) a0: v a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_MASGN: l r: r positions: positions
    ^ self NEW_NODE: (NodeType NODE_MASGN) a0: l a1: nil a2: r positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_MATCH2: n1 n2: n2 positions: positions
    ^ self NEW_NODE: (NodeType NODE_MATCH2) a0: n1 a1: n2 a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_MATCH3: r n2: n2 positions: positions
    ^ self NEW_NODE: (NodeType NODE_MATCH3) a0: r a1: n2 a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_MATCH: c positions: positions
    ^ self NEW_NODE: (NodeType NODE_MATCH) a0: c a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_MODULE: n b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_MODULE) a0: n a1: (self NEW_SCOPE: nil b: b) a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_NEXT: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_NEXT) a0: s a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_NIL: positions
    ^ self NEW_NODE: (NodeType NODE_NIL) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_NTH_REF: n positions: positions
    ^ self NEW_NODE: (NodeType NODE_NTH_REF) a0: nil a1: n a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_NUMBER: l positions: positions
    ^ self NEW_NODE: (NodeType NODE_NUMBER) a0: (self REF: l) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OPTBLOCK: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_OPTBLOCK) a0: a a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OPT_ARG: i v: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_OPT_ARG) a0: i a1: v a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OPT_N: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_OPT_N) a0: nil a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OP_ASGN1: p id: id a: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_OP_ASGN1) a0: p a1: id a2: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OP_ASGN22: i o: o positions: positions
    ^ self NEW_NODE: (NodeType NODE_OP_ASGN2) a0: i a1: o a2: (self rb_id_attrset: i) positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OP_ASGN2: r i: i o: o val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_OP_ASGN2) a0: r a1: val a2: (self NEW_OP_ASGN22: i o: o) positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OP_ASGN_AND: i val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_OP_ASGN_AND) a0: i a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OP_ASGN_OR: i val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_OP_ASGN_OR) a0: i a1: val a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_OP_CDECL: v op: op val: val positions: positions
    ^ self NEW_NODE: (NodeType NODE_OP_CDECL) a0: v a1: val a2: op positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_POSTARG: i v: v positions: positions
    ^ self NEW_NODE: (NodeType NODE_POSTARG) a0: i a1: v a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_POSTEXE: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_POSTEXE) a0: nil a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_PREEXE: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_PREEXE) a0: nil a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_PRELUDE: p b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_PRELUDE) a0: p a1: b a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_RATIONAL: l positions: positions
    ^ self NEW_NODE: (NodeType NODE_RATIONAL) a0: (self REF: l) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_REDO: positions
    ^ self NEW_NODE: (NodeType NODE_REDO) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_REGEX: l o: o positions: positions
    ^ self NEW_NODE: (NodeType NODE_REGEX) a0: (self REF: l) a1: nil a2: o positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_RESBODY: a ex: ex n: n positions: positions
    ^ self NEW_NODE: (NodeType NODE_RESBODY) a0: n a1: ex a2: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_RESCUE: b res: res e: e positions: positions
    ^ self NEW_NODE: (NodeType NODE_RESCUE) a0: b a1: res a2: e positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_RETRY: positions
    ^ self NEW_NODE: (NodeType NODE_RETRY) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_RETURN: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_RETURN) a0: s a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_SCLASS: r b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_SCLASS) a0: r a1: (self NEW_SCOPE: nil b: b) a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_SCOPE: a b: b positions: positions
    ^ self NEW_NODE: (NodeType NODE_SCOPE) a0: (self local_tbl) a1: b a2: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_SELF: positions
    ^ self NEW_NODE: (NodeType NODE_SELF) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_SPLAT: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_SPLAT) a0: a a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_STR: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_STR) a0: (self REF: s) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_STRTERM: func term: term paren: paren positions: positions
    ^ self NEW_NODE: (NodeType NODE_STRTERM) a0: func a1: ((term) bitOr: (paren bitShift: ((Limits CHAR_BIT) * 2))) a2: nil positions: positions.

    "Created: / 03-04-2015 / 16:38:57 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_SUPER: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_SUPER) a0: nil a1: nil a2: a positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_TO_ARY: a positions: positions
    ^ self NEW_NODE: (NodeType NODE_TO_ARY) a0: a a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_TRUE: positions
    ^ self NEW_NODE: (NodeType NODE_TRUE) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_UNDEF: i positions: positions
    ^ self NEW_NODE: (NodeType NODE_UNDEF) a0: nil a1: i a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_UNLESS: c t: t e: e positions: positions
    ^ self NEW_IF: c t: e e: t positions: positions positions: positions.

    "Created: / 16-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 16-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_UNTIL: c b: b n: n positions: positions
    ^ self NEW_NODE: (NodeType NODE_UNTIL) a0: c a1: b a2: n positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_VALIAS: n o: o positions: positions
    ^ self NEW_NODE: (NodeType NODE_VALIAS) a0: n a1: o a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_VCALL: m positions: positions
    ^ self NEW_NODE: (NodeType NODE_VCALL) a0: nil a1: (RubyGrammar convert_op: m) a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_WHEN: c t: t e: e positions: positions
    ^ self NEW_NODE: (NodeType NODE_WHEN) a0: c a1: t a2: e positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_WHILE: c b: b n: n positions: positions
    ^ self NEW_NODE: (NodeType NODE_WHILE) a0: c a1: b a2: n positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_XSTR: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_XSTR) a0: (self REF: s) a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_YIELD: a s: s positions: positions
    ^ self NEW_NODE: (NodeType NODE_YIELD) a0: a a1: nil a2: s positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ZARRAY: positions
    ^ self NEW_NODE: (NodeType NODE_ZARRAY) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

NEW_ZSUPER: positions
    ^ self NEW_NODE: (NodeType NODE_ZSUPER) a0: nil a1: nil a2: nil positions: positions.

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!ASTBuilder methodsFor:'helper'!

REF: s
    "hold reference to object in memory"
    ^ self node_add_reference: s

    "Created: / 20-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 13-04-2015 / 12:14:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

local_cnt: id
    "TODO: "
    ^ 0

    "Created: / 20-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

local_tbl
    "TODO: not implemented"
    ^ nil

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

node_add_reference: obj
    "hold reference to object in memory"
    ^ self parser_add_reference: (self parser_state) obj: obj

    "Created: / 20-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parser_add_reference: parser_state obj: obj
    "hold reference to object in memory"
    ^ (parser_state references) add: obj

    "Created: / 20-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 20-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

rb_id_attrset: id
    "TODO: not implemented"
    ^ nil

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

