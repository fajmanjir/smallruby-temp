"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:09'                !

"{ NameSpace: Smalltalk }"

Object subclass:#TokenBuilder
	instanceVariableNames:'token state idx buf'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!TokenBuilder class methodsFor:'initialization'!

initializeWithState: state
    "Create new token with a reference to the parser state"
    ^ self new state: state; token: (Token new)

    "Created: / 09-04-2015 / 13:59:43 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!TokenBuilder class methodsFor:'queries'!

MBCLEN_CHARFOUND_P                            
    ^ true

    "Created: / 09-04-2015 / 15:44:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 13-04-2015 / 12:23:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!TokenBuilder methodsFor:'* uncategorized *'!

parserPreciseMbcLen
    "raise an error: this method should be implemented (TODO)"

    ^ self shouldImplement

    "Created: / 09-04-2015 / 13:54:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!TokenBuilder methodsFor:'accessing'!

buf
    ^ buf
!

buf:something
    buf := something.
!

flags
    ^ flags
!

flags:something
    flags := something.
!

idx
    ^ idx
!

idx:something
    idx := something.
!

state
    ^ state
!

state:something
    state := something.
!

token
    ^ token
!

token:something
    token := something.
! !

!TokenBuilder methodsFor:'accessing - behavior'!

add: c
    "Add char code to the token"
    self addChar: (Character value: c).

    "Created: / 09-04-2015 / 15:50:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 09-04-2015 / 23:00:16 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addChar: c
    "Add char object to the token"
    idx := idx + 1.
    buf := buf , c.

    "Created: / 09-04-2015 / 22:59:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addMbc: char enc: enc
    |len|
    len := state codelen: char enc: enc.
    self space: len.
    self mbcPut: char enc: enc.

    "Created: / 10-04-2015 / 21:11:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addMbchar: code
    |len|
    len := self parserPreciseMbcLen.
    (self class MBCLEN_CHARFOUND_P) ifFalse: [
        state compileError: ('invalid multibyte char (' , (state enc name) , ')').
        ^ -1
    ].

    self add: code.
    len := len - 1.
    state pos: ((state pos) + len).

    "TODO:"

    "Created: / 09-04-2015 / 13:53:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 09-04-2015 / 15:57:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 25-04-2015 / 15:47:58 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

addUTF8: parserState stringLiteral: stringLiteral symbolLiteral: symbolLiteral regexpLiteral: regexpLiteral
    "If string_literal is true, then we allow multiple codepoints
    in \u{}, and add the codepoints to the current token.
    Otherwise we're parsing a character literal and return a single
    codepoint without adding it"

    |codePoint condition encoding|
    (regexpLiteral = nil) ifTrue: [
        self addChar: $\;
             addChar: $u.
    ].

    (parserState peekChar: ${) ifTrue: [
        "do while"
        condition := true.
        [ condition ] whileTrue: [
            (regexpLiteral ~= nil) ifTrue: [ self add: (parserState atRelative: 0) ].
            parserState nextc.
            codePoint := parserState scanHex: 6.
            (parserState scanLength = 0) ifTrue: [
                parserState yyError: 'invalid Unicode escape'.
                ^ 0
            ].
            (codePoint > 16r10ffff) ifTrue: [
                parserState yyError: 'invalid Unicode codepoint (too large)'.
                ^ 0
            ].

            parserState pos: ((parserState pos) + (parserState scanLength)).
            (regexpLiteral ~= nil) ifTrue: [
                self copy: (parserState scanLength).
            ] ifFalse: [ (codePoint >= 16r80) ifTrue: [
                encoding := state UTF8.
                (stringLiteral ~= nil) ifTrue: [
                    self addMbc: codePoint enc: encoding.
                ].
            ] ifFalse: [ (stringLiteral ~= nil) ifTrue: [
                self add: (Character value: codePoint).
            ].].].

            "while condition"
            condition := (stringLiteral ~= nil) and: [(parserState peekChar: (SpecialChar space)) or: [parserState peekChar: (SpecialChar t)]].
        ].

        (state peekChar: $}) ifFalse: [
            parserState yyError: 'unterminated Unicode escape'.
            ^ 0
        ].

        (regexpLiteral ~= nil) ifTrue: [
            self add: $}.
        ].
        parserState nextc.
    ] ifFalse: [
        "handle \uxxxx form"
        codePoint := parserState scanHex: 4.
        (parserState scanLength < 4) ifTrue: [
            parserState yyError: 'invalid Unicode escape'. 
            ^ 0
        ].

        parserState pos: ((parserState pos) + 4).
        (regexpLiteral ~= nil) ifTrue: [
            self copy: 4.
        ] ifFalse: [ (codePoint >= 16r80) ifTrue: [
            encoding := parserState UTF8.
            parserState enc name: encoding.
            (stringLiteral ~= nil) ifTrue: [
                self addMbc: codePoint enc: encoding.
            ].
        ] ifFalse: [ (stringLiteral ~= nil) ifTrue: [
            self add: codePoint.
        ].].].
    ].

    ^ codePoint

    "Created: / 09-04-2015 / 22:54:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 13-04-2015 / 12:25:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

copy: length
    self space: length.    
    buf := buf , (state lastNChars: length).

    "Created: / 10-04-2015 / 19:39:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

fix
    "do nothing - compatibility reason only"

    "Created: / 13-04-2015 / 11:18:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

intern


    "TODO:"

    self error: 'NotImplemented: TokenBuilder intern'.

    "Created: / 25-04-2015 / 16:44:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

mbcPut:arg1 enc:arg2
    "do nothing"

    "Created: / 10-04-2015 / 21:15:06 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

scanLength: length
    "empty method - only for compatibility reason"

    "Created: / 10-04-2015 / 19:40:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

space:length
    idx := idx + length.

    "Created: / 10-04-2015 / 20:00:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

