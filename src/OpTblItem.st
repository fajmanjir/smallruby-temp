'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:07'                !

"{ NameSpace: Smalltalk }"

Object subclass:#OpTblItem
	instanceVariableNames:'token name'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!OpTblItem class methodsFor:'initialize'!

initializeWithToken: token name: name
    "create a pair token:name"
    |item|
    item := self new.
    item token: token;
    	   name: name.
    ^ item

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!OpTblItem methodsFor:'accessing'!

name
    ^ name
!

name:something
    name := something.
!

token
    ^ token
!

token:something
    token := something.
! !

