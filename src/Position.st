"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#Position
	instanceVariableNames:'beg end'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Position class methodsFor:'initialization'!

initializeWithBeg: beg end: end
    "fill the Position"
    ^ (self new beg: beg; end: end)

    "Created: / 02-04-2015 / 11:56:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Position methodsFor:'accessing'!

beg
    ^ beg
!

beg:something
    beg := something.
!

end
    ^ end
!

end:something
    end := something.
! !

