'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#StateBit
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!StateBit class methodsFor:'enum'!

EXPR_ARG_BIT
    "newline significant, +/- is an operator. "
    ^ 4

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_BEG_BIT
    "ignore newline, +/- is a sign. "
    ^ 0

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_CLASS_BIT
    "immediate after `class', no here document. "
    ^ 9

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_CMDARG_BIT
    "newline significant, +/- is an operator. "
    ^ 5

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_DOT_BIT
    "right after `.' or `::', no reserved words. "
    ^ 8

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ENDARG_BIT
    "ditto, and unbound braces. "
    ^ 2

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_ENDFN_BIT
    "ditto, and unbound braces. "
    ^ 3

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_END_BIT
    "newline significant, +/- is an operator. "
    ^ 1

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_FNAME_BIT
    "ignore newline, no reserved words. "
    ^ 7

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_MAX_STATE
    ^ 11

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_MID_BIT
    "newline significant, +/- is an operator. "
    ^ 6

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

EXPR_VALUE_BIT
    "like EXPR_BEG but label is disallowed. "
    ^ 10

    "Created: / 02-04-2015 / 12:21:00 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

