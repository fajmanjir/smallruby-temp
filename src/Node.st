"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#Node
	instanceVariableNames:'nd_file u1 u2 u3 positions nodeFlags'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!Node class methodsFor:'initialization'!

initializeWithPositions: aPositions type: aType a0: a0 a1: a1 a2: a2
    "Factory method of Node"
    |node u1 u2 u3|
    node := self new.
    node positions: aPositions;
         nodeFlags: 0;
         type: aType.
    u1 := NodePart new.
    u2 := NodePart new.
    u3 := NodePart new.
    u1 value: a0.
    u2 value: a1.
    u3 value: a2.
    node u1: u1;
         u2: u2;
         u3: u3.

    ^ node.
    "
     optional: comment giving example use
    "

"
 change the above template into real code;
 remove this comment.
 Then `accept' either via the menu 
 or via the keyboard (usually CMD-A).

 You do not need this template; you can also
 select any existing methods code, change it,
 and finally `accept'. The method will then be
 installed under the selector as defined in the
 actual text - no matter which method is selected
 in the browser.

 Or clear this text, type in the method from scratch
 and install it with `accept'.
"

    "Created: / 22-03-2015 / 11:18:16 / hhyperion <fajmaji1@fit.cvut.cz>"
    "Modified: / 22-03-2015 / 16:22:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Node class methodsFor:'enum'!

TYPEMASK
    ^ 16r7f << (self TYPESHIFT)
    "
     optional: comment giving example use
    "

"
 change the above template into real code;
 remove this comment.
 Then `accept' either via the menu 
 or via the keyboard (usually CMD-A).

 You do not need this template; you can also
 select any existing methods code, change it,
 and finally `accept'. The method will then be
 installed under the selector as defined in the
 actual text - no matter which method is selected
 in the browser.

 Or clear this text, type in the method from scratch
 and install it with `accept'.
"

    "Created: / 22-03-2015 / 12:09:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

TYPESHIFT
    ^ 8

    "Created: / 22-03-2015 / 12:07:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Node methodsFor:'accessing'!

nd_1st
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_1st: nd_1st
    u1 node: nd_1st

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_2nd
    ^ u2 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_2nd: nd_2nd
    u2 node: nd_2nd

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_aid
    ^ u3 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_aid: nd_aid
    u3 id: nd_aid

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ainfo
    ^ u3 args

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ainfo: nd_ainfo
    u3 args: nd_ainfo

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_alen
    ^ u2 argc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_alen: nd_alen
    u2 argc: nd_alen

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_argc
    ^ u2 argc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_argc: nd_argc
    u2 argc: nd_argc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_args
    ^ u3 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_args: nd_args
    u3 node: nd_args

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_beg
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_beg: nd_beg
    u1 node: nd_beg

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_body
    ^ u2 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_body: nd_body
    u2 node: nd_body

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cflag
    ^ u2 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cflag: nd_cflag
    u2 id: nd_cflag

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cfnc
    ^ u1 cfunc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cfnc: nd_cfnc
    u1 cfunc: nd_cfnc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_clss
    ^ u1 value

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_clss: nd_clss
    u1 value: nd_clss

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cnt
    ^ u3 cnt

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cnt: nd_cnt
    u3 cnt: nd_cnt

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cond
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cond: nd_cond
    u1 node: nd_cond

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cpath
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cpath: nd_cpath
    u1 node: nd_cpath

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cval
    ^ u3 value

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_cval: nd_cval
    u3 value: nd_cval

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_defn
    ^ u3 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_defn: nd_defn
    u3 node: nd_defn

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_else
    ^ u3 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_else: nd_else
    u3 node: nd_else

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_end
    ^ u2 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_end: nd_end
    u2 node: nd_end

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ensr
    ^ u3 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_ensr: nd_ensr
    u3 node: nd_ensr

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_entry
    ^ u3 entry

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_entry: nd_entry
    u3 entry: nd_entry

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_file
    ^ nd_file
!

nd_file:something
    nd_file := something.
!

nd_frml
    ^ u2 argc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_frml: nd_frml
    u2 argc: nd_frml

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_head
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_head: nd_head
    u1 node: nd_head

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_iter
    ^ u3 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_iter: nd_iter
    u3 node: nd_iter

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_lit
    ^ u1 value

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_lit: nd_lit
    u1 value: nd_lit

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_mid
    ^ u2 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_mid: nd_mid
    u2 id: nd_mid

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_modl
    ^ u1 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_modl: nd_modl
    u1 id: nd_modl

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_next
    ^ u3 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_next: nd_next
    u3 node: nd_next

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_noex
    ^ u3 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_noex: nd_noex
    u3 id: nd_noex

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_nth
    ^ u2 argc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_nth: nd_nth
    u2 argc: nd_nth

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_oid
    ^ u1 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_oid: nd_oid
    u1 id: nd_oid

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_opt
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_opt: nd_opt
    u1 node: nd_opt

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_orig
    ^ u3 value

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_orig: nd_orig
    u3 value: nd_orig

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_pid
    ^ u1 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_pid: nd_pid
    u1 id: nd_pid

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_plen
    ^ u2 argc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_plen: nd_plen
    u2 argc: nd_plen

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_recv
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_recv: nd_recv
    u1 node: nd_recv

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_resq
    ^ u2 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_resq: nd_resq
    u2 node: nd_resq

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rest
    ^ u1 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rest: nd_rest
    u1 id: nd_rest

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rval
    ^ u2 value

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_rval: nd_rval
    u2 value: nd_rval

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_state
    ^ u3 state

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_state: nd_state
    u3 state: nd_state

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_stts
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_stts: nd_stts
    u1 node: nd_stts

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_super
    ^ u3 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_super: nd_super
    u3 node: nd_super

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tag
    ^ u1 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tag: nd_tag
    u1 id: nd_tag

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tbl
    ^ u1 tbl

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tbl: nd_tbl
    u1 tbl: nd_tbl

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tval
    ^ u2 value

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_tval: nd_tval
    u2 value: nd_tval

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_value
    ^ u2 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_value: nd_value
    u2 node: nd_value

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_var
    ^ u1 node

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_var: nd_var
    u1 node: nd_var

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_vid
    ^ u1 id

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_vid: nd_vid
    u1 id: nd_vid

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_visi
    ^ u2 argc

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nd_visi: nd_visi
    u2 argc: nd_visi

    "Created: / 23-03-2015 / 13:12:34 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

nodeFlags
    ^ nodeFlags
!

nodeFlags:something
    nodeFlags := something.
!

positions
    ^ positions
!

positions:something
    positions := something.
!

type  
    "get node type"
    ^ (nodeFlags & (self class TYPEMASK)) >> (self class TYPESHIFT)

    "Created: / 22-03-2015 / 14:58:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 22-03-2015 / 16:01:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

type: t
    "set new node type"
    |left right|
    left := (nodeFlags & (MathHelper bitInvert4Byte: (self class TYPEMASK))).

    right := (t << (self class TYPESHIFT)) & (self class TYPEMASK).
    nodeFlags := left | right.

    "Created: / 22-03-2015 / 16:01:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

u1
    ^ u1
!

u1:something
    u1 := something.
!

u2
    ^ u2
!

u2:something
    u2 := something.
!

u3
    ^ u3
!

u3:something
    u3 := something.
! !

