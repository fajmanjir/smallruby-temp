"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#RubyParser
	instanceVariableNames:'reader builder'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!RubyParser class methodsFor:'initialize'!

initializeWithInputReader: reader builder: builder
    "comment stating purpose of this message"

    |parser state|

    state := ParserState new.
    state initialize.  "TODO: initialize by default"

    parser := self new.
    parser reader: reader;
           builder: builder;
           parserState: state.

    ^ parser

.

    "Created: / 23-03-2015 / 21:25:41 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!RubyParser methodsFor:'accessing'!

builder
    ^ builder
!

builder:something 
    builder := something.
!

reader
    ^ reader
!

reader:something
    reader := something.
! !

