'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#TokenType
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!TokenType class methodsFor:'enum'!

fromChar: c
    "create token type from given char"
    ^ c asciiValue

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_BEGIN
    ^ 302

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_END
    ^ 303

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword__ENCODING__
    ^ 306

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword__FILE__
    ^ 305

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword__LINE__
    ^ 304

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_alias
    ^ 300

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_and
    ^ 292

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_begin
    ^ 262

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_break
    ^ 276

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_case
    ^ 271

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_class
    ^ 258

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_def
    ^ 260

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_defined
    ^ 301

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_do
    ^ 281

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_do_LAMBDA
    ^ 284

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_do_block
    ^ 283

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_do_cond
    ^ 282

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_else
    ^ 270

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_elsif
    ^ 269

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_end
    ^ 265

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_ensure
    ^ 264

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_false
    ^ 291

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_for
    ^ 275

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_if
    ^ 266

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_in
    ^ 280

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_module
    ^ 259

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_next
    ^ 277

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_nil
    ^ 289

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_not
    ^ 294

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_or
    ^ 293

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_redo
    ^ 278

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_rescue
    ^ 263

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_retry
    ^ 279

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_return
    ^ 285

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_self
    ^ 288

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_super
    ^ 287

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_then
    ^ 268

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_true
    ^ 290

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_undef
    ^ 261

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_unless
    ^ 267

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_until
    ^ 274

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_when
    ^ 272

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_while
    ^ 273

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

keyword_yield
    ^ 286

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

modifier_if
    ^ 295

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

modifier_rescue
    ^ 299

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

modifier_unless
    ^ 296

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

modifier_until
    ^ 298

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

modifier_while
    ^ 297

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tAMPER
    ^ 354

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tANDOP
    ^ 332

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tAREF
    ^ 338

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tASET
    ^ 339

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tASSOC
    ^ 345

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tBACK_REF
    ^ 321

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tCHAR
    ^ 319

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tCMP
    ^ 326

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tCOLON2
    ^ 342

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tCOLON3
    ^ 343

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tCONSTANT
    ^ 311

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tCVAR
    ^ 312

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tDOT2
    ^ 336

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tDOT3
    ^ 337

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tDSTAR
    ^ 353

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tEQ
    ^ 327

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tEQQ
    ^ 328

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tFID
    ^ 308

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tFLOAT
    ^ 315

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tGEQ
    ^ 330

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tGVAR
    ^ 309

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tIDENTIFIER
    ^ 307

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tIMAGINARY
    ^ 317

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tINTEGER
    ^ 314

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tIVAR
    ^ 310

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLABEL
    ^ 313

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLAMBDA
    ^ 355

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLAMBEG
    ^ 368

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLAST_TOKEN
    ^ 371

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLBRACE
    ^ 350

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLBRACE_ARG
    ^ 351

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLBRACK
    ^ 349

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLEQ
    ^ 331

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLOWEST
    ^ 369

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLPAREN
    ^ 346

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLPAREN_ARG
    ^ 347

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tLSHFT
    ^ 340

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tMATCH
    ^ 334

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tNEQ
    ^ 329

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tNMATCH
    ^ 335

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tNTH_REF
    ^ 320

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tOP_ASGN
    ^ 344

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tOROP
    ^ 333

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tPOW
    ^ 325

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tQSYMBOLS_BEG
    ^ 363

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tQWORDS_BEG
    ^ 361

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tRATIONAL
    ^ 316

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tREGEXP_BEG
    ^ 359

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tREGEXP_END
    ^ 322

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tRPAREN
    ^ 348

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tRSHFT
    ^ 341

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSTAR
    ^ 352

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSTRING_BEG
    ^ 357

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSTRING_CONTENT
    ^ 318

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSTRING_DBEG
    ^ 364

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSTRING_DEND
    ^ 365

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSTRING_DVAR
    ^ 366

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSTRING_END
    ^ 367

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSYMBEG
    ^ 356

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tSYMBOLS_BEG
    ^ 362

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tUMINUS
    ^ 324

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tUMINUS_NUM
    ^ 370

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tUPLUS
    ^ 323

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tWORDS_BEG
    ^ 360

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tXSTRING_BEG
    ^ 358

    "Created: / 15-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 15-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

