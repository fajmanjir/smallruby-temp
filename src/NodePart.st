'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#NodePart
	instanceVariableNames:'node id value tbl argc state rb_args_info cnt'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!NodePart methodsFor:'accessing'!

argc
    ^ argc
!

argc:something
    argc := something.
!

cnt
    ^ cnt
!

cnt:something
    cnt := something.
!

id
    ^ id
!

id:something
    id := something.
!

node
    ^ node
!

node:something
    node := something.
!

rb_args_info
    ^ rb_args_info
!

rb_args_info:something
    rb_args_info := something.
!

state
    ^ state
!

state:something
    state := something.
!

tbl
    ^ tbl
!

tbl:something
    tbl := something.
!

value
    ^ value
!

value:something
    value := something.
! !

