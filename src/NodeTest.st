"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:08'                !

"{ NameSpace: Smalltalk }"

TestCase subclass:#NodeTest
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - AST - Test'
!

!NodeTest methodsFor:'running'!

testNodeInitialize
    "comment stating purpose of this message"
    
    |node|

    node := Node initializeWithPositions: #positions type: (NodeType NODE_IF) a0: #a0Value a1: #a1Value a2: #a2Value.

    "test type (non-trivial) setter and getter"
    self assert: ((node type) = (NodeType NODE_IF)).
    node type: (NodeType NODE_SCOPE).
    self assert: (node type = (NodeType NODE_SCOPE)).

    self assert: (node positions = #positions).
    self assert: (node u1 value = #a0Value).
    self assert: (node u2 value = #a1Value).
    self assert: (node u3 value = #a2Value).

    "
     optional: comment giving example use"
    "
     change the above template into real code;
     remove this comment.
     Then `accept' either via the menu
     or via the keyboard (usually CMD-A).

     You do not need this template; you can also
     select any existing methods code, change it,
     and finally `accept'. The method will then be
     installed under the selector as defined in the
     actual text - no matter which method is selected
     in the browser.

     Or clear this text, type in the method from scratch
     and install it with `accept'."

    "Created: / 22-03-2015 / 12:27:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (format): / 22-03-2015 / 16:07:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testNodeShortcuts
    "comment stating purpose of this message"

    |node|

    node := Node initializeWithPositions: #positions type: (NodeType NODE_IF) a0: #a0Value a1: #a1Value a2: #a2Value.

    self assert: (#tvalTest ~= node u2 value).
    self assert: (#tvalTest ~= node nd_tval).
    node nd_tval: #tvalTest.
    self assert: (#tvalTest = node u2 value).
    self assert: (#tvalTest = node nd_tval).

    "
     optional: comment giving example use"
    "
     change the above template into real code;
     remove this comment.
     Then `accept' either via the menu
     or via the keyboard (usually CMD-A).

     You do not need this template; you can also
     select any existing methods code, change it,
     and finally `accept'. The method will then be
     installed under the selector as defined in the
     actual text - no matter which method is selected
     in the browser.

     Or clear this text, type in the method from scratch
     and install it with `accept'."

    "Created: / 23-03-2015 / 13:40:47 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

