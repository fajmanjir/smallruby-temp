"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#BitStack
	instanceVariableNames:'value'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!BitStack class methodsFor:'initialize'!

initialize
    ^ (self new value: 0)

    "Created: / 21-04-2015 / 18:11:46 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!BitStack methodsFor:'accessing'!

value
    ^ value
!

value:something
    value := something.
! !

!BitStack methodsFor:'accessing - behavior'!

lexPop
    ^ value := (value >> 1) bitOr: (value bitAnd: 1).

    "Created: / 21-04-2015 / 18:10:29 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

pop
    ^ value := (value >> 1)

    "Created: / 21-04-2015 / 18:52:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

push: newBit
    ^ value := (value << 1) bitOr: (newBit bitAnd: 1).

    "Created: / 21-04-2015 / 18:42:48 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

top
    ^ value bitAnd: 1.

    "Created: / 21-04-2015 / 18:54:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !


BitStack initialize!
