"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#SpecialChar
	instanceVariableNames:''
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!SpecialChar class methodsFor:'queries'!

isSpace: code
    "char with code 'code' is a kind of whitespace"
    (code = (self space asciiValue) or: [ code = self t asciiValue ]) ifTrue: [ ^ true ].
    (code >= (self n asciiValue) and: [ code <= self r asciiValue ]) ifTrue: [ ^ true ].
    
    ^ false

    "Created: / 02-04-2015 / 18:07:40 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!SpecialChar class methodsFor:'shortcuts'!

a
    "get '\a' character (alarm bell)"

    ^ 7 asCharacter

    "Created: / 12-04-2015 / 15:47:55 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

b
    "get '\b' character (backspace)"

    ^ 8 asCharacter

    "Created: / 12-04-2015 / 23:40:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

e
    "get '\e' character (escape)"

    ^ 27 asCharacter

    "Created: / 12-04-2015 / 15:49:23 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

f
    "get '\f' character"

    ^ 12 asCharacter

    "Created: / 01-04-2015 / 21:00:24 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 02-04-2015 / 18:26:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

n
    "get '\n' character"

    ^ 10 asCharacter

    "Created: / 28-03-2015 / 15:29:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 28-03-2015 / 20:56:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

r
    "get '\r' character"
    
    ^ 13 asCharacter

    "Created: / 28-03-2015 / 15:29:59 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 28-03-2015 / 20:56:19 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

space
    "get ' ' character"
    
    ^ 32 asCharacter

    "Created: / 01-04-2015 / 20:43:26 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 12-04-2015 / 16:11:16 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

t
    "get '\t' character"

    ^ 9 asCharacter

    "Created: / 01-04-2015 / 20:59:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 02-04-2015 / 18:26:21 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

v
    "get '\v' character"

    ^ 11 asCharacter

    "Created: / 02-04-2015 / 18:26:11 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

zeroCode
    "get '\0' character code"
    
    ^ 0

    "Created: / 12-04-2015 / 16:11:33 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

