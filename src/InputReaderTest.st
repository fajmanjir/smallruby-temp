"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

TestCase subclass:#InputReaderTest
	instanceVariableNames:'inputReader'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser - Test'
!

!InputReaderTest class methodsFor:'documentation'!

documentation
"
    documentation to be added.

    [author:]
        fajmaji1 <fajmaji1@fit.cvut.cz>

    [instance variables:]

    [class variables:]

    [see also:]

"
! !

!InputReaderTest methodsFor:'accessing'!

inputReader
    ^ inputReader
!

inputReader:something
    inputReader := something.
! !

!InputReaderTest methodsFor:'initialize / release'!

setUp
    "common setup - invoked before testing."
    |in|
    in := '..abc'.
    inputReader := InputReader initializeWithString: in.

    "Modified: / 03-04-2015 / 11:31:08 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

tearDown
    "common cleanup - invoked after testing."

    super tearDown

    "Modified (format): / 28-03-2015 / 15:24:09 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!InputReaderTest methodsFor:'tests'!

testAt
    inputReader := InputReader initializeWithString: 'abcde'.
    inputReader pos: 1;
                currentLine: (Position initializeWithBeg: 1 end: 6).   

    self assert: ((inputReader at: 3) = (TokenType fromChar: $c )).
    self assert: ((inputReader at: 4) = (TokenType fromChar: $d)).
    self assert: ((inputReader at: 4) ~= (TokenType fromChar: $c)).

    "Created: / 28-03-2015 / 15:49:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 10-04-2015 / 19:32:45 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testCharAt
    inputReader := InputReader initializeWithString: 'abcde'.
    inputReader pos: 1;
                currentLine: (Position initializeWithBeg: 1 end: 6).   

    self assert: ((inputReader charAt: 3) = $c).
    self assert: ((inputReader charAt: 4) = $d).
    self assert: ((inputReader charAt: 4) ~= $c).

    "Created: / 10-04-2015 / 19:32:10 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testInnerNSwitch
    "test switch in outer yylex switch after \n encountered"
    |c spaceSeenBlock|
    inputReader := InputReader initializeWithString: 'abcde'.
    inputReader pos: 1;
                currentLine: (Position initializeWithBeg: 1 end: 6).

    spaceSeenBlock := [ :c |
        inputReader spaceSeen: false;
                   c: c;
                   pos: 1.
        (inputReader innerNSwitch at: c) value.
        self assert: (inputReader spaceSeen).
    ].

    spaceSeenBlock value: (SpecialChar space asciiValue).
    spaceSeenBlock value: (SpecialChar t asciiValue).
    spaceSeenBlock value: (SpecialChar f asciiValue).
    spaceSeenBlock value: (SpecialChar r asciiValue).
    spaceSeenBlock value: 13.

    "$. char"
    c := $. asciiValue.
    inputReader spaceSeen: false;
                c: c.
    "another $. follows, should pushback twice and goto retryBlock"
    inputReader pos: 2.
    self should: [(inputReader innerNSwitch at: c) value. ] raise: Goto.
    self assert: ((inputReader pos) = 1).

    "another $. follows, should pushback twice and goto retryBlock"
    inputReader pos: 2.
    self assert: ([(inputReader innerNSwitch at: c) value. false. ] on: Goto do: [:ex |
        self assert: (((ex block) = (inputReader retryBlock))). true. 
    ]).
    self assert: ((inputReader pos) = 1).

    "no $. follows, should call default and continue with case -1, then goto normalNewlineBlock"
    inputReader pos: 2;
                sourceline: 2;
                lastline: 10;
                currentLine: (Position initializeWithBeg: 1 end: 25).
    self assert: ([(inputReader innerNSwitch at: c) value. false ] on: Goto do: [ :ex |
        self assert: (((ex block) = (inputReader normalNewlineBlock))). true. 
    ] ).
    self assert: ((inputReader pos) = ((inputReader currentLine end))).
    self assert: ((inputReader sourceline) = 1).
    self assert: ((inputReader nextline) = 10).

    "Created: / 01-04-2015 / 22:47:52 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 10-04-2015 / 19:33:28 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testLastNChars
    self setUp.
                   
    inputReader := InputReader initializeWithString: 'abcdefghijk'.
    inputReader pos: 8.

    self assert: ('cdefg' = (inputReader lastNChars: 5)).

    "Created: / 10-04-2015 / 20:11:36 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testPeekChar
    inputReader := InputReader initializeWithString: 'abcde'.
    inputReader pos: 1;
                currentLine: (Position initializeWithBeg: 1 end: 6).

    self assert: (inputReader pos = 1).
    self assert: (inputReader peekChar: $a).

    "do not change pos after peek"
    self assert: (inputReader peekChar: $b) not.

    inputReader pos: 5.
    self assert: (inputReader peekChar: $e).  

    "peek at the end of file"
    inputReader pos: 6.
    self assertFalse: (inputReader peekChar: $e).

    "Created: / 09-04-2015 / 23:06:25 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 10-04-2015 / 19:15:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testScanHex
    self setUp.
                   
    inputReader := InputReader initializeWithString: '2a'.
    inputReader pos: 1.

    self assert: (42 = (inputReader scanHex: 3)).

    inputReader := InputReader initializeWithString: '2F'.
    inputReader pos: 1.

    self assert: (47 = (inputReader scanHex: 3)).
    self assert: (2 = (inputReader scanHex: 1)).

    "Created: / 10-04-2015 / 13:14:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 12-04-2015 / 16:28:20 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

testScanOct
    self setUp.
                   
    inputReader := InputReader initializeWithString: '15'.
    inputReader pos: 1.

    self assert: (8r15 = (inputReader scanOct: 3)).

    inputReader := InputReader initializeWithString: '7306'.
    inputReader pos: 1.

    self assert: (8r7306 = (inputReader scanOct: 4)).

    self assert: (8r73 = (inputReader scanOct: 2)).

    "Created: / 12-04-2015 / 16:23:03 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

