"{ Encoding: utf8 }"

'From Smalltalk/X, Version:6.2.5.1565 on 27-04-2015 at 13:15:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#Encoding
	instanceVariableNames:'name'
	classVariableNames:''
	poolDictionaries:''
	category:'Ruby Parser - Parser'
!

!Encoding class methodsFor:'enum'!

ASCII8BIT
    ^ #'ASCII-8BIT'

    "Created: / 13-04-2015 / 12:34:37 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ENC_CODERANGE_7BIT
    ^ 1

    "Created: / 06-04-2015 / 15:45:46 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

ENC_CODERANGE_UNKNOWN
    ^ 0

    "Created: / 06-04-2015 / 15:45:51 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

USASCII
    ^ #'US-ASCII'

    "Created: / 13-04-2015 / 12:31:27 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

UTF8                                          
    ^ #'UTF-8'

    "Created: / 10-04-2015 / 20:38:30 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 13-04-2015 / 12:30:15 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Encoding methodsFor:'accessing'!

name
    ^ name
!

name:something
    name := something.
! !

!Encoding methodsFor:'accessing - behavior'!

associate: str enc: encoding
    ^ str

    "Created: / 13-04-2015 / 12:35:14 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Encoding methodsFor:'creation'!

strNew: p
    ^ p

    "Created: / 13-04-2015 / 11:32:35 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!Encoding methodsFor:'queries'!

asciiCompat
    ^ true

    "Created: / 13-04-2015 / 12:18:03 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isAlnum: char
    "Test if given char code is an alphanumeric char"

    ^ (Character value: char) isAlphaNumeric

    "Created: / 09-04-2015 / 22:14:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isAlpha: code
    "Test if given char code is an alphanumeric char"

    ^ (Character value: code) isLetter

    "Created: / 14-04-2015 / 16:50:50 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isAscii: code
    "Test if given char code is an ascii char"
    ^ (code <= 127) and: [code >= 0]

    "Created: / 09-04-2015 / 22:40:54 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (format): / 14-04-2015 / 16:46:32 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isDigit: c
    "check if char with code c is a number"
    |char|
    char := Character value: c.
    ^ (char >= $0) and: [char <= $9]

    "Created: / 13-04-2015 / 13:43:02 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified (comment): / 26-04-2015 / 17:10:12 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isIdent: c
    "Test if c code is an identifier char"
    ^ (self isAlnum: c) or: [c = (TokenType fromChar: $_)] or: [ (self isAscii: c) not ]

    "Created: / 13-04-2015 / 21:39:16 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isSpace: c
    "is c code a blank char?"
    ^ SpecialChar isSpace: c

    "Created: / 09-04-2015 / 12:07:01 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

isXDigit: c
    |char|
    char := (Character value: c) asLowercase.
    ^ ((char >= $0) and: [char <= $9]) or: [(char >= $a) and: [char <= $f]]

    "Created: / 13-04-2015 / 22:18:49 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

