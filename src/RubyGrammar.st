'From Smalltalk/X, Version:6.2.5.1565 on 13-04-2015 at 22:41:08'                !

"{ NameSpace: Smalltalk }"

Object subclass:#RubyGrammar
	instanceVariableNames:''
	classVariableNames:'opTbl parserState'
	poolDictionaries:''
	category:'Ruby Parser - AST'
!

!RubyGrammar class methodsFor:'helper'!

convert_op: id
    "search for operator in opTbl"
    |result|

    "Initialize the table if not done yet"
    opTbl.

    result := id.

    opTbl do: [ :item |
        (item token = id) ifTrue: [
            ^ self parser_intern: (item name)
        ].
    ].

    ^ result

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

op_tbl
    "Lazy loading of opTbl"
    (opTbl isNil)
        ifTrue: [
            opTbl := { 
                (OpTblItem initializeWithToken:  (TokenType tDOT2) name: '..') .
                (OpTblItem initializeWithToken:  (TokenType tDOT3) name: '...') .
                (OpTblItem initializeWithToken:  (TokenType tPOW) name: '**') .
                (OpTblItem initializeWithToken:  (TokenType tDSTAR) name: '**') .
                (OpTblItem initializeWithToken:  (TokenType tUPLUS) name: '+@') .
                (OpTblItem initializeWithToken:  (TokenType tUMINUS) name: '-@') .
                (OpTblItem initializeWithToken:  (TokenType tCMP) name: '<=>') .
                (OpTblItem initializeWithToken:  (TokenType tGEQ) name: '>=') .
                (OpTblItem initializeWithToken:  (TokenType tLEQ) name: '<=') .
                (OpTblItem initializeWithToken: (TokenType tEQ) name: '==') .
                (OpTblItem initializeWithToken: (TokenType tEQQ) name: '===') .
                (OpTblItem initializeWithToken: (TokenType tNEQ) name: '!!=') .
                (OpTblItem initializeWithToken: (TokenType tMATCH) name: '=~') .
                (OpTblItem initializeWithToken: (TokenType tNMATCH) name: '!!~') .
                (OpTblItem initializeWithToken: (TokenType tAREF) name: '[]') .
                (OpTblItem initializeWithToken: (TokenType tASET) name: '[]=') .
                (OpTblItem initializeWithToken: (TokenType tLSHFT) name: '<<') .
                (OpTblItem initializeWithToken: (TokenType tRSHFT) name: '>>') .
                (OpTblItem initializeWithToken: (TokenType tCOLON2) name: '::') .
                (OpTblItem initializeWithToken: (TokenType fromChar: $!!) name: $!!) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $%) name: $%) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $&) name: $&) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $*) name: $*) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $+) name: $+) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $-) name: $-) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $/) name: $/) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $<) name: $<) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $>) name: $>) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $^) name: $^) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $`) name: $`) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $|) name: $|) .
                (OpTblItem initializeWithToken: (TokenType fromChar: $~) name: $~)
            }.
        ].

    ^ opTbl

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!RubyGrammar class methodsFor:'internal'!

parser_intern2: name length: length
    ^ self parser_intern3: name length: (name length) enc: (self parser_usascii_encoding)

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parser_intern3: name length: length
    "TODO: not implemented"
    ^ nil

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parser_intern: name
    ^ self parser_intern2: name length: (name length)

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parser_usascii_encoding
    "TODO: not implemented"
    ^ nil

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

!RubyGrammar methodsFor:'accessing'!

parserState
    ^ parserState

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
!

parserState: something
    parserState := something.

    "Created: / 17-03-2015 / 11:51:18 / fajmaji1 <fajmaji1@fit.cvut.cz>"
    "Modified: / 17-03-2015 / 15:48:39 / fajmaji1 <fajmaji1@fit.cvut.cz>"
! !

